package cn.huiyunche.driver.datasync.schedule;


import cn.huiyunche.driver.service.TmsQueueCalculateService;
import org.joda.time.DateTime;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * 更新价格定时任务
 * The type Recalculate orders quartz cluster.
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class RecalculateDWaybillQuartzCluster implements Job, Serializable{

    private static final long serialVersionUID = 7089854684831425637L;

	private static final Logger logger = LoggerFactory.getLogger(RecalculateDWaybillQuartzCluster.class);

    @Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("job start: recalculate orders job time: {}", new DateTime().toString("yyyy-MM-dd HH:mm:ss:SSS"));

        TmsQueueCalculateService tmsQueueCalculateService = QuartzClusterFactory.getTmsQueueCalculateService();

        tmsQueueCalculateService.calculateDWaybill();

        logger.info("jbo end: recalculate orders job time: {}", new DateTime().toString("yyyy-MM-dd HH:mm:ss:SSS"));
	}

}
