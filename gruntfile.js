/**
 * Created by Tuffy on 16/5/30.
 */
'use strict';

module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: ['hycapp-kyle/src/main/webapp/js/module/weixin/**/*.js', 'hycapp-kyle/src/main/webapp/js/module/kyle-tools.js', 'hycapp-kyle/src/main/webapp/js/module/pingpp/pingpp.js'],
                dest: 'hycapp-kyle/src/main/webapp/js/module/build/<%= pkg.name %>.wx.min.js'
            }
        },
        ngtemplates: {
            kyleApp: {
                // cwd: 'dist',
                src: ['hycapp-kyle/src/main/webapp/templates/**/*.html'],
                dest: 'hycapp-kyle/src/main/webapp/js/module/build/<%= pkg.name %>.wx.tmpl.js',
                options: {
                    htmlmin: { //https://github.com/kangax/html-minifier#options-quick-reference
                        removeComments: true,
                        removeCommentsFromCDATA: true,
                        removeCDATASectionsFromCDATA: true,
                        collapseWhitespace: true,
                        conservativeCollapse: true,
                        preserveLineBreaks: true,
                        collapseBooleanAttributes: false,
                        removeAttributeQuotes: false,
                        removeRedundantAttributes: true,
                        preventAttributesEscaping: true,
                        useShortDoctype: true,
                        removeEmptyAttributes: false,
                        removeScriptTypeAttributes: false,
                        removeStyleLinkTypeAttributes: true,
                        removeOptionalTags: false,
                        removeIgnored: false,
                        removeEmptyElements: false,
                        keepClosingSlash: true,
                        caseSensitive: true,
                        minifyJS: {
                            mangle: true
                        },
                        minifyCSS: true,
                        minifyURLs: false
                    }
                }
            }
        },
        'string-replace': {
            tmpl_html: {
                files: {'hycapp-kyle/src/main/webapp/js/module/build/<%= pkg.name %>.wx.tmpl.js': 'hycapp-kyle/src/main/webapp/js/module/build/<%= pkg.name %>.wx.tmpl.js'},
                options: {
                    replacements: [
                        {
                            pattern: /put\('hycapp-kyle\/src\/main\/webapp/g,
                            replacement: "put(contextPath + '"
                        }
                    ]
                }
            },
            min_js: {
                files: {'hycapp-kyle/src/main/webapp/js/module/build/<%= pkg.name %>.wx.min.js': 'hycapp-kyle/src/main/webapp/js/module/build/<%= pkg.name %>.wx.min.js'},
                options: {
                    replacements: [
                        {
                            pattern: "var kyleCtrl=angular.module(\"kyle.controllers\",[]);",
                            replacement: ""
                        },
                        {
                            pattern: "\"use strict\";",
                            replacement: "\"use strict\";var kyleCtrl = angular.module(\"kyle.controllers\", []);"
                        }
                    ]
                }
            }
        },
        cssmin: { // https://github.com/jakubpawlowicz/clean-css#how-to-use-clean-css-programmatically
            css: {
                src: 'hycapp-kyle/src/main/webapp/style/css/weixin/kyle-css.css',
                dest: 'hycapp-kyle/src/main/webapp/style/css/weixin/<%= pkg.name %>.wx.min.css'
            }
        }
    });

    // 加载包含 "uglify" 任务的插件。
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // 默认被执行的任务列表。
    grunt.registerTask('hyc', ['uglify', 'ngtemplates', 'string-replace', 'cssmin']);

};