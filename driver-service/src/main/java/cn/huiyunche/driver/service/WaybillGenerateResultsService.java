package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.WaybillGenerateResults;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.query.WaybillGenerateResultsQueryConditions;

import java.util.Map;

/**
 * The interface Tms order import results service.
 *
 * @FileName: cn.huiyunche.driver.service
 * @Description: tms是否成功生成运单记录
 * @author: Aaron
 * @date: 2017 /3/9 下午2:44
 */
public interface WaybillGenerateResultsService {

    /**
     * Add integer.
     *
     * @param result the result
     * @return the integer
     */
    Long addWaybillGenerateResults(WaybillGenerateResults result);

    /**
     * Select list by conditions list.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the list
     */
    Map<String, Object> selectListByConditions(PageVo pageVo, WaybillGenerateResultsQueryConditions conditions);

    /**
     * Regenerate.
     *
     * @param ids the ids
     */
    void regenerate(String ids);

    /**
     * Select by order id waybill generate results.
     *
     * @param orderId the order id
     * @return the waybill generate results
     */
    WaybillGenerateResults selectByOrderId(Long orderId);
}
