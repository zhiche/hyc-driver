package cn.huiyunche.driver.service.query;

/**
 * @FileName: cn.huiyunche.driver.service.query
 * @Description: 线路查询条件
 * @author: Aaron
 * @date: 2017/2/26 下午8:50
 */
public class DRouteQueryConditions {

    //线路名称
    private String name;

    //起始地关键字
    private String oTag;

    //起点省编码
    private String oProvinceCode;

    //起点省份
    private String oProvince;

    //起点市编码
    private String oCityCode;

    //起点城市
    private String oCity;

    //起点县编码
    private String oCountyCode;

    //起点区县
    private String oCounty;

    //目的地关键字
    private String dTag;

    //终点省编码
    private String dProvinceCode;

    //终点省份
    private String dProvince;

    //终点市编码
    private String dCityCode;

    //终点城市
    private String dCity;

    //终点县编码
    private String dCountyCode;

    //终点区县
    private String dCounty;

    //状态
    private Boolean enable;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOProvinceCode() {
        return oProvinceCode;
    }

    public void setOProvinceCode(String oProvinceCode) {
        this.oProvinceCode = oProvinceCode;
    }

    public String getOProvince() {
        return oProvince;
    }

    public void setOProvince(String oProvince) {
        this.oProvince = oProvince;
    }

    public String getOCityCode() {
        return oCityCode;
    }

    public void setOCityCode(String oCityCode) {
        this.oCityCode = oCityCode;
    }

    public String getOCity() {
        return oCity;
    }

    public void setOCity(String oCity) {
        this.oCity = oCity;
    }

    public String getOCountyCode() {
        return oCountyCode;
    }

    public void setOCountyCode(String oCountyCode) {
        this.oCountyCode = oCountyCode;
    }

    public String getOCounty() {
        return oCounty;
    }

    public void setOCounty(String oCounty) {
        this.oCounty = oCounty;
    }

    public String getDProvinceCode() {
        return dProvinceCode;
    }

    public void setDProvinceCode(String dProvinceCode) {
        this.dProvinceCode = dProvinceCode;
    }

    public String getDProvince() {
        return dProvince;
    }

    public void setDProvince(String dProvince) {
        this.dProvince = dProvince;
    }

    public String getDCityCode() {
        return dCityCode;
    }

    public void setDCityCode(String dCityCode) {
        this.dCityCode = dCityCode;
    }

    public String getDCity() {
        return dCity;
    }

    public void setDCity(String dCity) {
        this.dCity = dCity;
    }

    public String getDCountyCode() {
        return dCountyCode;
    }

    public void setDCountyCode(String dCountyCode) {
        this.dCountyCode = dCountyCode;
    }

    public String getDCounty() {
        return dCounty;
    }

    public void setDCounty(String dCounty) {
        this.dCounty = dCounty;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getOTag() {
        return oTag;
    }

    public void setOTag(String oTag) {
        this.oTag = oTag;
    }

    public String getDTag() {
        return dTag;
    }

    public void setDTag(String dTag) {
        this.dTag = dTag;
    }

    @Override
    public String toString() {
        return "DRouteQueryConditions{" +
                "name='" + name + '\'' +
                ", oTag='" + oTag + '\'' +
                ", oProvinceCode='" + oProvinceCode + '\'' +
                ", oProvince='" + oProvince + '\'' +
                ", oCityCode='" + oCityCode + '\'' +
                ", oCity='" + oCity + '\'' +
                ", oCountyCode='" + oCountyCode + '\'' +
                ", oCounty='" + oCounty + '\'' +
                ", dTag='" + dTag + '\'' +
                ", dProvinceCode='" + dProvinceCode + '\'' +
                ", dProvince='" + dProvince + '\'' +
                ", dCityCode='" + dCityCode + '\'' +
                ", dCity='" + dCity + '\'' +
                ", dCountyCode='" + dCountyCode + '\'' +
                ", dCounty='" + dCounty + '\'' +
                ", enable=" + enable +
                '}';
    }
}
