package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.TmsOrder;

import java.util.Date;
import java.util.List;

/**
 * Created by zhaoguixin on 2017/6/27.
 */
public interface FetchTmsOrder {

    /**
     * 得到数据库上次调用成功的时间
     * @return
     */
    Date fetchLastTimeSuccess();

    /**
     * 得到TMS中最新修改的订单号列表
     * @param lastTime 上次时间
     * @return
     */
    List<String> fetchLastOrderNo(Date lastTime);


    /**
     * 得到待处理的订单号列表
     * @param lastTime
     * @return
     */
    List<String> fetchPendingOrderNo(Date lastTime);


    TmsOrder fetchTmsOrderByOrderNo(String orderNo);
}
