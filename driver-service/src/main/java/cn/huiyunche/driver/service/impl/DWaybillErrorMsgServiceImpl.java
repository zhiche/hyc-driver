package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.DWaybillErrorMsgMapper;
import cn.huiyunche.base.service.mappers.ext.DWaybillErrorMsgExtMapper;
import cn.huiyunche.base.service.model.DWaybillErrorMsg;
import cn.huiyunche.base.service.model.DWaybillErrorMsgExample;
import cn.huiyunche.base.service.vo.DWaybillErrorMsgVo;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.DWaybillErrorMsgService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/4 下午12:02
 */
@Service
public class DWaybillErrorMsgServiceImpl implements DWaybillErrorMsgService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DWaybillErrorMsgServiceImpl.class);

    @Autowired
    private DWaybillErrorMsgMapper dWaybillErrorMsgMapper;

    @Autowired
    private DWaybillErrorMsgExtMapper dWaybillErrorMsgExtMapper = null;

    @Override
    public Integer add(DWaybillErrorMsg errorMsg) {
        LOGGER.info("DWaybillErrorMsgServiceImpl.add param : {}", errorMsg);

        if (null == errorMsg) {
            LOGGER.error("DWaybillErrorMsgServiceImpl.add param errorMsg must not be null");
            throw new IllegalArgumentException("运单错误信息不能为空");
        }

        dWaybillErrorMsgMapper.insertSelective(errorMsg);

        return errorMsg.getId();
    }

    @Override
    public List<DWaybillErrorMsg> selectByConditions(DWaybillErrorMsgExample example) {
        LOGGER.info("DWaybillErrorMsgServiceImpl.selectByConditions param : {}", example);

        if (null == example) {
            LOGGER.error("DWaybillErrorMsgServiceImpl.selectByConditions param example must not be null");
            throw new IllegalArgumentException("运单错误信息查询条件不能为空");
        }

        return dWaybillErrorMsgMapper.selectByExample(example);
    }

    @Override
    public Map<String, Object> listByCondition(PageVo page, DWaybillErrorMsgVo vo) {
        LOGGER.info("DWaybillErrorMsgServiceImpl.listByCondition params: {}, {}", page, vo);
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> rtMap = new HashMap<>();
        if (null != vo) {
            if (StringUtils.isNotBlank(vo.getOrderCode())) {
                map.put("orderCode", vo.getOrderCode());
            }
            if (StringUtils.isNotBlank(vo.getErrCode())) {
                map.put("errCode", vo.getErrCode());
            }
            if (null != vo.getOrderLineId()) {
                map.put("orderLineId", vo.getOrderLineId());
            }
            if (null != vo.getIsHandle()) {
                map.put("isHandle", vo.getIsHandle());
            }
            if (StringUtils.isNotBlank(vo.getPhone())) {
                map.put("phone", vo.getPhone());
            }
        }
        if (null != page) {
            page.setTotalRecord(dWaybillErrorMsgExtMapper.countByCondition(map));
            map.put("limitStart", page.getStartIndex());
            map.put("limitEnd", page.getPageSize());
        }
        List<DWaybillErrorMsgVo> vos = dWaybillErrorMsgExtMapper.listByCondition(map);
        rtMap.put("waybillError", vos);
        if (null != page) {
            rtMap.put("page", page);
        }
        return map;
    }

    @Override
    public int modifyisHandle(boolean bool, Integer id, Integer handleTimes) {
        LOGGER.info("DWaybillErrorMsgServiceImpl.modifyisHandle params: {}, {}", bool, id);
        if (id == null) {
            LOGGER.info("DWaybillErrorMsgServiceImpl.modifyisHandle id must be not null");
            throw new BusinessException("ID不能为空");
        }
        DWaybillErrorMsg msg = this.dWaybillErrorMsgMapper.selectByPrimaryKey(id);
        if (null == msg) {
            LOGGER.info("DWaybillErrorMsgServiceImpl.modifyisHandle result is null");
            throw new BusinessException("无异常数据");
        }
        msg.setIsHandle(bool);
        msg.setHandleTimes(handleTimes);
        return this.dWaybillErrorMsgMapper.updateByPrimaryKeySelective(msg);
    }


}
