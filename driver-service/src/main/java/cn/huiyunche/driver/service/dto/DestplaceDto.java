package cn.huiyunche.driver.service.dto;

/**
 * Created by lenovo on 2017/11/30.
 */
public class DestplaceDto {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "StartplaceDto{" +
                "name='" + name + '\'' +
                '}';
    }
}
