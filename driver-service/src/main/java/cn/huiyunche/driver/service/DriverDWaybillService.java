package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;

public interface DriverDWaybillService {

    /**
     * 查询在途订单
     *
     * @param pageVo
     * @param orderCode
     * @return
     */
    Result<Object> list(PageVo pageVo, String orderCode);

    /**
     * 交车时是否可选择本地照片
     *
     * @param id
     * @return
     */
    Result<String> modifyIsCheckLocalPic(Long id);

}
