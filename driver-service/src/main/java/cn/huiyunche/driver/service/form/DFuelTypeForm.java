package cn.huiyunche.driver.service.form;

import cn.huiyunche.base.service.constant.RegularConstant;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.service.form
 * @Description: 燃油类型表单
 * @author: Aaron
 * @date: 2017/2/26 下午5:02
 */
public class DFuelTypeForm {

    private Integer id;

    //燃油编码
    @NotBlank(message = "燃油编码不能为空")
    @Pattern(regexp = RegularConstant.letter_number, message = "燃油编码只能输入数字或字母")
    @Size(max = 32, min = 1, message = "燃油编码长度位1-32位")
    private String fuelCode;

    //燃油名称
    @NotBlank(message = "燃油名称不能为空")
//    @Pattern(regexp = RegularConstant.special, message = "燃油名称不能包含特殊字符")
    @Size(max = 16, min = 1, message = "燃油编码长度位1-16位")
    private String fuelName;

    //基础油价
//    @NotNull(message = "基础价格不能为空")
//    @Pattern(regexp = RegularConstant.number_decimals, message = "基础油价只能输入数字且为正数")
//    @DecimalMin(value = "0.01", message = "基础价格不能小于0公里")
//    private BigDecimal basePrice;

    private Date createTime;

    private String creator;

    private Date updateTime;

    public String getFuelCode() {
        return fuelCode;
    }

    public void setFuelCode(String fuelCode) {
        this.fuelCode = fuelCode;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "DFuelTypeForm{" +
                "id=" + id +
                ", fuelCode='" + fuelCode + '\'' +
                ", fuelName='" + fuelName + '\'' +
                ", createTime=" + createTime +
                ", creator='" + creator + '\'' +
                ", updateTime=" + updateTime +
                '}';
    }
}
