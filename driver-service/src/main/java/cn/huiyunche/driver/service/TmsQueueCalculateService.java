package cn.huiyunche.driver.service;


/**
 * 司机维度派单接口
 *
 * @author hdy [Tuffy]
 */
public interface TmsQueueCalculateService {


    /**
     * 计算运单价格
     */
    public void calculateDWaybill();

}
