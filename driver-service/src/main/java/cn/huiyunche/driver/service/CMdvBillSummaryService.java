package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.CMdvBillSummary;
import cn.huiyunche.base.service.model.CMdvBillSummaryExample;
import cn.huiyunche.base.service.vo.Result;


import java.util.List;

/**
 * The interface D route service.
 *
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: tms导入账单
 * @author: ligl
 * @date: 2017 /3/9 上午11:22
 */
public interface CMdvBillSummaryService {

    /**
     * Add integer.
     *
     * @param cMdvBillSummary
     * @return the integer
     */
    Long add(CMdvBillSummary cMdvBillSummary);


    /**
     * Select by primary key
     *
     * @param summaryExample
     * @return List<CMdvBillSummary>
     */
    List<CMdvBillSummary> selectByExample(CMdvBillSummaryExample summaryExample);

    /**
     * Select by primary key
     *
     * @param beginDate
     * @param endDate
     * @return Result<Object>
     */
    Result<Object> getSummaryList(String beginDate, String endDate) throws Exception;

    /**
     * Select by primary key d route.
     *
     * @param orderCode
     * @return
     * @throws Exception
     */
    Result<Object> getBillDetailList(String orderCode) throws Exception;


}
