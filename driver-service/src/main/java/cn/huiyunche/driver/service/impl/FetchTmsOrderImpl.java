package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.constant.TmsUrlConstant;
import cn.huiyunche.base.service.enums.TmsUrlTypeEnum;
import cn.huiyunche.base.service.interfaces.TmsOrderCallHistoryService;
import cn.huiyunche.base.service.model.TmsOrder;
import cn.huiyunche.base.service.utils.DateUtils;
import cn.huiyunche.base.service.utils.HttpRequestUtil;
import cn.huiyunche.driver.service.FetchTmsOrder;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by zhaoguixin on 2017/6/27.
 */
@Service
public class FetchTmsOrderImpl implements FetchTmsOrder {

    private static final Logger LOGGER = LoggerFactory.getLogger(FetchTmsOrderImpl.class);

    @Autowired
    private TmsOrderCallHistoryService tmsOrderCallHistoryService;

    @Override
    public Date fetchLastTimeSuccess() {
        return tmsOrderCallHistoryService.getLastTimeSuccess(TmsUrlTypeEnum.PULL_ORDER_NO.getValue(), true);
    }

    @Override
    public List<String> fetchLastOrderNo(Date lastTime) {
        final String url = TmsUrlConstant.PULL_ORDER_NO;
        Map<String, Object> paramsMap = new HashedMap<>();
        //DateTime datatime = new DateTime(2016, 12, 20, 00, 00,00);
        paramsMap.put("lastTime", new DateTime(lastTime).toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));
        String result = tmsOrderCallHistoryService.urlCall(url, TmsUrlTypeEnum.PULL_ORDER_NO.getValue(), paramsMap, 30000);

        if (StringUtils.isNotBlank(result)) {
            JSONObject jsonObject = JSONObject.parseObject(result);
            String data = jsonObject.getString("data");
            if (StringUtils.isNotBlank(data)) {
                List<OrderNo> tmsOrderNos = JSONArray.parseArray(data, OrderNo.class);

                if (null==tmsOrderNos ||tmsOrderNos.size()==0) return  null;

                List<String> orderNos = new ArrayList<String>();

                for (OrderNo orderNo:tmsOrderNos) {
                    orderNos.add(orderNo.getVcorderno());
                }
                return  orderNos;
            }
            return null;
        }
        return null;
    }

    @Override
    public List<String> fetchPendingOrderNo(Date lastTime) {
        List<String> latestOrderNo = fetchLastOrderNo(lastTime);

        //TODO 将日志中转化出错的订单重新抓取到得处理订单列表

        if(null==latestOrderNo||latestOrderNo.size()==0){
            return null;
        }else{
            return latestOrderNo;
        }
    }

    @Override
    public TmsOrder fetchTmsOrderByOrderNo(String orderNo) {
        final String url = TmsUrlConstant.PULL_SINGLE_ORDER;
        Map<String, Object> paramsMap = new HashedMap<>();
        paramsMap.put("orderCode", orderNo);
//        String resultStr = tmsOrderCallHistoryService.urlCall(url, TmsUrlTypeEnum.PULL_SINGLE_ORDER.getValue(), paramsMap, 30000);
//        if (StringUtils.isNotBlank(resultStr)) {
//            JSONObject jsonObject = JSONObject.parseObject(resultStr);
//            String data = jsonObject.getString("data");
//            if (StringUtils.isNotBlank(data)) {
//                List<TmsOrder> tmsOrders = JSONArray.parseArray(data, TmsOrder.class);
//                return tmsOrders.get(0);
//            }
//            return  null;
//        }else{
//            return null;
//        }
        try{
            String result = HttpRequestUtil.sendHttpPost(url, paramsMap, 30000);
            if(StringUtils.isNotBlank(result)) {
                JSONObject object = JSONObject.parseObject(result);
                boolean success = object.getBoolean("success").booleanValue();
                if(success) {
                    String data = object.getString("data");
                    JSONObject dataObj = JSONObject.parseObject(data);
                    String tmsData = dataObj.getString("data");
                    if (StringUtils.isNotBlank(tmsData)) {
                        List<TmsOrder> tmsOrders = JSONArray.parseArray(tmsData, TmsOrder.class);
                        return tmsOrders.get(0);
                    }
                }
            }
            return null;
        }catch (Exception e){
            LOGGER.error("urlCall getData : {}", e);
            return  null;
        }
    }
}
