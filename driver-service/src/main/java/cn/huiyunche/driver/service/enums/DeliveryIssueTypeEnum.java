package cn.huiyunche.driver.service.enums;

/**
 * 交车异常枚举
 *
 * @author hjh
 */
public enum DeliveryIssueTypeEnum {

    DRIVER_SELF_IMG(10, "司机本人照片尚未采集"),
    DELIVERY_IMG(20, "交车照片模糊"),
    BILL_IMG(30, "单据照片模糊"),
    GROUP_IMG(40, "合影照片模糊");

    private final int value;
    private final String text;

    DeliveryIssueTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static DeliveryIssueTypeEnum getByValue(int value) {
        for (DeliveryIssueTypeEnum temp : DeliveryIssueTypeEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }
}
