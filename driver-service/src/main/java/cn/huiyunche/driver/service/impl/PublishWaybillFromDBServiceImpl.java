package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.enums.DWaybillStatusEnum;
import cn.huiyunche.base.service.enums.DWaybillTypeEnum;
import cn.huiyunche.base.service.framework.utils.TmsQueueApi;
import cn.huiyunche.base.service.interfaces.DWaybillService;
import cn.huiyunche.driver.service.PublishWaybillFromDBService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/9 下午10:30
 */
@Service
public class PublishWaybillFromDBServiceImpl implements PublishWaybillFromDBService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PublishWaybillFromDBServiceImpl.class);

    @Autowired
    private DWaybillService dWaybillService;

    @Override
    public void db2Redis() {
        LOGGER.info("PublishWaybillFromDBServiceImpl.db2Redis invoking ");

        TmsQueueApi.appendOrders(dWaybillService.selectDwaybillByUserId(0L, Arrays.asList(DWaybillStatusEnum.SAVE.getValue()),Arrays.asList(DWaybillTypeEnum.NORMAL.getValue())));
    }
}
