package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.CMdvBillDetail;
import cn.huiyunche.base.service.model.CMdvBillDetailExample;

import java.util.List;

/**
 * The interface D route service.
 *
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: tms导入账单
 * @author: ligl
 * @date: 2017 /3/9 上午11:22
 */
public interface CMdvBillDetailService {

    /**
     * Add integer.
     *
     * @param cMdvBillDetail
     * @return the integer
     */
    Long add(CMdvBillDetail cMdvBillDetail);

    /**
     * Select by primary key d route.
     *
     * @param detailExample
     * @return List<CMdvBillDetail>
     */
    List<CMdvBillDetail> selectByExample(CMdvBillDetailExample detailExample);


}
