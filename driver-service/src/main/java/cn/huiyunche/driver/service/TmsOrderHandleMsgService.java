package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.TmsOrderHandleMsg;

/**
 * The interface D waybill error msg service.
 *
 * @FileName: cn.huiyunche.driver.service
 * @Description: 运单错误信息维护
 * @author: ligl
 * @date: 2017 /3/4 下午12:01
 */
public interface TmsOrderHandleMsgService {

    /**
     * Add integer.
     *
     * @param handleMsg the error msg
     * @return the integer
     */
    Integer add(TmsOrderHandleMsg handleMsg);

}
