package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.DWaybillMapper;
import cn.huiyunche.base.service.model.DWaybill;
import cn.huiyunche.base.service.model.DWaybillExample;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DriverDWaybillService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DriverDWaybillServiceImpl implements DriverDWaybillService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DriverDWaybillServiceImpl.class);

    @Autowired
    private DWaybillMapper dWaybillMapper = null;

    private DWaybillMapper getDWaybillMapper() {
        return this.dWaybillMapper;
    }

    @Override
    public Result<Object> list(PageVo pageVo, String orderCode) {
        LOGGER.info("list params pageVo: {}, orderCode: {}", pageVo.toString(), orderCode);
        Result<Object> result = new Result<>(true, null, "数据加载成功");
        Map<String, Object> map = new HashMap<>();
        String orderByClause = StringUtils.isNotBlank(pageVo.getOrder()) == true ? pageVo.getOrder() : " id DESC";
        DWaybillExample example = new DWaybillExample();
        if (StringUtils.isNotBlank(orderCode)) {
            example.createCriteria().andOrderCodeEqualTo(orderCode);
        }
        pageVo.setTotalRecord(this.getDWaybillMapper().countByExample(example));
        example.setLimitStart(pageVo.getStartIndex());
        example.setLimitEnd(pageVo.getPageSize());
        example.setOrderByClause(orderByClause);
        List<DWaybill> dWaybills = this.getDWaybillMapper().selectByExample(example);
        map.put("page", pageVo);
        map.put("dWaybills", dWaybills);
        result.setData(map);
        return result;
    }

    @Override
    public Result<String> modifyIsCheckLocalPic(Long id) {
        LOGGER.info("modifyIsCheckLocalPic params id: {}", id);
        Result<String> result = new Result<>(true, null, "数据操作已成功");
        if (id == null || id == 0) {
            LOGGER.info("id is not null");
            throw new BusinessException("ID不能为空");
        }
        DWaybill dWaybill = this.getDWaybillMapper().selectByPrimaryKey(id);
        if (dWaybill != null) {
            dWaybill.setIsCheckLocalPic(true);
            this.getDWaybillMapper().updateByPrimaryKeySelective(dWaybill);
        }
        return result;
    }

}
