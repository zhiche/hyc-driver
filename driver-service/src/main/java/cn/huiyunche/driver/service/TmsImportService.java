package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.TmsOrder;
import cn.huiyunche.base.service.vo.Result;

import java.util.List;

/**
 * tms导入数据 服务层
 *
 * @author hdy [Tuffy]
 */
public interface TmsImportService {

    /**
     * 导入tms用户数据
     *
     * @return 结果集
     */
    public Result<String> importUserExcel();

    /**
     * 导入tms订单数据
     *
     * @return 结果集
     */
    public List<TmsOrder> importOrderExcel();

}
