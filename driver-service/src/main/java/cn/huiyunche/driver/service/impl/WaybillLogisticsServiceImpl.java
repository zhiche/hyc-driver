package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.DWaybillLogisticsMapper;
import cn.huiyunche.base.service.model.DWaybillLogistics;
import cn.huiyunche.base.service.model.DWaybillLogisticsExample;
import cn.huiyunche.driver.service.WaybillLogisticsService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WaybillLogisticsServiceImpl implements WaybillLogisticsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WaybillLogisticsServiceImpl.class);

    @Autowired
    private DWaybillLogisticsMapper dWaybillLogisticsMapper = null;

    private DWaybillLogisticsMapper getDWaybillLogisticsMapper() {
        return this.dWaybillLogisticsMapper;
    }

    @Override
    public List<DWaybillLogistics> listByConditions(DWaybillLogistics logistics) {
        LOGGER.info("DWaybillLogisticsServiceImpl.listByConditions params: {}", logistics);
        DWaybillLogisticsExample example = new DWaybillLogisticsExample();
        DWaybillLogisticsExample.Criteria criteria = example.createCriteria();

        if (null != logistics) {
            if (logistics.getWaybillId() != null && logistics.getWaybillId() != 0) {
                criteria.andWaybillIdEqualTo(logistics.getWaybillId());
            }
            if (StringUtils.isNotBlank(logistics.getProvince())) {
                criteria.andProvinceEqualTo(logistics.getProvince());
            }
            if (StringUtils.isNotBlank(logistics.getCity())) {
                criteria.andCityEqualTo(logistics.getCity());
            }
            if (StringUtils.isNotBlank(logistics.getCounty())) {
                criteria.andCountyEqualTo(logistics.getCounty());
            }
        }
        return this.getDWaybillLogisticsMapper().selectByExample(example);
    }

}
