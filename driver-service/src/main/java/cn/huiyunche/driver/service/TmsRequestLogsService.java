package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DRequestLogs;
import com.alibaba.fastjson.JSONObject;


/**
 * tms请求日志写入
 *
 * @author hdy [Tuffy]
 */
public interface TmsRequestLogsService {

    /**
     * 添加日志
     *
     * @param orderCode 订单编号
     * @param type      日志类型
     * @param params    参数
     * @return 返回对象实体
     */
    public DRequestLogs addLog(String orderCode, int type, JSONObject params);

    /**
     * 添加返回结果集日志
     *
     * @param id     日志id
     * @param result 返回结果集
     */
    public void addResultLog(Long id, String result);
}
