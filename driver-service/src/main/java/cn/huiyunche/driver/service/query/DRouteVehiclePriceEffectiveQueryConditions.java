package cn.huiyunche.driver.service.query;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.service.query
 * @Description: 车型线路价格变动查询条件
 * @author: Aaron
 * @date: 2017/2/26 下午3:59
 */
public class DRouteVehiclePriceEffectiveQueryConditions {

    //线路ID
    @Min(value = 1, message = "请选择线路")
    private Integer routeId;

    //车型ID
    @Min(value = 1, message = "请选择车型")
    private Integer vehicleTypeId;

    //生效日期
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date effectiveDate;

    //失效日期
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date invalidDate;

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Integer getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getInvalidDate() {
        return invalidDate;
    }

    public void setInvalidDate(Date invalidDate) {
        this.invalidDate = invalidDate;
    }

    @Override
    public String toString() {
        return "DRouteVehiclePriceEffectiveQueryConditions{" +
                "routeId=" + routeId +
                ", vehicleTypeId=" + vehicleTypeId +
                ", effectiveDate=" + effectiveDate +
                ", invalidDate=" + invalidDate +
                '}';
    }
}
