package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.vo.UserShowVo;

import java.util.Map;

/**
 * @FileName: cn.huiyunche.driver.service
 * @Description: Description
 * @author: Aaron
 * @date: 2017/1/20 下午1:20
 */
public interface SDUserService {

    Map<String, Object> online(UserShowVo userVo) throws Exception;
}
