package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;

public interface SUserBindDeviceService {

    /**
     * 账号设备绑定列表
     *
     * @param phone
     * @return
     * @throws BusinessException
     */
    Result<Object> selectSUserBindDeviceByExample(PageVo pageVo, String phone) throws BusinessException;

    /**
     * 账号设备解绑
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    Result<Object> updateBindStatus(Integer id) throws BusinessException;
}
