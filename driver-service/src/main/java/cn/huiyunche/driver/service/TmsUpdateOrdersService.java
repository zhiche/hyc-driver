package cn.huiyunche.driver.service;

/**
 * 查询TMS关联人送订单接口
 *
 * @author ligl [Tuffy]
 */
public interface TmsUpdateOrdersService {

    /**
     * 查询TMS关联人送订单接口
     */
    public void start();

}
