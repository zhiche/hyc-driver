package cn.huiyunche.driver.service;

/**
 * 司机维度派单接口
 *
 * @author hdy [Tuffy]
 */
public interface TmsQueueDimensionService {

    /**
     * 开始派单
     */
    public void startDispatch();

}
