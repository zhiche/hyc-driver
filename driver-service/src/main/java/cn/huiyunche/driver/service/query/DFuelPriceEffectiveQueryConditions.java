package cn.huiyunche.driver.service.query;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.service.query
 * @Description: 燃油价格变动查询条件
 * @author: Aaron
 * @date: 2017/2/26 下午6:48
 */
public class DFuelPriceEffectiveQueryConditions {

    //燃油价格变动名称
    private String name;

    //燃油类型主键
    @Min(value = 1, message = "费用类型主键不能小于0")
    private Integer fuelTypeId;

    //生效时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date effectiveDate;

    //失效时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date invalidDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(Integer fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getInvalidDate() {
        return invalidDate;
    }

    public void setInvalidDate(Date invalidDate) {
        this.invalidDate = invalidDate;
    }

    @Override
    public String toString() {
        return "DFuelPriceEffectiveQueryConditions{" +
                "name='" + name + '\'' +
                ", fuelTypeId=" + fuelTypeId +
                ", effectiveDate=" + effectiveDate +
                ", invalidDate=" + invalidDate +
                '}';
    }
}
