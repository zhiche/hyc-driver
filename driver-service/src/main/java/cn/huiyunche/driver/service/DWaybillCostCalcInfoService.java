package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DWaybillCostCalcInfo;

/**
 * The interface D waybill cost calc info service.
 *
 * @FileName: cn.huiyunche.driver.service
 * @Description: 运单费用计算关联
 * @author: Aaron
 * @date: 2017 /2/28 下午3:39
 */
public interface DWaybillCostCalcInfoService {

    /**
     * Add integer.
     *
     * @param calcInfo the calc info
     * @return the integer
     */
    Integer add(DWaybillCostCalcInfo calcInfo);

    /**
     * Select by waybill id d waybill cost calc info.
     *
     * @param waybillId the waybill id
     * @return the d waybill cost calc info
     */
    DWaybillCostCalcInfo selectByWaybillId(Long waybillId);
}
