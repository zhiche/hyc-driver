package cn.huiyunche.driver.service;

import cn.huiyunche.driver.service.form.TmsDDeliveryAuditForm;

/**
 * Created by houjianhui on 2017/4/10.
 */
public interface TmsDDeliveryIssueService {

    /**
     * Add TmsDDeliveryIssue
     *
     * @param form
     * @return
     * @throws Exception
     */
    Long save(TmsDDeliveryAuditForm form) throws Exception;
}
