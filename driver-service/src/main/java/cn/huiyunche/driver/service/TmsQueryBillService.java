package cn.huiyunche.driver.service;


/**
 * Created by Ligl on 2017/3/7.
 */
public interface TmsQueryBillService {

    /**
     * Tms query bill.
     *
     * @param resultStr the result str
     */
    void tmsQueryBill(String resultStr);
}
