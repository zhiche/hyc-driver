package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.enums.DWaybillFeeDetailCostTypeEnum;
import cn.huiyunche.base.service.enums.DWaybillFeeEnum;
import cn.huiyunche.base.service.interfaces.DWaybillFeeDetailService;
import cn.huiyunche.base.service.interfaces.DWaybillFeeService;
import cn.huiyunche.base.service.interfaces.DWaybillFeeTypeService;
import cn.huiyunche.base.service.interfaces.DWaybillService;
import cn.huiyunche.base.service.model.DWaybill;
import cn.huiyunche.base.service.model.DWaybillFeeDetail;
import cn.huiyunche.driver.service.TmsQueueCalculateService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by Nicky on 17/6/26.
 */
@Service
public class TmsQueueCalculateServiceImpl implements TmsQueueCalculateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TmsQueueCalculateServiceImpl.class);

    @Autowired
    private DWaybillService dWaybillService = null;

    @Autowired
    private DWaybillFeeService dWaybillFeeService=null;

    @Autowired
    private DWaybillFeeTypeService dWaybillFeeTypeService;

    @Autowired
    private DWaybillFeeDetailService dWaybillFeeDetailService;


    /**
     * 计算运单价格
     */
    public void calculateDWaybill(){

        List<DWaybill> dWaybillList = null;

        try{
            //查找待发运订单和退单
            dWaybillList=this.dWaybillService.selectNoDeliveredDWaybill();

        }
        catch (Exception ex){
            LOGGER.error("dWaybillService.selectNoDeliveredDWaybill  is error:{} ,",ex );
        }
        //计算运单单价格
        if(CollectionUtils.isNotEmpty(dWaybillList)){
            for (DWaybill waybill:dWaybillList) {
                Map<Integer, Object> feeDetailsMap = null;
                try {
                    //计算价格
                    feeDetailsMap = dWaybillFeeService.getWaybillCostByCalc(
                                waybill.getIstyleid().intValue(), waybill.getDepartureProvince(), waybill.getDepartureCity(),
                                waybill.getDestProvince(), waybill.getDestCity(), waybill.getExtraCost());
                } catch (Exception e) {
                        LOGGER.error("dWaybillFeeService.getWaybillCostByCalc  is error:{} ,",e);
                }

                if(MapUtils.isNotEmpty(feeDetailsMap)) {
                    try {
                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.TOTAL_SHIPPING_COST.getValue()))) {
                            waybill.setConfirmTotalCost(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.TOTAL_SHIPPING_COST.getValue())).toString()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.LABOR_COSTS.getValue()))) {
                            waybill.setTotalShippingCost(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.LABOR_COSTS.getValue())).toString()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.TOTAL_OIL_COST.getValue()))) {
                            waybill.setTotalFuelCost(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.TOTAL_OIL_COST.getValue())).toString()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.REMAINING_OIL_FEE.getValue()))) {
                            waybill.setRemainingOilFee(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.REMAINING_OIL_FEE.getValue())).toString()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.MARKET_OIL_PRICE.getValue()))) {
                            waybill.setOilPercent(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.MARKET_OIL_PRICE.getValue())).toString()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.SHIP_DISTANCE.getValue()))) {
                            waybill.setDistance(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.SHIP_DISTANCE.getValue())).toString()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.PRICE_PER_KM.getValue()))) {
                            waybill.setPricePerKm(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.PRICE_PER_KM.getValue())).toString()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.FIRST_BUCKET_OIL_FEE.getValue()))) {
                            waybill.setFirstFuelOilFee(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.FIRST_BUCKET_OIL_FEE.getValue())).toString()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.FIRST_BUCKET_OIL.getValue()))) {
                            waybill.setFirstFuelTotal(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.FIRST_BUCKET_OIL.getValue())).toString()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.PRICE_OIL_PER_KM.getValue()))) {
                            waybill.setPriceOilPerKm(new BigDecimal(feeDetailsMap.get(Integer.valueOf(DWaybillFeeEnum.PRICE_OIL_PER_KM.getValue())).toString()));
                        }
                        //总油量
                        if (feeDetailsMap.containsKey(DWaybillFeeEnum.TOTAL_FUEL_CONSUMPTION.getValue())) {
                            waybill.setTotalFuelConsumption(new BigDecimal(feeDetailsMap.get(DWaybillFeeEnum.TOTAL_FUEL_CONSUMPTION.getValue()).toString()));
                        }
                    } catch (Exception e) {
                        LOGGER.error("dWaybillService setPrice is error:{} ,",e);
                    }

                    try {
                        //更新价格
                        this.dWaybillService.updateSelective(waybill);
                    } catch (Exception e) {
                        LOGGER.error("dWaybillService.updateSelective is error:{} ,",e);
                    }


                    try{
                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.SHIP_DISTANCE.getValue()))) {
                            feeDetailsMap.remove(Integer.valueOf(DWaybillFeeEnum.SHIP_DISTANCE.getValue()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.PRICE_PER_KM.getValue()))) {
                            feeDetailsMap.remove(Integer.valueOf(DWaybillFeeEnum.PRICE_PER_KM.getValue()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.FIRST_BUCKET_OIL_FEE.getValue()))) {
                            feeDetailsMap.remove(Integer.valueOf(DWaybillFeeEnum.FIRST_BUCKET_OIL_FEE.getValue()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.FIRST_BUCKET_OIL.getValue()))) {
                            feeDetailsMap.remove(Integer.valueOf(DWaybillFeeEnum.FIRST_BUCKET_OIL.getValue()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.FUEL_TYPE.getValue()))) {
                            feeDetailsMap.remove(Integer.valueOf(DWaybillFeeEnum.FUEL_TYPE.getValue()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.PRICE_OIL_PER_KM.getValue()))) {
                            feeDetailsMap.remove(Integer.valueOf(DWaybillFeeEnum.PRICE_OIL_PER_KM.getValue()));
                        }

                        if(feeDetailsMap.containsKey(Integer.valueOf(DWaybillFeeEnum.TOTAL_FUEL_CONSUMPTION.getValue()))) {
                            feeDetailsMap.remove(Integer.valueOf(DWaybillFeeEnum.TOTAL_FUEL_CONSUMPTION.getValue()));
                        }
                    } catch (Exception e) {
                        LOGGER.error("feeDetailsMap.remove is error:{} ,",e);
                    }

                    try{
                        feeDetailsMap.forEach((key, value) -> {
                            /**
                             * 组装保存运单费用详情
                             */
                            DWaybillFeeDetail feeDetail = new DWaybillFeeDetail();
                            feeDetail.setCostType(DWaybillFeeDetailCostTypeEnum.RETURN_COST.getText());
                            feeDetail.setWaybillId(waybill.getId());
                            feeDetail.setFeeId(dWaybillFeeTypeService.getIdByConditions(String.valueOf(key)));
                            feeDetail.setFeeName(DWaybillFeeEnum.getByValue(key).getText());
                            feeDetail.setCost((BigDecimal) value);
                            dWaybillFeeDetailService.add(feeDetail);
                        });
                    } catch (Exception e) {
                        LOGGER.error("dWaybillFeeDetailService.add is error:{} ,",e);
                    }
                }

            }
        }

    }


}
