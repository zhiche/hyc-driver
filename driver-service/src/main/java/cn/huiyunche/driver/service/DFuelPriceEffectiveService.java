package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DFuelPriceEffective;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.form.DFuelPriceEffectiveForm;
import cn.huiyunche.driver.service.query.DFuelPriceEffectiveQueryConditions;
import org.springframework.validation.BindingResult;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The interface D fuel price effective service.
 *
 * @FileName: cn.huiyunche.driver.service
 * @Description: 燃油价格变动
 * @author: Aaron
 * @date: 2017 /2/26 下午6:45
 */
public interface DFuelPriceEffectiveService {

    /**
     * Add integer.
     *
     * @param form the form
     * @param br   the br
     * @return the integer
     */
    Integer add(DFuelPriceEffectiveForm form, BindingResult br);

    /**
     * Del.
     *
     * @param id the id
     */
    void del(Integer id);

    /**
     * Select list by conditions map.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the map
     */
    Map<String, Object> selectListByConditions(PageVo pageVo, DFuelPriceEffectiveQueryConditions conditions);

    /**
     * Select not effective list.
     * 查询未生效的
     *
     * @param fuelTypeId the fuel type id
     * @return the list
     */
    List<DFuelPriceEffective> selectNotEffective(Integer fuelTypeId);

    /**
     * Select currently active d fuel price effective.
     * 查询上一个激活的记录的燃油价格变动信息
     *
     * @param fuelTypeId  the fuel type id
     * @param invalidDate the invalid date
     * @return the d fuel price effective
     */
    DFuelPriceEffective selectPreviousActive(Integer fuelTypeId, Date invalidDate);

    /**
     * Sellect by primary key d fuel price effective.
     *
     * @param id the id
     * @return the d fuel price effective
     */
    DFuelPriceEffective selectByPrimaryKey(Integer id);

    /**
     * Update.
     *
     * @param effective the current effective
     */
    void update(DFuelPriceEffective effective);

    /**
     * Select by fuel type id list.
     *
     * @param fuelTypeId the fuel type id
     * @return the list
     */
    List<DFuelPriceEffective> selectByFuelTypeId(Integer fuelTypeId);

    /**
     * Update integer.
     *
     * @param form the form
     * @param br   the br
     * @return the integer
     */
    Integer update(DFuelPriceEffectiveForm form, BindingResult br);

    /**
     * Select by effective date and fuel type id d fuel price effective.
     *
     * @param fuelTypeId    the fuel type id
     * @param effectiveDate the effective date
     * @return the d fuel price effective
     */
    DFuelPriceEffective selectByEffectiveDateAndFuelTypeId(Integer fuelTypeId, Date effectiveDate);
}
