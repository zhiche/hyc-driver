package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.TmsDDeliveryIssueMapper;
import cn.huiyunche.base.service.model.TmsDDeliveryIssue;
import cn.huiyunche.driver.service.TmsDDeliveryIssueService;
import cn.huiyunche.driver.service.form.TmsDDeliveryAuditForm;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by houjianhui on 2017/4/10.
 */

@Service
public class TmsDDeliveryIssueServiceImpl implements TmsDDeliveryIssueService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TmsDDeliveryIssueServiceImpl.class);

    @Autowired
    private TmsDDeliveryIssueMapper tmsDDeliveryIssueMapper;

    @Override
    public Long save(TmsDDeliveryAuditForm form) throws Exception {
        LOGGER.info("TmsDDeliveryIssueServiceImpl.save params form: {}", form);
        if (null == form) {
            LOGGER.info("TmsDDeliveryIssueServiceImpl.save form must not be null");
            throw new IllegalArgumentException("表单数据不能为空");
        }

        // 校验数据
        this.verification(form);

        TmsDDeliveryIssue issue = new TmsDDeliveryIssue();
        BeanUtils.copyProperties(form, issue);
        this.tmsDDeliveryIssueMapper.insertSelective(issue);

        return issue.getId();
    }

    private void verification(TmsDDeliveryAuditForm form) throws Exception {

        if (null == form.getIssueTypeId() || 0 == form.getIssueTypeId()) {
            LOGGER.error("TmsDDeliveryIssueServiceImpl.verification issueTypeId must not be null");
            throw new IllegalArgumentException("交车异常类型ID不能为空");
        } else if (StringUtils.isBlank(form.getIssueTypeContent())) {
            LOGGER.error("TmsDDeliveryIssueServiceImpl.verification issueTypeContent must not be null");
            throw new IllegalArgumentException("交车异常类型描述不能为空");
        } else if (StringUtils.isNotBlank(form.getIssueComment()) && form.getIssueComment().length() > 100) {
            LOGGER.error("TmsDDeliveryIssueServiceImpl.verification issueComment Length must be less than 100");
            throw new IllegalArgumentException("交车异常备注只能输入0～100字");
        }

    }
}
