package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.CrmCustomerServiceContact;

import java.util.List;

/**
 * 客服配置
 *
 * Created by houjianhui on 2017/4/24.
 */
public interface CrmCustomerServiceContactService {

    /**
     * Lists CrmCustomerServiceContact By clientType or appType
     *
     * @param clientType 客户端类型（10：微信，20：安卓，30：IOS）
     * @param appType 应用类型（10：司机，20：货主，30：人送车）
     * @return
     * @throws Exception
     */
    List<CrmCustomerServiceContact> listCrmCustomerServiceContact(String clientType, String appType) throws Exception;
}
