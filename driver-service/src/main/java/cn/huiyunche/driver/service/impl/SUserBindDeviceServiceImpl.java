package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.enums.SUserBindStatusEnum;
import cn.huiyunche.base.service.mappers.SUserBindDeviceMapper;
import cn.huiyunche.base.service.mappers.ext.SUserBindDeviceViewMapper;
import cn.huiyunche.base.service.model.SUserBindDevice;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.base.service.vo.SUserBindDeviceVo;
import cn.huiyunche.driver.service.SUserBindDeviceService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SUserBindDeviceServiceImpl implements SUserBindDeviceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SUserBindDeviceServiceImpl.class);

    @Autowired
    private SUserBindDeviceMapper sUserBindDeviceMapper = null;

    @Autowired
    private SUserBindDeviceViewMapper sUserBindDeviceViewMapper = null;

    private SUserBindDeviceViewMapper getSUserBindDeviceViewMapper() {
        return this.sUserBindDeviceViewMapper;
    }

    private SUserBindDeviceMapper getSUserBindDeviceMapper() {
        return this.sUserBindDeviceMapper;
    }

    @Override
    public Result<Object> selectSUserBindDeviceByExample(PageVo pageVo, String phone) throws BusinessException {
        LOGGER.info("selectSUserBindDeviceByExample params pageVo: {}, phone: {}", pageVo.toString(), phone);
        Result<Object> result = new Result<>(true, null, "数据加载成功");
        Map<String, Object> paramsMap = new HashMap<>();
        String orderByClause = StringUtils.isNotBlank(pageVo.getOrder()) == true ? pageVo.getOrder() : " subd.id DESC";
        paramsMap.put("limitStart", pageVo.getStartIndex());
        paramsMap.put("limitEnd", pageVo.getPageSize());
        paramsMap.put("orderByClause", orderByClause);
        if (StringUtils.isNotBlank(phone)) {
            paramsMap.put("phone", phone);
        }
        int record = this.getSUserBindDeviceViewMapper().countByExample(paramsMap);
        LOGGER.info("selectSUserBindDeviceByExample result record size: {}", record);
        pageVo.setTotalRecord(record);
        List<SUserBindDeviceVo> deviceVos = this.getSUserBindDeviceViewMapper().selectByExample(paramsMap);
        Map<String, Object> map = new HashMap<>();
        if (CollectionUtils.isNotEmpty(deviceVos)) {
            LOGGER.info("selectSUserBindDeviceByExample record size: {}", deviceVos.size());
            map.put("device", deviceVos);
        }
        map.put("page", pageVo);
        map.putIfAbsent("device", null);
        result.setData(map);
        return result;
    }

    @Override
    public Result<Object> updateBindStatus(Integer id) throws BusinessException {
        Result<Object> result = new Result<>(true, null, "账号设备解绑成功");
        LOGGER.info("updateBindStatus params id: {}", id);
        if (null == id || id == 0) {
            LOGGER.info("ID is null");
            throw new BusinessException("用户ID不能为空");
        }
        SUserBindDevice device = this.getSUserBindDeviceMapper().selectByPrimaryKey(id);
        if (device != null) {
            device.setBindStatus(SUserBindStatusEnum.F.getValue());
            int record = this.getSUserBindDeviceMapper().updateByPrimaryKeySelective(device);
            if (record == 0) {
                result.setSuccess(false);
                result.setMessage("账号设备解绑失败");
            }
        }
        return result;
    }

}
