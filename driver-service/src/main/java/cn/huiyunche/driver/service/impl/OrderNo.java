package cn.huiyunche.driver.service.impl;

/**
 * Created by zhaoguixin on 2017/6/27.
 */
public class OrderNo {

    private String vcorderno;

    public String getVcorderno() {
        return vcorderno;
    }

    public void setVcorderno(String vcorderno) {
        this.vcorderno = vcorderno == null ? null : vcorderno.trim();
    }

}
