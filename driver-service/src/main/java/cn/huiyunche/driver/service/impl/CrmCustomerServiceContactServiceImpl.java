package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.CrmCustomerServiceContactMapper;
import cn.huiyunche.base.service.model.CrmCustomerServiceContact;
import cn.huiyunche.base.service.model.CrmCustomerServiceContactExample;
import cn.huiyunche.driver.service.CrmCustomerServiceContactService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * Created by houjianhui on 2017/4/24.
 */
@Service
public class CrmCustomerServiceContactServiceImpl implements CrmCustomerServiceContactService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CrmCustomerServiceContactServiceImpl.class);

    @Autowired
    private CrmCustomerServiceContactMapper crmCustomerServiceContactMapper;

    @Override
    public List<CrmCustomerServiceContact> listCrmCustomerServiceContact(String clientType, String appType) throws Exception {
        LOGGER.info("CrmCustomerServiceContactServiceImpl.listCrmCustomerServiceContact params clientType: {}, appType: {}", clientType, appType);
        if (Objects.equals(clientType, null)) {
            LOGGER.info("CrmCustomerServiceContactServiceImpl.listCrmCustomerServiceContact params clientType must not be null");
            throw new IllegalArgumentException("客户端类型不能为空");
        } else if (Objects.equals(appType, null)) {
            LOGGER.info("CrmCustomerServiceContactServiceImpl.listCrmCustomerServiceContact params appType must not be null");
            throw new IllegalArgumentException("应用类型不能为空");
        }
        CrmCustomerServiceContactExample example = new CrmCustomerServiceContactExample();
        example.createCriteria().andClientTypeEqualTo(clientType).andAppTypeEqualTo(appType);
        List<CrmCustomerServiceContact> list = crmCustomerServiceContactMapper.selectByExample(example);
        if (CollectionUtils.isNotEmpty(list)) {
            LOGGER.info("listCrmCustomerServiceContact result {}", list);
            return list;
        }
        return null;
    }
}
