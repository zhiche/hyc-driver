package cn.huiyunche.driver.service.query;

/**
 * @FileName: cn.huiyunche.driver.service.query
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/26 下午5:03
 */
public class DFuelTypeQueryConditions {

    //燃油编码
    private String fuelCode;

    //燃油名称
    private String fuelName;

    public String getFuelCode() {
        return fuelCode;
    }

    public void setFuelCode(String fuelCode) {
        this.fuelCode = fuelCode;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    @Override
    public String toString() {
        return "DFuelTypeQueryConditions{" +
                "fuelCode='" + fuelCode + '\'' +
                ", fuelName='" + fuelName + '\'' +
                '}';
    }
}
