package cn.huiyunche.driver.service.form;

/**
 * Created by houjianhui on 2017/4/10.
 */
public class TmsDDeliveryAuditForm {

    private Long id;

    private Long waybillId;

    private Long deliveryId;

    private Long operatorId;

    private Long auditResultId;

    private String auditResult;

    private Long issueTypeId;

    private String issueTypeContent;

    private String issueComment;

    private Long issueId;

    private Long disposalTypeId;

    private String disposalContent;

    private Long auditId;

    private Integer queueTime;

    private String attachs;

    public String getAttachs() {
        return attachs;
    }

    public void setAttachs(String attachs) {
        this.attachs = attachs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(Long waybillId) {
        this.waybillId = waybillId;
    }

    public Long getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public Long getAuditResultId() {
        return auditResultId;
    }

    public void setAuditResultId(Long auditResultId) {
        this.auditResultId = auditResultId;
    }

    public String getAuditResult() {
        return auditResult;
    }

    public void setAuditResult(String auditResult) {
        this.auditResult = auditResult;
    }

    public Long getIssueTypeId() {
        return issueTypeId;
    }

    public void setIssueTypeId(Long issueTypeId) {
        this.issueTypeId = issueTypeId;
    }

    public String getIssueTypeContent() {
        return issueTypeContent;
    }

    public void setIssueTypeContent(String issueTypeContent) {
        this.issueTypeContent = issueTypeContent;
    }

    public String getIssueComment() {
        return issueComment;
    }

    public void setIssueComment(String issueComment) {
        this.issueComment = issueComment;
    }

    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }

    public Long getDisposalTypeId() {
        return disposalTypeId;
    }

    public void setDisposalTypeId(Long disposalTypeId) {
        this.disposalTypeId = disposalTypeId;
    }

    public String getDisposalContent() {
        return disposalContent;
    }

    public void setDisposalContent(String disposalContent) {
        this.disposalContent = disposalContent;
    }

    public Long getAuditId() {
        return auditId;
    }

    public void setAuditId(Long auditId) {
        this.auditId = auditId;
    }

    public Integer getQueueTime() {
        return queueTime;
    }

    public void setQueueTime(Integer queueTime) {
        this.queueTime = queueTime;
    }

    @Override
    public String toString() {
        return "TmsDDeliveryAuditForm{" +
                "id=" + id +
                ", waybillId=" + waybillId +
                ", deliveryId=" + deliveryId +
                ", operatorId=" + operatorId +
                ", auditResultId=" + auditResultId +
                ", auditResult='" + auditResult + '\'' +
                ", issueTypeId=" + issueTypeId +
                ", issueTypeContent='" + issueTypeContent + '\'' +
                ", issueComment='" + issueComment + '\'' +
                ", issueId=" + issueId +
                ", disposalTypeId=" + disposalTypeId +
                ", disposalContent='" + disposalContent + '\'' +
                ", auditId=" + auditId +
                ", queueTime=" + queueTime +
                ", attachs='" + attachs + '\'' +
                '}';
    }
}
