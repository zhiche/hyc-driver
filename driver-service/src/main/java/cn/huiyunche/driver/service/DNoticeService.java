package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DNotice;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.tools.basic.exceptions.BusinessException;

import java.util.Map;

public interface DNoticeService {


    /**
     * @param 通知类实体
     * @param 指定阅读人
     * @Title: 添加通知
     * @Description: TODO
     * @return: Long
     */
    Long addNotice(DNotice dNotice, Long user_id) throws BusinessException;

    /**
     * @param userid
     * @return
     * @Title: 根据用户选择通知
     * @Description: TODO
     * @return: List<DNoticeVo>
     */
    Map<String, Object> selectDNoticesByUserPage(PageVo page);

    /**
     * @param dNoticeId
     * @return
     * @Title: 根据ID选择通知
     * @Description: TODO
     * @return: DNotice
     */
    DNotice selectDNoticeByID(Long dNoticeId);

    /**
     * @param dNoticeId
     * @param userID
     * @throws BusinessException
     * @Title: 根据用户ID和通知ID更新通知
     * @Description: TODO
     * @return: void
     */
    void updateDNoticeStatusByID(Long dNoticeId) throws BusinessException;


    /**
     * @param userID
     * @return
     * @Title: 得到未读通知
     * @Description: TODO
     * @return: int
     */
    int getIsNotReadNotice(Long userID);


}
