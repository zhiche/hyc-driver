package cn.huiyunche.driver.service.form;

import cn.huiyunche.base.service.constant.RegularConstant;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.service.form
 * @Description: 车型线路价格变动表单
 * @author: Aaron
 * @date: 2017/2/26 下午1:00
 */
public class DRouteVehiclePriceEffectiveForm {

    private Integer id;

    //线路ID
    @NotNull(message = "请选择线路")
    @Min(value = 1, message = "线路id不能小于0")
    private Integer routeId;

    //车型ID
    @NotNull(message = "请选择车型类型")
    @Min(value = 1, message = "车型类型id不能小于0")
    private Integer vehicleTypeId;

    //劳务价
    @NotNull(message = "劳务价不能为空")
    @Pattern(regexp = RegularConstant.number_decimals, message = "劳务价只能为数字且为正数（保留两位小数）")
//    @DecimalMin(value = "0.01", message = "劳务价不能小于0元")
    private BigDecimal labourServicesPrice;

    //总运价
    @NotNull(message = "总运价不能为空")
    @Pattern(regexp = RegularConstant.number_decimals, message = "总运价只能为数字且为正数（保留两位小数）")
    @DecimalMin(value = "0.01", message = "总运价不能小于0元")
    private BigDecimal totalPrice;

    //生效时间
    @NotNull(message = "生效时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Future(message = "生效时间必须是一个未来的时间")
    private Date effectiveDate;

    //失效时间
    private Date invalidDate;

    //创建时间
    private Date createTime;

    //创建人
    private String creator;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Integer getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public BigDecimal getLabourServicesPrice() {
        return labourServicesPrice;
    }

    public void setLabourServicesPrice(BigDecimal labourServicesPrice) {
        this.labourServicesPrice = labourServicesPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getInvalidDate() {
        return invalidDate;
    }

    public void setInvalidDate(Date invalidDate) {
        this.invalidDate = invalidDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Override
    public String toString() {
        return "DRouteVehiclePriceEffectiveForm{" +
                "id=" + id +
                ", routeId=" + routeId +
                ", vehicleTypeId=" + vehicleTypeId +
                ", labourServicesPrice=" + labourServicesPrice +
                ", totalPrice=" + totalPrice +
                ", effectiveDate=" + effectiveDate +
                ", invalidDate=" + invalidDate +
                ", createTime=" + createTime +
                ", creator='" + creator + '\'' +
                '}';
    }
}
