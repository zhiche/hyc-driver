package cn.huiyunche.driver.service.form;

import cn.huiyunche.base.service.constant.RegularConstant;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.service.form
 * @Description: 线路表单和查询条件
 * @author: Aaron
 * @date: 2017/2/26 上午11:32
 */
public class DRouteForm {

    private Integer id;

    //线路名称
    @NotBlank(message = "请输入线路名称")
    //@Pattern(regexp = RegularConstant.special, message = "线路名称不能包含特殊字符")
    @Size(max = 16, min = 1, message = "燃油编码长度位1-16位")
    private String name;

    //起始地关键字
    @NotBlank(message = "起始地关键字不能为空")
    @Pattern(regexp = RegularConstant.special, message = "起始地关键字不能包含特殊字符")
    @Size(max = 16, min = 1, message = "燃油编码长度位1-16位")
    private String oTag;

    //起点省编码
    private String oProvinceCode;

    //起点省份
    @NotBlank(message = "起始地省份不能为空")
    @Pattern(regexp = RegularConstant.special, message = "起点省份不能包含特殊字符")
    @Size(max = 16, min = 1, message = "燃油编码长度位1-16位")
    private String oProvince;

    //起点市编码
    private String oCityCode;

    //起点城市
//    @NotBlank(message = "起始地城市不能为空")
    private String oCity;

    //起点县编码
    private String oCountyCode;

    //起点区县
//    @NotBlank(message = "起始地区/县不能为空")
    private String oCounty;

    //起点网点ID
    private Integer oDepotId;

    //起点网点
    private String oDepot;

    //起点详细地址
//    @NotBlank(message = "起始地详细地址不能为空")
    private String oAddr;

    //目的地关键字
    @NotBlank(message = "目的地关键字不能为空")
    @Pattern(regexp = RegularConstant.special, message = "目的地关键字不能包含特殊字符")
    @Size(max = 16, min = 1, message = "燃油编码长度位1-16位")
    private String dTag;

    //终点省编码
    private String dProvinceCode;

    //终点省份
    @NotBlank(message = "目的地省份不能为空")
    @Pattern(regexp = RegularConstant.special, message = "终点省份不能包含特殊字符")
    @Size(max = 16, min = 1, message = "燃油编码长度位1-16位")
    private String dProvince;

    //终点市编码
    private String dCityCode;

    //终点城市
//    @NotBlank(message = "目的地城市不能为空")
    private String dCity;

    //终点县编码
    private String dCountyCode;

    //终点区县
//    @NotBlank(message = "目的地区/县不能为空")
    private String dCounty;

    //终点网点ID
    private Integer dDepotId;

    //终点网点
    private String dDepot;

    //终点详细地址
//    @NotBlank(message = "目的地详细地址不能为空")
    private String dAddr;

    //地图里程
//    @Pattern(regexp = RegularConstant.number_decimals, message = "地图里程只能为正数")
//    private BigDecimal mapDistance;

    //上期里程
//    @Pattern(regexp = RegularConstant.number_decimals, message = "上期里程只能为正数")
//    private BigDecimal previousDistance;

    //当前里程
//    @NotNull(message = "里程不能为空")
//    @Pattern(regexp = RegularConstant.number_decimals, message = "当前里程只能为数字且为正数（保留两位小数）")
//    @DecimalMin(value = "0.01", message = "里程不能小于0公里")
//    private BigDecimal currentDistance;

    //状态
    private Boolean enable;

    //创建时间
    private Date createTime;

    //创建人
    private String creator;

    //更新时间
    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public DRouteForm setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public DRouteForm setName(String name) {
        this.name = name;
        return this;
    }

    public String getOTag() {
        return oTag;
    }

    public DRouteForm setOTag(String oTag) {
        this.oTag = oTag;
        return this;
    }

    public String getOProvinceCode() {
        return oProvinceCode;
    }

    public DRouteForm setOProvinceCode(String oProvinceCode) {
        this.oProvinceCode = oProvinceCode;
        return this;
    }

    public String getOProvince() {
        return oProvince;
    }

    public DRouteForm setOProvince(String oProvince) {
        this.oProvince = oProvince;
        return this;
    }

    public String getOCityCode() {
        return oCityCode;
    }

    public DRouteForm setOCityCode(String oCityCode) {
        this.oCityCode = oCityCode;
        return this;
    }

    public String getOCity() {
        return oCity;
    }

    public DRouteForm setOCity(String oCity) {
        this.oCity = oCity;
        return this;
    }

    public String getOCountyCode() {
        return oCountyCode;
    }

    public DRouteForm setOCountyCode(String oCountyCode) {
        this.oCountyCode = oCountyCode;
        return this;
    }

    public String getOCounty() {
        return oCounty;
    }

    public DRouteForm setOCounty(String oCounty) {
        this.oCounty = oCounty;
        return this;
    }

    public Integer getODepotId() {
        return oDepotId;
    }

    public DRouteForm setODepotId(Integer oDepotId) {
        this.oDepotId = oDepotId;
        return this;
    }

    public String getODepot() {
        return oDepot;
    }

    public DRouteForm setODepot(String oDepot) {
        this.oDepot = oDepot;
        return this;
    }

    public String getOAddr() {
        return oAddr;
    }

    public DRouteForm setOAddr(String oAddr) {
        this.oAddr = oAddr;
        return this;
    }

    public String getDTag() {
        return dTag;
    }

    public DRouteForm setDTag(String dTag) {
        this.dTag = dTag;
        return this;
    }

    public String getDProvinceCode() {
        return dProvinceCode;
    }

    public DRouteForm setDProvinceCode(String dProvinceCode) {
        this.dProvinceCode = dProvinceCode;
        return this;
    }

    public String getDProvince() {
        return dProvince;
    }

    public DRouteForm setDProvince(String dProvince) {
        this.dProvince = dProvince;
        return this;
    }

    public String getDCityCode() {
        return dCityCode;
    }

    public DRouteForm setDCityCode(String dCityCode) {
        this.dCityCode = dCityCode;
        return this;
    }

    public String getDCity() {
        return dCity;
    }

    public DRouteForm setDCity(String dCity) {
        this.dCity = dCity;
        return this;
    }

    public String getDCountyCode() {
        return dCountyCode;
    }

    public DRouteForm setDCountyCode(String dCountyCode) {
        this.dCountyCode = dCountyCode;
        return this;
    }

    public String getDCounty() {
        return dCounty;
    }

    public DRouteForm setDCounty(String dCounty) {
        this.dCounty = dCounty;
        return this;
    }

    public Integer getDDepotId() {
        return dDepotId;
    }

    public DRouteForm setDDepotId(Integer dDepotId) {
        this.dDepotId = dDepotId;
        return this;
    }

    public String getDDepot() {
        return dDepot;
    }

    public DRouteForm setDDepot(String dDepot) {
        this.dDepot = dDepot;
        return this;
    }

    public String getDAddr() {
        return dAddr;
    }

    public DRouteForm setDAddr(String dAddr) {
        this.dAddr = dAddr;
        return this;
    }

    public Boolean getEnable() {
        return enable;
    }

    public DRouteForm setEnable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public DRouteForm setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getCreator() {
        return creator;
    }

    public DRouteForm setCreator(String creator) {
        this.creator = creator;
        return this;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public DRouteForm setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "DRouteForm{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", oTag='" + oTag + '\'' +
                ", oProvinceCode='" + oProvinceCode + '\'' +
                ", oProvince='" + oProvince + '\'' +
                ", oCityCode='" + oCityCode + '\'' +
                ", oCity='" + oCity + '\'' +
                ", oCountyCode='" + oCountyCode + '\'' +
                ", oCounty='" + oCounty + '\'' +
                ", oDepotId=" + oDepotId +
                ", oDepot='" + oDepot + '\'' +
                ", oAddr='" + oAddr + '\'' +
                ", dTag='" + dTag + '\'' +
                ", dProvinceCode='" + dProvinceCode + '\'' +
                ", dProvince='" + dProvince + '\'' +
                ", dCityCode='" + dCityCode + '\'' +
                ", dCity='" + dCity + '\'' +
                ", dCountyCode='" + dCountyCode + '\'' +
                ", dCounty='" + dCounty + '\'' +
                ", dDepotId=" + dDepotId +
                ", dDepot='" + dDepot + '\'' +
                ", dAddr='" + dAddr + '\'' +
                ", enable=" + enable +
                ", createTime=" + createTime +
                ", creator='" + creator + '\'' +
                ", updateTime=" + updateTime +
                '}';
    }
}
