package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DRouteVehiclePriceEffective;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.form.DRouteVehiclePriceEffectiveForm;
import cn.huiyunche.driver.service.query.DRouteVehiclePriceEffectiveQueryConditions;
import org.springframework.validation.BindingResult;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The interface D route vehicle price effective service.
 *
 * @FileName: cn.huiyunche.driver.service
 * @Description: 车型线路价格变动
 * @author: Aaron
 * @date: 2017 /2/26 下午12:55
 */
public interface DRouteVehiclePriceEffectiveService {

    /**
     * Add integer.
     *
     * @param form the form
     * @param br   the br
     * @return the integer
     */
    Integer add(DRouteVehiclePriceEffectiveForm form, BindingResult br);

    /**
     * Update integer.
     *
     * @param form the form
     * @param br   the br
     * @return the integer
     */
    Integer update(DRouteVehiclePriceEffectiveForm form, BindingResult br);

    /**
     * Select by primary key d route vehicle price effective.
     *
     * @param id the id
     * @return the d route vehicle price effective
     */
    DRouteVehiclePriceEffective selectByPrimaryKey(Integer id);

    /**
     * Delete integer.
     *
     * @param ids the ids
     * @return the integer
     */
    void delete(String ids);

    /**
     * Select list by conditions map.
     *
     * @param pageVo     the page vo
     * @param conditions the form
     * @return the map
     */
    Map<String, Object> selectListByConditions(PageVo pageVo, DRouteVehiclePriceEffectiveQueryConditions conditions);

    /**
     * Select not effective list.
     * 查询未生效的
     *
     * @param routeId       the route id
     * @param vehicleTypeId the vehicle type id
     * @return the list
     */
    List<DRouteVehiclePriceEffective> selectNotEffective(Integer routeId, Integer vehicleTypeId);

    /**
     * Select currently active d route vehicle price effective.
     * 查询当前正在生效的车型线路价格信息
     *
     * @return the d route vehicle price effective
     */
    DRouteVehiclePriceEffective selectCurrentlyActive();

    /**
     * Update.
     *
     * @param effective the effective
     */
    void update(DRouteVehiclePriceEffective effective);

    /**
     * Calc total price big decimal.
     *
     * @param vehicleTypeId       the vehicle type id
     * @param labourServicesPrice the labour services price
     * @param effectiveDate       the effective date
     * @return the big decimal
     */
    BigDecimal calcTotalPrice(Integer vehicleTypeId, BigDecimal labourServicesPrice, Date effectiveDate);

    /**
     * Select DRouteVehiclePriceEffective By Id
     *
     * @param id
     * @return
     */
    DRouteVehiclePriceEffective getById(Integer id);
}
