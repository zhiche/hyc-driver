package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.enums.DWaybillStatusEnum;
import cn.huiyunche.base.service.enums.UserStatusEnum;
import cn.huiyunche.base.service.enums.UserTypeEnum;
import cn.huiyunche.base.service.interfaces.DWaybillService;
import cn.huiyunche.base.service.interfaces.TmsQueueService;
import cn.huiyunche.base.service.mappers.SUserMapper;
import cn.huiyunche.base.service.mappers.ext.DWaybillViewMapper;
import cn.huiyunche.base.service.model.SUser;
import cn.huiyunche.base.service.utils.DateUtils;
import cn.huiyunche.base.service.vo.ShowOrderVo;
import cn.huiyunche.base.service.vo.ShowQueueVo;
import cn.huiyunche.base.service.vo.TmsQueueDriverRtnVo;
import cn.huiyunche.base.service.vo.UserShowVo;
import cn.huiyunche.driver.service.SDUserService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: Description
 * @author: Aaron
 * @date: 2017/1/20 下午1:20
 */
@Service
public class SDUserServiceImpl implements SDUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SDUserServiceImpl.class);

    @Autowired
    private SUserMapper sUserMapper;

    @Autowired
    private DWaybillService dWaybillService;

    @Autowired
    private TmsQueueService tmsQueueService;

    @Autowired
    private DWaybillViewMapper dWaybillViewMapper;

    @Override
    public Map<String, Object> online(UserShowVo userVo) throws Exception {
        Map<String, Object> map = new HashMap<>();
        if (userVo == null) {
            LOGGER.info("current user is null");
            throw new BusinessException("获取当前用户信息异常");
        }
        if (userVo.getUserType() != UserTypeEnum.SEND_DRIVER.getValue()) {
            LOGGER.info("司机不属于人送司机");
            throw new BusinessException("您不属于人送司机，不可上线排队");
        }
        SUser user = new SUser();
        user.setId(userVo.getId());
        user.setUserStatus(UserStatusEnum.T.getValue());
        int res = 0;
        try {
            // 更新用户状态
            res = sUserMapper.updateByPrimaryKeySelective(user);
        } catch (Exception e) {
            LOGGER.error("updateByPrimaryKeySelective error : {}", e);
            throw new BusinessException("上线失败");
        }
        if (res > 0) {
            ShowQueueVo sqv = new ShowQueueVo();
            // 查询运单信息
            List<Integer> statusList = new ArrayList<>();
            statusList.add(DWaybillStatusEnum.DEPARTURE.getValue());
            statusList.add(DWaybillStatusEnum.INTRANSIT.getValue());
            statusList.add(DWaybillStatusEnum.DEAL_CAR.getValue());
            ShowOrderVo sov = dWaybillViewMapper.selectOrderInfo(userVo.getId(), statusList);
            if (sov != null) {
                if (sov.getOrderTime() != null) {
                    String dt = DateUtils.getStringFromDate(sov.getOrderTime(), "yyyy-MM-dd");
                    String current = DateUtils.getStringFromDate(new Date(), "yyyy-MM-dd");
                    String time = DateUtils.getStringFromDate(sov.getOrderTime(), "yyyy-MM-dd HH:mm");
                    if (dt.equals(current)) {
                        sov.setShowWaybillDate("今日运单");
                        sov.setShowDate("今天 " + time.substring(11, 16));
                        sov.setShowTime(time.substring(11, 16));
                    } else {
                        sov.setShowWaybillDate(dt);
                        sov.setShowDate(dt + " " + time.substring(11, 16));
                        sov.setShowTime(time.substring(11, 16));
                    }
                }
            } else {
                tmsQueueService.appendDriver(userVo.getPhone());
                // 获取排队中的数据
                TmsQueueDriverRtnVo tmsDV = tmsQueueService.getMyself(userVo.getPhone());
                if (tmsDV != null) {
                    sqv.setFrontNumber(tmsDV.getIndex());
                    sqv.setOrderNumber(tmsDV.getOrderNumber());
                    sqv.setRankNumber(tmsDV.getNubmer());
                }
            }
            map.put("queue", sqv);
            map.put("order", sov);
        }
        return map;
    }
}
