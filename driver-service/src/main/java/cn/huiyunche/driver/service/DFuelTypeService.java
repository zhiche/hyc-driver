package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DFuelType;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.form.DFuelTypeForm;
import cn.huiyunche.driver.service.query.DFuelTypeQueryConditions;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Map;

/**
 * The interface D fuel type service.
 *
 * @FileName: cn.huiyunche.driver.service
 * @Description: 线路类型
 * @author: Aaron
 * @date: 2017 /2/26 下午4:59
 */
public interface DFuelTypeService {

    /**
     * Add integer.
     *
     * @param form the form
     * @param br   the br
     * @return the integer
     */
    Integer add(DFuelTypeForm form, BindingResult br) throws Exception;

    /**
     * Select by primary key d fuel type.
     *
     * @param id the id
     * @return the d fuel type
     */
    DFuelType selectByPrimaryKey(Integer id) throws Exception;

    /**
     * Update integer.
     *
     * @param form the form
     * @param br   the br
     * @return the integer
     */
    Integer update(DFuelTypeForm form, BindingResult br) throws Exception;

    /**
     * Delete.
     *
     * @param id the id
     */
    void delete(Integer id) throws Exception;

    /**
     * Select list by conditions map.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the map
     */
    Map<String, Object> selectListByConditions(PageVo pageVo, DFuelTypeQueryConditions conditions) throws Exception;

    /**
     * Select list by conditions list.
     *
     * @param conditions the conditions
     * @return the list
     */
    List<DFuelType> selectListByConditions(DFuelTypeQueryConditions conditions) throws Exception;

    /**
     * Verification code or name unique list.
     *
     * @param id
     * @param fuelName the fuel name
     * @param fuelCode the fuel code
     * @return the list
     */
    void verificationCodeOrNameUnique(Integer id, String fuelName, String fuelCode) throws Exception;

}
