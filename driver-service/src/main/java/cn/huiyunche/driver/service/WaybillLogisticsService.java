package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DWaybillLogistics;

import java.util.List;

public interface WaybillLogisticsService {

    /**
     * 查询在途信息
     *
     * @param logistics
     * @return
     */
    List<DWaybillLogistics> listByConditions(DWaybillLogistics logistics);

}
