package cn.huiyunche.driver.service.enums;

/**
 * 交车异常处理枚举
 *
 * @author hjh
 */
public enum DeliveryIssueDisposalTypeEnum {

    PASS(10, "警告"),
    ISSUE_FORBIDDEN_QUEUE(20, "不可排队");

    private final int value;
    private final String text;

    DeliveryIssueDisposalTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static DeliveryIssueDisposalTypeEnum getByValue(int value) {
        for (DeliveryIssueDisposalTypeEnum temp : DeliveryIssueDisposalTypeEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }
}
