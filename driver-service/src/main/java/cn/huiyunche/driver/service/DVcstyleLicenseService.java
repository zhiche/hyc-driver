package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DVcstyleLicense;
import cn.huiyunche.base.service.vo.DVcstyleLicenseVo;
import cn.huiyunche.base.service.vo.PageVo;

import java.util.Map;

/**
 * Created by jxy on 16/12/14.
 */
public interface DVcstyleLicenseService {

    /**
     * 根据车型编号获得车型准驾实体
     *
     * @param vcCode 车型编号
     * @return
     */
    public DVcstyleLicense getVcStyleByStyleCode(String vcCode);

    /**
     * 添加准驾车型
     *
     * @param dVcstyleLicense
     */
    public void addDvcStyleLicense(DVcstyleLicense dVcstyleLicense);

    /**
     * 修改准驾车型
     *
     * @param dVcstyleLicense
     */
    public void modifyDvcStyleLicense(DVcstyleLicense dVcstyleLicense);

    /**
     * 校验准驾车型的车型编号是否已经存在
     *
     * @param styleCode
     * @return
     */
    public boolean validStyleCodeExist(String styleCode);

    /**
     * 删除准驾车型
     *
     * @param id
     */
    public void deleteDvcStyleLicense(String id);

    /**
     * 获取准驾车型列表
     *
     * @return
     */
    public Map<String, Object> getListByPage(PageVo page, DVcstyleLicenseVo dVcstyleLicenseVo);

}
