package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DRoute;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.dto.DestplaceDto;
import cn.huiyunche.driver.service.dto.StartplaceDto;
import cn.huiyunche.driver.service.form.DRouteForm;
import cn.huiyunche.driver.service.query.DRouteQueryConditions;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Map;

/**
 * The interface D route service.
 *
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: 线路
 * @author: Aaron
 * @date: 2017 /2/26 上午11:22
 */
public interface DRouteService {

    /**
     * Add integer.
     *
     * @param form the form
     * @param br   the br
     * @return the integer
     */
    Integer add(DRouteForm form, BindingResult br);


    /**
     * Select by primary key d route.
     *
     * @param id the id
     * @return the d route
     */
    DRoute selectByPrimaryKey(Integer id);

    /**
     * Update integer.
     *
     * @param form the form
     * @param br   the br
     * @return the integer
     */
    Integer update(DRouteForm form, BindingResult br);

    /**
     * Enable or disabled integer.
     *
     * @param id               the id
     * @param enableOrDisabled the enable or disabled
     * @return the integer
     */
    Integer enableOrDisabled(Integer id, boolean enableOrDisabled);

    /**
     * Select list by conditions map.
     *
     * @param pageVo     the page vo
     * @param conditions the form
     * @return the map
     */
    Map<String, Object> selectListByConditions(PageVo pageVo, DRouteQueryConditions conditions);

    /**
     * Verification o tag and d tag and name unique list.
     *
     * @param oTag      the o tag
     * @param dTag      the d tag
     * @param routeName the route name
     * @return the list
     */
    List<DRoute> verificationOTagAndDTagAndNameUnique(String oTag, String dTag, String routeName);

    /**
     * Del.
     *
     * @param id the id
     */
    void del(Integer id) throws Exception;

    /**
     * 根据目的地模糊查询线路
     * @param dTag
     * @return
     */
    List<DestplaceDto> findByDestTag(String dTag);

    /**
     * 根据起始地模糊查询线路
     * @param oTag
     * @return
     */
    List<StartplaceDto> findByStartTag(String oTag);

}
