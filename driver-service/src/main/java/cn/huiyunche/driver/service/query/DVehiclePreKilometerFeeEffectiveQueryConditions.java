package cn.huiyunche.driver.service.query;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.service.query
 * @Description: 车型单公里油费变动查询条件
 * @author: Aaron
 * @date: 2017/2/26 下午9:08
 */
public class DVehiclePreKilometerFeeEffectiveQueryConditions {

    //车型类型主键
    @Min(value = 1, message = "请选择车型")
    private Integer vehicleTypeId;

    //燃油价格生效变动主键
    @Min(value = 1, message = "请选择燃油价格生效变动")
    private Integer fuelPriceId;

    //生效时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date effectiveDate;

    //失效时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date invalidDate;

    public Integer getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public Integer getFuelPriceId() {
        return fuelPriceId;
    }

    public void setFuelPriceId(Integer fuelPriceId) {
        this.fuelPriceId = fuelPriceId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getInvalidDate() {
        return invalidDate;
    }

    public void setInvalidDate(Date invalidDate) {
        this.invalidDate = invalidDate;
    }

    @Override
    public String toString() {
        return "DVehiclePreKilometerFeeEffectiveQueryConditions{" +
                "vehicleTypeId=" + vehicleTypeId +
                ", fuelPriceId=" + fuelPriceId +
                ", effectiveDate=" + effectiveDate +
                ", invalidDate=" + invalidDate +
                '}';
    }
}
