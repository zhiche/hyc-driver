package cn.huiyunche.driver.service.enums;

/**
 * 交车审核枚举
 *
 * @author hjh
 */
public enum DeliveryAuditEnum {

    PASS(10, "通过"),
    ISSUE_APPROVE_QUEUE(20, "交车异常但可排队"),
    ISSUE_FORBIDDEN_QUEUE(30, "交车异常不可排队");

    private final int value;
    private final String text;

    DeliveryAuditEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static DeliveryAuditEnum getByValue(int value) {
        for (DeliveryAuditEnum temp : DeliveryAuditEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }
}
