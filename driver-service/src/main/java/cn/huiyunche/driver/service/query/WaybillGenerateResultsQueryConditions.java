package cn.huiyunche.driver.service.query;

/**
 * @FileName: cn.huiyunche.driver.service.query
 * @Description: 运单生成结果信息查询条件
 * @author: Aaron
 * @date: 2017/3/14 下午9:38
 */
public class WaybillGenerateResultsQueryConditions {

    //流水号
    private String orderLineId;

    //订单编号
    private String orderCode;

    //是否处理成功
    private Boolean isGenerated;

    //错误信息
    private String errMsg;

    public String getOrderLineId() {
        return orderLineId;
    }

    public WaybillGenerateResultsQueryConditions setOrderLineId(String orderLineId) {
        this.orderLineId = orderLineId;
        return this;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public WaybillGenerateResultsQueryConditions setOrderCode(String orderCode) {
        this.orderCode = orderCode;
        return this;
    }

    public Boolean getIsGenerated() {
        return isGenerated;
    }

    public WaybillGenerateResultsQueryConditions setIsGenerated(Boolean isGenerated) {
        this.isGenerated = isGenerated;
        return this;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public WaybillGenerateResultsQueryConditions setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }

    @Override
    public String toString() {
        return "WaybillGenerateResultsQueryConditions{" +
                "orderLineId='" + orderLineId + '\'' +
                ", orderCode='" + orderCode + '\'' +
                ", isGenerated=" + isGenerated +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}
