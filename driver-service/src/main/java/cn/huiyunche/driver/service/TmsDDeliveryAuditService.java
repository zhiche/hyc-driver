package cn.huiyunche.driver.service;

import cn.huiyunche.driver.service.form.TmsDDeliveryAuditForm;

/**
 * Created by houjianhui on 2017/4/10.
 */
public interface TmsDDeliveryAuditService {

    /**
     * Add TmsDDeliveryAudit
     *
     * @param form
     * @return
     * @throws Exception
     */
    Long save(TmsDDeliveryAuditForm form) throws Exception;

    /**
     * update Waybill Status
     *
     * @throws Exception
     */
    void updateWaybillStatus() throws Exception;
}
