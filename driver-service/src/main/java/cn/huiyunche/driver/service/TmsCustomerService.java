package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.BLicenseType;
import cn.huiyunche.base.service.model.DWaybill;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.base.service.vo.TmsDriverVo;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 中联客服接口
 *
 * @author hdy [Tuffy]
 */
public interface TmsCustomerService {

    /**
     * 登录
     *
     * @param phone    手机号
     * @param password 密码
     * @return 页面 result
     */
    public Result<String> login(HttpServletRequest request, String phone, String password);

    /**
     * 退出登录
     *
     * @return
     */
    public Result<String> logout(HttpServletRequest request);

    /**
     * 首页
     *
     * @return 页面 model and view
     */
    public ModelAndView index(HttpServletRequest request);

    /**
     * 开通账号
     *
     * @param request
     * @param phone
     * @return
     */
    public Result<String> openAccount(HttpServletRequest request, String phone);

    /**
     * 修改用户手机号，并开通
     *
     * @param request 请求
     * @param idCard  身份证
     * @param phone   手机号
     * @return 结果集
     */
    public Result<String> exchangeAccount(HttpServletRequest request, String idCard, String phone);

    /**
     * 根据身份证获取用户信息
     *
     * @param request 请求
     * @param idCard  省份证
     * @return 结结果集
     */
    public Result<String> getAccountByIdNo(HttpServletRequest request, String idCard);

    /**
     * 禁用账号
     *
     * @param request
     * @param phone
     * @return
     */
    public Result<String> disableAccount(HttpServletRequest request, String phone);

    /**
     * Cancel d waybill result.
     *
     * @param orderCode the order code
     * @return the result
     */
    public Result<String> cancelDWaybillWithUser(HttpServletRequest request, String orderCode, String cancelreason, String cancelflag);

    /**
     * 置顶队列订单
     *
     * @param request    请求
     * @param orderCodes 订单队列
     * @return 结果集
     */
    public Result<String> topOrder(HttpServletRequest request, String orderCodes) throws Exception;

    /**
     * 异常回单
     *
     * @param request    请求
     * @param orderCodes 订单
     * @return 结果集
     */
    public Result<String> exceptionConfirmResult(HttpServletRequest request, String orderCodes);

    /**
     * 队列订单列表
     *
     * @return 结果集
     */
    public Result<Object> queueOrderList(HttpServletRequest request) throws Exception;

    /**
     * 队列司机列表
     *
     * @return 结果集
     */
    public Result<Object> queueDriverList(HttpServletRequest request) throws Exception;

    /**
     * 回单
     *
     * @param request    请求
     * @param phone      手机号
     * @param orderCodes 订单号
     * @return 结果集
     */
    public Result<String> receipt(HttpServletRequest request, String phone, String orderCodes) throws Exception;

    /**
     * 退单
     *
     * @param request
     * @param orderCode
     * @return
     */
    public Result<Object> calcenList(HttpServletRequest request, String orderCode);


    /**
     * 添加司机
     *
     * @param tdv 司机表单
     * @return 结果集
     */
    public Result<String> addDriver(TmsDriverVo tdv);

    /**
     * 获取驾照类型列表
     *
     * @return 结果集
     */
    public Result<List<BLicenseType>> getBLicenseList();

    /**
     * 订单重新入队
     *
     * @param orderCode
     * @return
     * @throws BusinessException
     */
    public Result<String> addOrderToRedis(HttpServletRequest request, String orderCode) throws Exception;

    /**
     * 查询订单信息
     *
     * @param request
     * @param orderCode
     * @return
     */
    public Result<Object> queryOrder(HttpServletRequest request, String orderCode);

    /**
     * Select Users by Phone
     *
     * @param request
     * @param phone
     * @return
     */
    public Result<Object> userList(HttpServletRequest request, String phone);

    /**
     * 自动任务取消中联订单
     */
    public Result<String> cancelTMSbyIDCard(DWaybill waybill);
}
