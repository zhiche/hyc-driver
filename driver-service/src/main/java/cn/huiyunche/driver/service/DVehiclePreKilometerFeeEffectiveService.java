package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DVehiclePreKilometerFeeEffective;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.query.DVehiclePreKilometerFeeEffectiveQueryConditions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The interface D vehicle pre kilometer fee effective service.
 *
 * @FileName: cn.huiyunche.driver.service
 * @Description: 车型每公里油费变动
 * @author: Aaron
 * @date: 2017 /2/26 下午9:00
 */
public interface DVehiclePreKilometerFeeEffectiveService {

    /**
     * Add integer.
     *
     * @param dVehiclePreKilometerFeeEffective the d vehicle pre kilometer fee effective
     * @return the integer
     */
    Integer add(DVehiclePreKilometerFeeEffective dVehiclePreKilometerFeeEffective);

    /**
     * Add integer.
     *
     * @param fuelTypeId           the fuel type id
     * @param fuelPriceEffectiveId the fuel price effective id
     * @param marketPrice          the market price
     * @param effectiveDate        the effective date   @return the integer
     */
    void automaticGeneration(Integer fuelTypeId, Integer fuelPriceEffectiveId, BigDecimal marketPrice, Date effectiveDate);

    /**
     * Select list by conditions map.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the map
     */
    Map<String, Object> selectListByConditions(PageVo pageVo, DVehiclePreKilometerFeeEffectiveQueryConditions conditions);

    /**
     * Select not effective list.
     * 查询未激活的车型每公里油费变动
     *
     * @param vehicleTypeId the vehicle type id
     * @return the list
     */
    List<DVehiclePreKilometerFeeEffective> selectNotEffective(Integer vehicleTypeId);

    /**
     * Select currently active d vehicle pre kilometer fee effective.
     * 查询上一个激活的记录的车型每公里油费变动
     *
     * @param vehicleTypeId the vehicle type id
     * @param invalidDate   the invalid date
     * @return the d vehicle pre kilometer fee effective
     */
    DVehiclePreKilometerFeeEffective selectPreviousActive(Integer vehicleTypeId, Date invalidDate);

    /**
     * Update.
     *
     * @param feeEffective the fee effective
     */
    void update(DVehiclePreKilometerFeeEffective feeEffective);

    /**
     * Select list by conditions list.
     *
     * @param conditions the conditions
     * @return the list
     */
    List<DVehiclePreKilometerFeeEffective> selectListByConditions(DVehiclePreKilometerFeeEffectiveQueryConditions conditions);

    /**
     * Update by d fuel price effective.
     * 燃油价格变动信息更新时更新车型每公里油费
     *
     * @param fuelPriceEffectiveId the fuel price id
     * @param marketPrice          the market price
     * @param effectiveDate        the effective date
     */
    void updateByDFuelPriceEffective(Integer fuelPriceEffectiveId, BigDecimal marketPrice, Date effectiveDate);

    /**
     * Select actived by effective date d vehicle pre kilometer fee effective.
     *
     * @param vehicleTypeId the vehicle type id
     * @param effectiveDate the effective date
     * @return the d vehicle pre kilometer fee effective
     */
    DVehiclePreKilometerFeeEffective selectActivedByEffectiveDate(Integer vehicleTypeId, Date effectiveDate);
}
