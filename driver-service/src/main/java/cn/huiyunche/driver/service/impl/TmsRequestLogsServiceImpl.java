package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.DRequestLogsMapper;
import cn.huiyunche.base.service.model.DRequestLogs;
import cn.huiyunche.base.service.model.DRequestLogsWithBLOBs;
import cn.huiyunche.driver.service.TmsRequestLogsService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 日志实现类
 *
 * @author hdy [Tuffy]
 */
@Service
public class TmsRequestLogsServiceImpl implements TmsRequestLogsService {

    @Autowired
    private DRequestLogsMapper dRequestLogsMapper = null;

    @Override
    public DRequestLogs addLog(String orderCode, int type, JSONObject params) {
        DRequestLogsWithBLOBs drlwb = new DRequestLogsWithBLOBs();
        drlwb.setCreateTime(new Date());
        drlwb.setOrderCode(orderCode);
        drlwb.setRequest(params.toJSONString());
        drlwb.setType(type);
        this.dRequestLogsMapper.insert(drlwb);
        return drlwb;
    }

    @Override
    public void addResultLog(Long id, String result) {
        DRequestLogsWithBLOBs drlwb = new DRequestLogsWithBLOBs();
        drlwb.setId(id);
        drlwb.setResult(result);
        drlwb.setResultTime(new Date());
        this.dRequestLogsMapper.updateByPrimaryKeySelective(drlwb);
    }

}
