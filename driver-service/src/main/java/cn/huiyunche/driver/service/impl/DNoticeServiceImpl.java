package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.interfaces.UserService;
import cn.huiyunche.base.service.mappers.DNoticeMapper;
import cn.huiyunche.base.service.mappers.DNoticeUserMapper;
import cn.huiyunche.base.service.mappers.ext.DNoticeViewMapper;
import cn.huiyunche.base.service.model.DNotice;
import cn.huiyunche.base.service.model.DNoticeUser;
import cn.huiyunche.base.service.model.DNoticeUserExample;
import cn.huiyunche.base.service.vo.DNoticeVo;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.UserShowVo;
import cn.huiyunche.driver.service.DNoticeService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The interface D waybill service.
 *
 * @FileName: cn.huiyunche.app.service.tms.interfaces
 * @Description: 通知服务类
 * @author: liangpeng
 * @date: 2017/3/1
 */
@Service
public class DNoticeServiceImpl implements DNoticeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DNoticeServiceImpl.class);

    @Autowired
    private DNoticeMapper dNoticeMapper;

    @Autowired
    private DNoticeUserMapper dNoticeUserMapper;

    @Autowired
    private DNoticeViewMapper dNoticeViewMapper;

    @Autowired
    private UserService userService;

    private DNoticeMapper getDNoticeMapper() {
        return this.dNoticeMapper;
    }

    private DNoticeUserMapper getDNoticeUserMapper() {
        return this.dNoticeUserMapper;
    }

    private DNoticeViewMapper getDNoticeViewMapper() {
        return this.dNoticeViewMapper;
    }

    private UserService getUserService() {
        return this.userService;
    }


    @Override
    public Long addNotice(DNotice dNotice, Long user_id) throws BusinessException {
        LOGGER.info("addNotice dNotice : {},userid:{} ", dNotice, user_id);

        try {
            this.getDNoticeMapper().insertSelective(dNotice);
            DNoticeUser dNoticeUser = new DNoticeUser();
            dNoticeUser.setNoticeId(dNotice.getId());
            dNoticeUser.setUserId(user_id);
            dNoticeUser.setIsRead(false);
            this.getDNoticeUserMapper().insertSelective(dNoticeUser);
        } catch (Exception e) {
            LOGGER.error("DNoticeServiceImpl.addNotice error : {} ", e);
            throw new BusinessException("添加消息失败！");
        }

        return dNotice.getId();

    }

    @Override
    public Map<String, Object> selectDNoticesByUserPage(PageVo page) throws BusinessException {
        // 查询通知信息
        Map<String, Object> map = new HashMap<>();

        try {
            UserShowVo userVo = this.getUserService().getCurrentUser();
            Map<String, Object> paramsMap = new HashMap<>();
            String orderByClause = StringUtils.isNotBlank(page.getOrder()) == true ? page.getOrder()
                    : " a.create_time DESC";
            paramsMap.put("limitStart", page.getStartIndex());
            paramsMap.put("limitEnd", page.getPageSize());
            paramsMap.put("orderByClause", orderByClause);
            paramsMap.put("userId", userVo.getId());

            page.setTotalRecord(this.getDNoticeViewMapper().countByCondition(paramsMap));
            List<DNoticeVo> dNoticeVoList = this.getDNoticeViewMapper().selectNoticeInfo(paramsMap);

            map.put("page", page);
            map.put("dNoticeList", dNoticeVoList);
            return map;
        } catch (Exception e) {
            LOGGER.error("select current user dnotice list error : {}.", e);
            throw new BusinessException("查询当前用户的通知列表异常");
        }
    }

    @Override
    public DNotice selectDNoticeByID(Long dNoticeId) {
        DNotice dNotice = this.getDNoticeMapper().selectByPrimaryKey(dNoticeId);
        return dNotice;
    }

    @Override
    public void updateDNoticeStatusByID(Long dNoticeId) throws BusinessException {
        try {
            UserShowVo userVo = this.getUserService().getCurrentUser();
            DNoticeUserExample example = new DNoticeUserExample();
            example.createCriteria().andNoticeIdEqualTo(dNoticeId).andUserIdEqualTo(userVo.getId());
            List<DNoticeUser> dNoticeUsers = this.getDNoticeUserMapper().selectByExample(example);
            if (CollectionUtils.isNotEmpty(dNoticeUsers)) {
                DNoticeUser dNoticeUser = dNoticeUsers.get(0);
                if (!dNoticeUser.getIsRead()) {
                    dNoticeUser.setIsRead(true);
                    this.getDNoticeUserMapper().updateByPrimaryKeySelective(dNoticeUser);
                }
            }
        } catch (Exception e) {
            LOGGER.error("DNoticeServiceImpl.updateDNoticeStatusByID error : {} ", e);
            throw new BusinessException("更新消息失败！");
        }
    }

    @Override
    public int getIsNotReadNotice(Long userID) {
        int intRtn = 0;
        intRtn = this.getDNoticeViewMapper().countIsNotReadByUserID(userID);
        return intRtn;
    }


}
