package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DWaybill;
import cn.huiyunche.base.service.model.TmsOrder;
import cn.huiyunche.base.service.vo.OrderConfirmResult;
import cn.huiyunche.base.service.vo.OrderDepartureVo;

import java.util.List;

/**
 * The interface Order service.
 */
public interface OrderService {

    /**
     * Handle tms orders.
     *
     * @param tmsOrders the tms orders
     */
    void handleTmsOrders(List<TmsOrder> tmsOrders);

    /**
     * Order departure.
     *
     * @param odvList the odv list
     */
    void orderDeparture(List<OrderDepartureVo> odvList) throws Exception;

    /**
     * Is already exist boolean.
     *
     * @param ilineid the ilineid
     * @return the boolean
     */
    boolean isAlreadyExist(Long ilineid);

    /**
     * Confirm result.
     *
     * @param waybillId the waybill id
     */
    void confirmResult(Long waybillId) throws Exception;

    /**
     * Confirm results.
     *
     * @param confirmResults the confirm results
     */
    void confirmResults(List<OrderConfirmResult> confirmResults);

    /**
     * Gets ids from redis.
     *
     * @return the ids from redis
     */
    String getIdsFromRedis() throws Exception;

    /**
     * Select recalculate order ids string.
     *
     * @return the string
     */
    String selectRecalculateOrderIds() throws Exception;

    /**
     * Gets list by status.
     *
     * @param status the status
     * @return the list by status
     */
    String getListByStatus(List<Integer> status);

    /**
     * Recalculate order cost.
     *
     * @param waybillIds the waybill ids
     */
    void recalculateOrderCostByWaybillIds(String waybillIds) throws Exception;


    /**
     * 价格为0的订单重新入队方法
     *
     * @param tmsOrders the tms orders
     */
    void getReturnQueue(List<TmsOrder> tmsOrders);

    /**
     * Add queue.
     *
     * @param tmsOrder
     * @param waybill  the waybill
     */
    void addQueue(TmsOrder tmsOrder, DWaybill waybill) throws Exception;

    /**
     * Gets order_line_id by status
     *
     * @param status
     * @return
     */
    String getOrderLineIdsByStatus(List<Integer> status);


    /**
     * Gets order_line_id by status
     *
     * @param status
     * @return
     */
    List<Integer> getOrderLineIdListByStatus(List<Integer> status);
}
