package cn.huiyunche.driver.service;

/**
 * The interface Publish waybill from db service.
 *
 * @FileName: cn.huiyunche.driver.service
 * @Description: 发布运单
 * @author: Aaron
 * @date: 2017 /3/9 下午10:28
 */
public interface PublishWaybillFromDBService {

    /**
     * Db 2 redis.
     */
    void db2Redis();
}
