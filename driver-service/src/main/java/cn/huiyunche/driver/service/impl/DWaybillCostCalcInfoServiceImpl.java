package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.DWaybillCostCalcInfoMapper;
import cn.huiyunche.base.service.model.DWaybillCostCalcInfo;
import cn.huiyunche.base.service.model.DWaybillCostCalcInfoExample;
import cn.huiyunche.driver.service.DWaybillCostCalcInfoService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/28 下午4:43
 */
@Service
public class DWaybillCostCalcInfoServiceImpl implements DWaybillCostCalcInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DWaybillCostCalcInfoServiceImpl.class);

    @Autowired
    private DWaybillCostCalcInfoMapper dWaybillCostCalcInfoMapper;

    @Override
    public Integer add(DWaybillCostCalcInfo calcInfo) {
        LOGGER.info("DWaybillCostCalcInfoServiceImpl.add param : {}", calcInfo);

        if (null == calcInfo) {
            LOGGER.error("DWaybillCostCalcInfoServiceImpl.add param calcInfo must not be null");
            throw new IllegalArgumentException("运单费用计算关联信息不能为空");
        }

        dWaybillCostCalcInfoMapper.insertSelective(calcInfo);

        return calcInfo.getId();
    }

    @Override
    public DWaybillCostCalcInfo selectByWaybillId(Long waybillId) {
        LOGGER.info("DWaybillCostCalcInfoServiceImpl.selectByWaybillId param : {}", waybillId);

        if (null == waybillId) {
            LOGGER.error("DWaybillCostCalcInfoServiceImpl.selectByWaybillId param waybillId must not be null");
            throw new IllegalArgumentException("运单主键不能为空");
        }

        DWaybillCostCalcInfoExample example = new DWaybillCostCalcInfoExample();
        example.createCriteria().andWaybillIdEqualTo(waybillId);

        List<DWaybillCostCalcInfo> dWaybillCostCalcInfos = dWaybillCostCalcInfoMapper.selectByExample(example);
        if (CollectionUtils.isNotEmpty(dWaybillCostCalcInfos)) {
            return dWaybillCostCalcInfos.get(0);
        }

        return null;
    }
}
