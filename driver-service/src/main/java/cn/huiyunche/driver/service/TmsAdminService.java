package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.vo.Result;

import java.util.List;
import java.util.Map;

/**
 * 人送车 后端管理服务接口
 *
 * @author hdy [Tuffy]
 */
public interface TmsAdminService {

    /**
     * 时效统计列表接口
     *
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @return 结果集
     */
    public Result<List<Map<String, Object>>> getAgingList(String startDate, String endDate, String status);
}
