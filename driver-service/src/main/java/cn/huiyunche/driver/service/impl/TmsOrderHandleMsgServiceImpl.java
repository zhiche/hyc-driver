package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.TmsOrderHandleMsgMapper;
import cn.huiyunche.base.service.model.TmsOrderHandleMsg;
import cn.huiyunche.driver.service.TmsOrderHandleMsgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: Description
 * @author: ligl
 * @date: 2017/3/5 下午12:02
 */
@Service
public class TmsOrderHandleMsgServiceImpl implements TmsOrderHandleMsgService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TmsOrderHandleMsgServiceImpl.class);

    @Autowired
    private TmsOrderHandleMsgMapper tmsOrderHandleMsgMapper;


    @Override
    public Integer add(TmsOrderHandleMsg handleMsg) {
        return tmsOrderHandleMsgMapper.insertSelective(handleMsg);
    }
}
