package cn.huiyunche.driver.service;

import cn.huiyunche.base.service.model.DWaybillErrorMsg;
import cn.huiyunche.base.service.model.DWaybillErrorMsgExample;
import cn.huiyunche.base.service.vo.DWaybillErrorMsgVo;
import cn.huiyunche.base.service.vo.PageVo;

import java.util.List;
import java.util.Map;

/**
 * The interface D waybill error msg service.
 *
 * @FileName: cn.huiyunche.driver.service
 * @Description: 运单错误信息
 * @author: Aaron
 * @date: 2017 /3/4 下午12:01
 */
public interface DWaybillErrorMsgService {

    /**
     * Add integer.
     *
     * @param errorMsg the error msg
     * @return the integer
     */
    Integer add(DWaybillErrorMsg errorMsg);

    /**
     * Select by conditions d waybill error msg.
     *
     * @param example the example
     * @return the d waybill error msg
     */
    List<DWaybillErrorMsg> selectByConditions(DWaybillErrorMsgExample example);

    /**
     * Select DWaybillErrorMsg by Condition
     *
     * @param vo
     * @return
     */
    Map<String, Object> listByCondition(PageVo pageVo, DWaybillErrorMsgVo vo);


    /**
     * Update is_handle
     *
     * @param bool
     * @param id
     * @return
     */
    int modifyisHandle(boolean bool, Integer id, Integer handleTimes);
}
