package cn.huiyunche.driver.service.form;

import cn.huiyunche.base.service.constant.RegularConstant;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.service.form
 * @Description: 燃油价格变动表单
 * @author: Aaron
 * @date: 2017/2/26 下午6:47
 */
public class DFuelPriceEffectiveForm {

    private Integer id;

//    //燃油价格变动名称
//    @NotBlank(message = "请输入名称")
//    @Size(max=16, min = 1,message = "燃油价格变动名称为1-16位")
//    private String name;

    //燃油类型主键
    @NotNull(message = "请选择费用类型")
    @Min(value = 1, message = "费用类型主键不能小于0")
    private Integer fuelTypeId;

    //市场价
    @NotNull(message = "市场价不能为空")
    @Pattern(regexp = RegularConstant.number_decimals, message = "市场价只能输入数字且为正数")
    @DecimalMin(value = "0.01", message = "市场价不能小于0元")
    private BigDecimal marketPrice;

    //生效时间
    @NotNull(message = "生效时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Future(message = "生效时间必须是一个未来的时间")
    private Date effectiveDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(Integer fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @Override
    public String toString() {
        return "DFuelPriceEffectiveForm{" +
                "id=" + id +
                ", fuelTypeId=" + fuelTypeId +
                ", marketPrice=" + marketPrice +
                ", effectiveDate=" + effectiveDate +
                '}';
    }
}
