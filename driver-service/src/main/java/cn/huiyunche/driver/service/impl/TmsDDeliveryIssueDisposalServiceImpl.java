package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.TmsDDeliveryIssueDisposalMapper;
import cn.huiyunche.base.service.model.TmsDDeliveryIssueDisposal;
import cn.huiyunche.driver.service.TmsDDeliveryIssueDisposalService;
import cn.huiyunche.driver.service.form.TmsDDeliveryAuditForm;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by houjianhui on 2017/4/10.
 */

@Service
public class TmsDDeliveryIssueDisposalServiceImpl implements TmsDDeliveryIssueDisposalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TmsDDeliveryIssueDisposalServiceImpl.class);

    @Autowired
    private TmsDDeliveryIssueDisposalMapper tmsDDeliveryIssueDisposalMapper;

    @Override
    public Long save(TmsDDeliveryAuditForm form) throws Exception {
        LOGGER.info("TmsDDeliveryIssueDisposalServiceImpl.save params form: {}", form);
        if (null == form) {
            LOGGER.info("TmsDDeliveryIssueDisposalServiceImpl.save form must not be null");
            throw new IllegalArgumentException("表单数据不能为空");
        }

        // 检验数据
        this.verification(form);

        TmsDDeliveryIssueDisposal disposal = new TmsDDeliveryIssueDisposal();
        BeanUtils.copyProperties(form, disposal);

        this.tmsDDeliveryIssueDisposalMapper.insertSelective(disposal);

        return disposal.getId();
    }

    private void verification(TmsDDeliveryAuditForm form) throws Exception {

        if (null == form.getDisposalTypeId() || 0 == form.getDisposalTypeId()) {
            LOGGER.error("TmsDDeliveryIssueDisposalServiceImpl.verification disposalTypeId must not be null");
            throw new IllegalArgumentException("异常处理类型ID不能为空");
        } else if (StringUtils.isBlank(form.getDisposalContent())) {
            LOGGER.error("TmsDDeliveryIssueDisposalServiceImpl.verification disposalContent must not be null");
            throw new IllegalArgumentException("异常处理类型描述不能为空");
        }

    }
}
