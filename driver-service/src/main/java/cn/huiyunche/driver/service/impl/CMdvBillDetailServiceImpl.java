package cn.huiyunche.driver.service.impl;

import cn.huiyunche.base.service.mappers.CMdvBillDetailMapper;
import cn.huiyunche.base.service.model.CMdvBillDetail;
import cn.huiyunche.base.service.model.CMdvBillDetailExample;
import cn.huiyunche.driver.service.CMdvBillDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: Description
 * @author: ligl
 * @date: 2017/3/9 上午11:59
 */
@Service
public class CMdvBillDetailServiceImpl implements CMdvBillDetailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CMdvBillDetailServiceImpl.class);

    @Autowired
    private CMdvBillDetailMapper cMdvBillDetailMapper;

    @Override
    public Long add(CMdvBillDetail cMdvBillDetail) {
        LOGGER.info("CMdvBillDetailServiceImpl.add params : {}", cMdvBillDetail);

        if (null == cMdvBillDetail) {
            LOGGER.error("CMdvBillDetailServiceImpl.add param form must not be null");
            throw new IllegalArgumentException("账单明细不能为空");
        }

        cMdvBillDetailMapper.insertSelective(cMdvBillDetail);
        return cMdvBillDetail.getId();
    }

    @Override
    public List<CMdvBillDetail> selectByExample(CMdvBillDetailExample detailExample) {
        return cMdvBillDetailMapper.selectByExample(detailExample);
    }


//
}
