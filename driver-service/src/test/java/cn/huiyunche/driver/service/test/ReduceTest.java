package cn.huiyunche.driver.service.test;

import java.util.Arrays;

/**
 * @FileName: cn.huiyunche.driver.service
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/9 下午11:15
 */
public class ReduceTest {

    public static void main(String[] args) {

        class Province{
            int code;
            String name;

            public int getCode() {
                return code;
            }

            public Province setCode(int code) {
                this.code = code;
                return this;
            }

            public String getName() {
                return name;
            }

            public Province setName(String name) {
                this.name = name;
                return this;
            }

            public Province(int code, String name) {
                this.code = code;
                this.name = name;
            }
        }

        String reduce = Arrays.asList(new Province(1, "湖北"),
                new Province(2, "湖南"),
                new Province(3, "广东"),
                new Province(4, "河北"),
                new Province(5, "北京")).stream().map(Province::getName).reduce("", (x, y) -> x+y+",");

        System.out.println("reduce = " + reduce);
    }
}
