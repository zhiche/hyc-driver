package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.model.DWaybillCostCalcInfo;
import cn.huiyunche.driver.service.DWaybillCostCalcInfoService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @FileName: cn.huiyunche.driver.service
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/28 下午4:44
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
//@ActiveProfiles("dev")
public class DWaybillCostCalcInfoServiceTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private DWaybillCostCalcInfoService service;

    @Test
    @Ignore
    public void add() throws Exception {
        DWaybillCostCalcInfo calaInfo = new DWaybillCostCalcInfo();
        calaInfo.setWaybillId(1L);
        calaInfo.setPreKmFeeId(1);
        calaInfo.setPerKmOilFee(BigDecimal.valueOf(0.75));
        calaInfo.setFuelPriceId(1);
        calaInfo.setMarketPrice(BigDecimal.valueOf(6.5));
        calaInfo.setRouteVehicleId(1);
        calaInfo.setLabourServicesPrice(BigDecimal.valueOf(0.20));
        calaInfo.setTotalPrice(BigDecimal.valueOf(1.5));

        Integer id = service.add(calaInfo);
        System.out.println("id = " + id);
    }

    @Test
    @Ignore
    public void selectByWaybillId() throws Exception {
        DWaybillCostCalcInfo dWaybillCostCalcInfo = service.selectByWaybillId(1L);
        System.out.println("dWaybillCostCalcInfo = " + dWaybillCostCalcInfo);
    }

}