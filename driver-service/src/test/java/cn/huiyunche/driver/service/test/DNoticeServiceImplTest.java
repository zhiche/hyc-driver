package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.model.DNotice;
import cn.huiyunche.driver.service.DNoticeService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
@ActiveProfiles("dev")
public class DNoticeServiceImplTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private DNoticeService dNoticeServcie;


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void addNoticeTest() throws Exception {
        try {
            DNotice dNotice = new DNotice();
            dNotice.setTitle("提示");
            dNotice.setType(0);
            dNotice.setCreator("中联");
            dNotice.setContent("11223");
            Long user_id = 379L;
            Long noticeId = dNoticeServcie.addNotice(dNotice, user_id);
            System.out.println(noticeId);
        } catch (Exception e) {
            System.out.println("e = " + e);
        }
    }


}
