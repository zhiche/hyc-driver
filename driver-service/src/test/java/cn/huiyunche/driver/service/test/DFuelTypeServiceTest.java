package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.model.DFuelType;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.DFuelTypeService;
import cn.huiyunche.driver.service.form.DFuelTypeForm;
import cn.huiyunche.driver.service.query.DFuelTypeQueryConditions;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @FileName: cn.huiyunche.driver.service
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/27 下午2:20
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
//@ActiveProfiles("dev")
public class DFuelTypeServiceTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private DFuelTypeService dFuelTypeService;

    @Test
    @Ignore
    public void add() throws Exception {

        DFuelTypeForm form = new DFuelTypeForm();
//        form.setFuelCode("10");
//        form.setFuelName("93汽油");
//        form.setBasePrice(BigDecimal.valueOf(6.50));
//        form.setFuelCode("20");
//        form.setFuelName("97汽油");
//        form.setBasePrice(BigDecimal.valueOf(7.50));
        form.setFuelCode("30");
        form.setFuelName("0#柴油");
//        form.setBasePrice(BigDecimal.valueOf(6.00));

        dFuelTypeService.add(form, null);
    }

    @Test
    @Ignore
    public void delete() throws Exception {
        dFuelTypeService.delete(4);
    }

    @Test
    @Ignore
    public void selectListByConditions() throws Exception {

        PageVo pageVo = new PageVo();

        DFuelTypeQueryConditions conditions = new DFuelTypeQueryConditions();
        conditions.setFuelCode("0");
//        conditions.setFuelName("93#汽油");

        Map<String, Object> map = dFuelTypeService.selectListByConditions(pageVo, conditions);
        System.out.println("map = " + map);
    }

    @Test
    @Ignore
    public void selectListByConditions1() throws Exception {
        DFuelTypeQueryConditions conditions = new DFuelTypeQueryConditions();
//        conditions.setFuelCode("0");
        conditions.setFuelName("油");

        List<DFuelType> list = dFuelTypeService.selectListByConditions(conditions);
        System.out.println("list = " + list);
    }

}