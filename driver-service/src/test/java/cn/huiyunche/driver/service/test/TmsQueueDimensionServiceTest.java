package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.framework.utils.HYCUtils;
import cn.huiyunche.base.service.interfaces.TmsQueueService;
import cn.huiyunche.base.service.vo.UserShowVo;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.Jedis;

import java.util.Arrays;
import java.util.List;

/**
 * @FileName: cn.huiyunche.driver.service
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/6 下午5:23
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
public class TmsQueueDimensionServiceTest {

    private Jedis jedis;

    @Autowired
    private TmsQueueService tmsQueueService=null;

    @Before
    public void setUp() throws Exception {
        jedis = HYCUtils.getJedis();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testJedis() {
        String rpoplpush = jedis.rpoplpush("ZL_RS_DRIVER", "ZL_RS_DRIVER");
        System.out.println("rpoplpush = " + rpoplpush);
    }

    @Test
    public void testSetJedis(){
        List<String> listPhone= Arrays.asList("13576051103","13970066004","13970844428","13870812128",
                "13870828962","18870861007", "13576051150");
//        List<String> listPhone= Arrays.asList("13576051103");

        for (String phone:listPhone) {
            UserShowVo userVo = new UserShowVo();
            userVo.setPhone(phone);
            try{
                this.tmsQueueService.appendDriver(userVo.getPhone());
            } catch (Exception var13) {
                throw new BusinessException("上线失败");
            }

        }
    }

}