package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.interfaces.DriverCompareService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by qichao on 2017/3/2.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
public class DriverCompareImplTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    DriverCompareService dcs;

    @Test
    public void CompareTest() throws BusinessException {
        String img1_url = "https://pic2.zhimg.com/e9921080ff468e845f37175b5150376d_b.jpg";
        String img2_url = "https://pic2.zhimg.com/f03e08ee21da1ce2e89223bae76be77d_bdfdafdaf.jpg";
        dcs.compareFaceApi(img1_url, img2_url);
    }
}
