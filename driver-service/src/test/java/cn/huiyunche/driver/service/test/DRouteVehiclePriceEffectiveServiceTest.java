package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.DRouteVehiclePriceEffectiveService;
import cn.huiyunche.driver.service.form.DRouteVehiclePriceEffectiveForm;
import cn.huiyunche.driver.service.query.DRouteVehiclePriceEffectiveQueryConditions;
import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.service
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/27 下午2:19
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
//@ActiveProfiles("dev")
public class DRouteVehiclePriceEffectiveServiceTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private DRouteVehiclePriceEffectiveService dRouteVehiclePriceEffectiveService;

    @Test
    @Ignore
    public void add() throws Exception {
        DRouteVehiclePriceEffectiveForm form = new DRouteVehiclePriceEffectiveForm();
        form.setRouteId(2);
        form.setVehicleTypeId(11);
        form.setLabourServicesPrice(BigDecimal.valueOf(0.20));
        form.setTotalPrice(BigDecimal.valueOf(1.50));

        form.setEffectiveDate(new DateTime().plusDays(1).withMillisOfDay(0).toDate());


        dRouteVehiclePriceEffectiveService.add(form, null);

    }

    @Test
    @Ignore
    public void delete() throws Exception {

        dRouteVehiclePriceEffectiveService.delete("2");

    }

    @Test
    @Ignore
    public void selectListByConditions() throws Exception {

        PageVo pageVo = new PageVo();

        DRouteVehiclePriceEffectiveQueryConditions conditions = new DRouteVehiclePriceEffectiveQueryConditions();
        conditions.setRouteId(2);
        conditions.setVehicleTypeId(11);
        conditions.setEffectiveDate(new Date());
//        conditions.setInvalidDate();

        dRouteVehiclePriceEffectiveService.selectListByConditions(pageVo, conditions);

    }

    @Test
    @Ignore
    public void selectNotEffective() throws Exception {

    }

    @Test
    @Ignore
    public void selectCurrentlyActive() throws Exception {

    }

    @Test
    @Ignore
    public void update() throws Exception {

    }

}