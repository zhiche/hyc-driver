package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.model.DVehiclePreKilometerFeeEffective;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.DVehiclePreKilometerFeeEffectiveService;
import cn.huiyunche.driver.service.query.DVehiclePreKilometerFeeEffectiveQueryConditions;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @FileName: cn.huiyunche.driver.service
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/27 下午2:20
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
//@ActiveProfiles("dev")
public class DVehiclePreKilometerFeeEffectiveServiceTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private DVehiclePreKilometerFeeEffectiveService dVehiclePreKilometerFeeEffectiveService;

    @Test
    @Ignore
    public void add() throws Exception {

    }

    @Test
    @Ignore
    public void selectListByConditions() throws Exception {

        PageVo pageVo = new PageVo();

        DVehiclePreKilometerFeeEffectiveQueryConditions conditions = new DVehiclePreKilometerFeeEffectiveQueryConditions();
        conditions.setVehicleTypeId(11);
//        conditions.setFuelPriceId();
//        conditions.setEffectiveDate();
//        conditions.setInvalidDate();

        Map<String, Object> map = dVehiclePreKilometerFeeEffectiveService.selectListByConditions(pageVo, conditions);
        System.out.println("map = " + map);
    }

    @Test
    @Ignore
    public void selectNotEffective() throws Exception {
        List<DVehiclePreKilometerFeeEffective> list = dVehiclePreKilometerFeeEffectiveService.selectNotEffective(23);
        System.out.println("list = " + list);
    }

    @Test
    @Ignore
    public void selectCurrentlyActive() throws Exception {
        DVehiclePreKilometerFeeEffective lsit = dVehiclePreKilometerFeeEffectiveService.selectPreviousActive(24, null);
        System.out.println("lsit = " + lsit);
    }

    @Test
    @Ignore
    public void update() throws Exception {

    }

}