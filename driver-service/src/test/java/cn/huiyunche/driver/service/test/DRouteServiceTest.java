package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.DRouteService;
import cn.huiyunche.driver.service.form.DRouteForm;
import cn.huiyunche.driver.service.query.DRouteQueryConditions;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * @FileName: cn.huiyunche.driver.service
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/27 下午2:19
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
//@ActiveProfiles("dev")
public class DRouteServiceTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private DRouteService dRouteService;

    @Test
    @Ignore
    public void add() throws Exception {

        DRouteForm form = new DRouteForm();
        form.setOProvinceCode("360000");
        form.setOProvince("江西省");
        form.setOCityCode("360100");
        form.setOCity("南昌市");
        form.setDProvinceCode("440000");
        form.setDProvince("广东省");
        form.setDCityCode("440300");
        form.setDCity("深圳市");
//        form.setCurrentDistance(BigDecimal.valueOf(819));

        System.out.println(dRouteService.add(form, null));
    }

    @Test
    @Ignore
    public void update() throws Exception {
        DRouteForm form = new DRouteForm();
        form.setId(2);
        form.setOProvinceCode("360000");
        form.setOProvince("江西省");
        form.setOCityCode("360100");
        form.setOCity("南昌市");
        form.setDProvinceCode("440000");
        form.setDProvince("广东省");
        form.setDCityCode("440300");
        form.setDCity("深圳市");
//        form.setCurrentDistance(BigDecimal.valueOf(820));

        System.out.println(dRouteService.update(form, null));
    }

    @Test
    @Ignore
    public void enableOrDisabled() throws Exception {
        dRouteService.enableOrDisabled(2, false);
    }

    @Test
    @Ignore
    public void selectListByConditions() throws Exception {

        PageVo pageVo = new PageVo();

        DRouteQueryConditions conditions = new DRouteQueryConditions();
//        conditions.setOProvinceCode("360000");
//        conditions.setOCityCode("360100");
//        conditions.setDProvinceCode("440000");
//        conditions.setDCityCode("440300");
        conditions.setEnable(false);

        Map<String, Object> map = dRouteService.selectListByConditions(pageVo, conditions);
        System.out.println("map = " + map);
    }

}