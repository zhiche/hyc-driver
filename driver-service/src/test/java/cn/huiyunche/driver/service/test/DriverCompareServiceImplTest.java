package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.interfaces.DriverCompareService;
import cn.huiyunche.base.service.model.DCompareFaceResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @FileName: cn.huiyunche.driver.service.impl
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/8 下午1:12
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
public class DriverCompareServiceImplTest {

    @Autowired
    private DriverCompareService driverCompareService;

    @Test
    public void compareFaceApi() throws Exception {
        String imageSourceUrl = "https://ww2.sinaimg.cn/large/006tNbRwgy1fdffcn61cfj30hs0vkjus.jpg";
        String imageTargetUrl = "https://ww1.sinaimg.cn/large/006tNbRwgy1fdffcmq8jkj30qo0zk0vs.jpg";
        DCompareFaceResult dCompareFaceResult = driverCompareService.compareFaceApi(imageSourceUrl, imageTargetUrl);
        System.out.println("dCompareFaceResult ===============" + dCompareFaceResult);
    }


}