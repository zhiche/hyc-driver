package cn.huiyunche.driver.service.test;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ligl on 2017/3/10.
 */
public class JodaTimeTest {

    public static void main(String[] args) {
        List list = new ArrayList<>();
        DateTime now = new DateTime();
        DateTime withDayOfWeek1 = now.withDayOfWeek(1);
        DateTime withDayOfWeekend1 = now.withDayOfWeek(1).plusDays(6);

        DateTime withDayOfWeek2 = now.withDayOfWeek(1).minusDays(7);
        DateTime withDayOfWeekend2 = now.withDayOfWeek(1).minusDays(1);

        DateTime withDayOfWeek3 = now.withDayOfWeek(1).minusDays(14);
        DateTime withDayOfWeekend3 = now.withDayOfWeek(1).minusDays(8);

        DateTime withDayOfWeek4 = now.withDayOfWeek(1).minusDays(21);
        DateTime withDayOfWeekend4 = now.withDayOfWeek(1).minusDays(15);

        DateTime withDayOfWeek5 = now.withDayOfWeek(1).minusDays(28);
        DateTime withDayOfWeekend5 = now.withDayOfWeek(1).minusDays(22);
        list.add(withDayOfWeek1.toString("yyyy/MM/dd") + "-" + withDayOfWeekend1.toString("yyyy/MM/dd"));
        list.add(withDayOfWeek2.toString("yyyy/MM/dd") + "-" + withDayOfWeekend2.toString("yyyy/MM/dd"));
        list.add(withDayOfWeek3.toString("yyyy/MM/dd") + "-" + withDayOfWeekend3.toString("yyyy/MM/dd"));
        list.add(withDayOfWeek4.toString("yyyy/MM/dd") + "-" + withDayOfWeekend4.toString("yyyy/MM/dd"));
        list.add(withDayOfWeek5.toString("yyyy/MM/dd") + "-" + withDayOfWeekend5.toString("yyyy/MM/dd"));

    }
}
