package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.form.BVehicleTypeForm;
import cn.huiyunche.base.service.interfaces.BVehicleTypeService;
import cn.huiyunche.base.service.model.BVehicleType;
import cn.huiyunche.base.service.query.BVehicleTypeQueryConditions;
import cn.huiyunche.base.service.vo.PageVo;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @FileName: cn.huiyunche.base.service.interfaces
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/27 下午9:12
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
//@ActiveProfiles("dev")
public class BVehicleTypeServiceTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private BVehicleTypeService bVehicleTypeService;

    @Test
    @Ignore
    public void selectByFuelTypeId() throws Exception {
        List<BVehicleType> list = bVehicleTypeService.selectByFuelTypeId(1);
        System.out.println("list = " + list);
    }

    @Test
    @Ignore
    public void add() throws Exception {

        BVehicleTypeForm form = new BVehicleTypeForm();
        form.setTypeCode("DYDSAUQTW11AH0W001");
        form.setTypeName("测试车型");
        form.setLicenseType("C1");
        form.setDescripition("测试车型描述");
        form.setFuelTypeId(1);
        form.setFuelConsumption(BigDecimal.valueOf(11));

        Long id = bVehicleTypeService.add(form, null);
        System.out.println("id = " + id);
    }

    @Test
    public void update() throws Exception {
        BVehicleTypeForm form = new BVehicleTypeForm();
        form.setId(527L);
        form.setTypeCode("DYDSAUQTW11AH0W003");
        form.setTypeName("测试车型1");
        form.setLicenseType("C2");
        form.setDescripition("测试车型描述1");
        form.setFuelTypeId(2);
        form.setFuelConsumption(BigDecimal.valueOf(12));

        Long id = bVehicleTypeService.update(form, null);
        System.out.println("id = " + id);
    }

    @Test
    @Ignore
    public void selectListByConditions() throws Exception {
        PageVo pageVo = new PageVo();

        BVehicleTypeQueryConditions conditions = new BVehicleTypeQueryConditions();
//        conditions.setFuelTypeId(2);
//        conditions.setFuelConsumption(BigDecimal.valueOf(12));
//        conditions.setLicenseType("C2");
//        conditions.setTypeCode("DYDSAUQTW11AH0W003");
        conditions.setTypeName("测试车型1");

        bVehicleTypeService.selectListByConditions(pageVo, conditions);

    }

    @Test
    @Ignore
    public void selectByTypeCode() throws Exception {
        List<BVehicleType> list = bVehicleTypeService.selectByTypeCode("AYERBVSGKJCAN0B000");
        System.out.println("list = " + list);
    }

}