package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.model.DFuelPriceEffective;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.driver.service.DFuelPriceEffectiveService;
import cn.huiyunche.driver.service.form.DFuelPriceEffectiveForm;
import cn.huiyunche.driver.service.query.DFuelPriceEffectiveQueryConditions;
import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @FileName: cn.huiyunche.driver.service
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/27 下午2:20
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
@Transactional
//@ActiveProfiles("dev")
public class DFuelPriceEffectiveServiceTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private DFuelPriceEffectiveService dFuelPriceEffectiveService;

    @Test
    @Ignore
    public void add() throws Exception {

        DFuelPriceEffectiveForm form = new DFuelPriceEffectiveForm();
        form.setFuelTypeId(1);
        form.setMarketPrice(BigDecimal.valueOf(6.40));
        form.setEffectiveDate(new DateTime().plusDays(1).withMillisOfDay(1).toDate());

        dFuelPriceEffectiveService.add(form, null);
    }

    @Test
    @Ignore
    public void delete() throws Exception {
        dFuelPriceEffectiveService.del(9);
    }

    @Test
    @Ignore
    public void selectListByConditions() throws Exception {
        PageVo pageVo = new PageVo();

        DFuelPriceEffectiveQueryConditions conditions = new DFuelPriceEffectiveQueryConditions();
//        conditions.setFuelTypeId(1);
        conditions.setEffectiveDate(new DateTime().plusDays(1).withMillisOfDay(1).toDate());

        dFuelPriceEffectiveService.selectListByConditions(pageVo, conditions);
    }

    @Test
    @Ignore
    public void selectNotEffective() throws Exception {
        List<DFuelPriceEffective> list = dFuelPriceEffectiveService.selectNotEffective(1);
        System.out.println("list = " + list);
    }

    @Test
    @Ignore
    public void selectCurrentlyActive() throws Exception {
        DFuelPriceEffective list = dFuelPriceEffectiveService.selectPreviousActive(1, null);
        System.out.println("list = " + list);
    }

    @Test
    @Ignore
    public void update() throws Exception {
        List<DFuelPriceEffective> list = dFuelPriceEffectiveService.selectByFuelTypeId(1);
        System.out.println("list = " + list);
    }

}