package cn.huiyunche.driver.service.test;

import cn.huiyunche.base.service.form.AttachForm;
import cn.huiyunche.base.service.interfaces.DWaybillService;
import cn.huiyunche.base.service.mappers.TmsDDeliveryMapper;
import cn.huiyunche.base.service.model.DWaybill;
import cn.huiyunche.base.service.model.TmsDDelivery;
import cn.huiyunche.base.service.vo.UserPositionVo;
import cn.huiyunche.tools.basic.Collections3;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;


/**
 * Created by qichao on 2017/4/9.
 */
@Test
@ContextConfiguration(locations = {"classpath*:/spring/applicationContext*.xml"})
public class DeliveryTest extends AbstractTransactionalTestNGSpringContextTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryTest.class);

    @Autowired
    private DWaybillService dWaybillService;

    @DataProvider
    public Object[ ][ ] deliveryDataProvider() {
        Long waybillId = 5474L;
        UserPositionVo upv = new UserPositionVo();
        BigDecimal x = new BigDecimal(113.932819);
        BigDecimal y = new BigDecimal(22.538625);
        upv.setLatitude(x);
        upv.setLongitude(y);
        upv.setAddress("中国广东省深圳市南山区桃园路1号");
        List<AttachForm> atm_list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            AttachForm atm = new AttachForm();
            atm.setImage("FvlEtFnPrbLFoJbKDyPeUvYEA7SB");
            atm_list.add(atm);
        }
        String attachment = JSON.toJSONString(atm_list);
        Map<String, Object> map = new HashMap<>();
        map.put("waybillid", waybillId);
        map.put("upv", upv);
        map.put("attachment", attachment);
        return new Object[ ][ ]{{map}};
    }

    /**
     * 测试交车确认接口
     * @param deliveryData
     */
    @Test(dataProvider = "deliveryDataProvider")
    public void deliveryConfirmTest(Map<String, Object> deliveryData) {
        Long waybillid = ((Long) deliveryData.get("waybillid"));
        UserPositionVo upv = ((UserPositionVo) deliveryData.get("upv"));
        String attachment = ((String) deliveryData.get("attachment"));
        try {
            dWaybillService.deliveryConfirm(waybillid, upv, attachment);
        } catch (Exception exception) {
            LOGGER.error("some error");
        }
    }


    @Test(dataProvider = "deliveryDataProvider")
    public void queryWaybillDetailInfoTest(Map<String, Object> deliveryData) {
        Long waybillid = ((Long) deliveryData.get("waybillid"));
        try{
            dWaybillService.getById(waybillid);
        }catch(Exception exception){
            LOGGER.error("some error");
        }
    }

    @Test
    public void queryListInfoTest(){
        List<Integer> intList=new ArrayList<Integer>();

        for(int i=1;i<=195;i++){
            intList.add(i);
        }

        if(CollectionUtils.isNotEmpty(intList)){
            int idsSize=intList.size();
            int loop=idsSize%20==0?idsSize/20:idsSize/20+1;
            for(int i=0;i<loop;i++){
                if((i+1)*20>=idsSize){
                    List<Integer> isubOrderid=intList.subList(i*20,idsSize);
                    System.out.print("isubOrderid"+isubOrderid);
                }
                else{
                    List<Integer> isubOrderid=intList.subList(i*20,(i+1)*20);
                    System.out.print("isubOrderid"+isubOrderid);
                }
            }
        }


    }

    @Test
    public void queryListPartitionTest(){
        List<Integer> intList=new ArrayList<Integer>();

        for(int i=1;i<=195;i++){
            intList.add(i);
        }

        long memory=DeliveryTest.getFreeMemory();

        System.out.print("feeMemoy:" + memory);

        for(int i=0;i<10;i++){

            Map<String, Object> paramsMap = new HashedMap<>();

            memory=DeliveryTest.getFreeMemory();

            System.out.print("feeMemoy:" + memory);

            if(CollectionUtils.isNotEmpty(intList)){
                List<List<Integer>> partitionList= Lists.partition(intList,20);

                for(int j=0;j<partitionList.size();j++) {

                    List<Integer> isubOrderid = partitionList.get(j);

                    String sb = Collections3.convertToString(isubOrderid, ",");

                    paramsMap.put("isubOrderid",sb);

                    System.out.print("isubOrderid" + sb);
                }

                paramsMap.clear();
            }

            memory=DeliveryTest.getFreeMemory();

            System.out.print("feeMemoy:" + memory);

        }

        memory=DeliveryTest.getFreeMemory();

        System.out.print("feeMemoy:" + memory);


        sleep(1000);


        memory=DeliveryTest.getFreeMemory();

        System.out.print("feeMemoy:" + memory);

    }

    public static long getFreeMemory() {
        return Runtime.getRuntime().freeMemory() / (1024 * 1024);
    }

    public static void sleep(long sleepFor) {
        try {
            Thread.sleep(sleepFor);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }




}
