## 慧运车 接口 ＋ 业务逻辑服务

### 慧运车项目结构(hycapp)
   1. 定义项目结构

### 父项目(hycapp-parent)
   1. 定义依赖jar包

### 业务逻辑(hycapp-service)
   1. 所有业务逻辑

### 托运端个人版接口服务(hycapp-kyle)
   1. java web ＋ h5项目
   2. 提供rest接口服务app(IOS, Android, Wechat)
   3. 微信公众号项目
   
### 托运端企业版接口服务(hycapp-kylebuss)
   1. java web 项目, web与接口分离
   2. 提供rest接口服务(pc-web)
   3. web项目为node项目(kylebuss-web)

### 司机端个人版接口服务(hycapp-stan)
   1. java web项目
   2. 提供rest接口服务(IOS, Android)
   
### 司机端企业版接口服务(hycapp-stanbuss)
   1. java web + h5项目
   2. 提供rest接口服务(Wechat)
   3. 微信公众号项目

### 慧运车后台管理项目(hycapp-admin)
   1. java web + web项目
   2. 提供rest接口服务(pc-web)
