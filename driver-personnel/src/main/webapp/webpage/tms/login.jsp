<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String urlPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta name="format-detection" content="telephone=no"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>慧运车-中联客服登录</title>
    <style type="text/css">
        body {
            margin: 0;
        }

        .hander {
            padding: 10px;
            text-align: center;
            background-color: #fa8334;
            color: #ffffff;
            font-size: 1.1em;
        }

        .body {
            margin: 25px 8px 8px 8px;
        }

        .body .form-table {
            border: 0;
            width: 100%;
        }

        .body .form-table th {
            vertical-align: middle;
            font-size: 1em;
        }

        .body .form-table td {
            padding: 5px;
            border-bottom: 1px solid #eeeeee;
        }

        .body .form-table input {
            border: 0;
            padding: 5px;
            width: 100%;
            font-size: 1em;
            color: #aaaaaa;
        }

        .body .button {
            border-radius: 5px;
            margin-top: 25px;
            padding: 10px 16px;
            text-align: center;
            background-color: #fa8334;
            color: #ffffff;
        }

        .body .button:active {
            background-color: rgba(255, 150, 0, 0.5);
        }

        .error-msg {
            margin-top: 25px;
            color: red;
        }
    </style>
</head>
<body>
<div class="hander">慧运车-中联客服登录</div>
<div class="body">
    <table class="form-table">
        <tr>
            <th>账号：</th>
            <td>
                <input id="login_name" value="" type="number" placeholder="请输入账号">
            </td>
        </tr>
        <tr>
            <th>密码：</th>
            <td>
                <input id="password" value="" type="password" placeholder="请输入密码">
            </td>
        </tr>
    </table>
    <div class="button" onclick="loginCtrl.login()">登录</div>
    <div class="error-msg" id="error_msg"></div>
</div>
</body>
<script type="text/javascript">
    var urlPath = '<%=urlPath %>';
    //ajax
    function ajax(option) {
        var _option = extend({
            'type': 'get',
            'dataType': 'html',
            'async': true
        }, option);
        //先声明一个异步请求对象
        var xmlHttpReg = null;
        if (window.ActiveXObject) {//如果是IE
            xmlHttpReg = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            xmlHttpReg = new XMLHttpRequest(); //实例化一个xmlHttpReg
        }
        //如果实例化成功,就调用open()方法,就开始准备向服务器发送请求
        if (xmlHttpReg != null) {
            if (_option.type.toUpperCase() === "POST") {
                xmlHttpReg.open("post", urlPath + _option.url, _option.async);
                xmlHttpReg.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlHttpReg.send(postParams());
            } else {
                xmlHttpReg.open("get", getParams(), _option.async);
                xmlHttpReg.send(null);
            }
            if (_option.async) {
                xmlHttpReg.onreadystatechange = doResult; //设置回调函数
            } else {
                doResult();
            }
        }
        //回调函数
        //一旦readyState的值改变,将会调用这个函数,readyState=4表示完成相应
        //设定函数doResult()
        function doResult() {
            if (xmlHttpReg.readyState == 4) {//4代表执行完成
                if (xmlHttpReg.status == 200) {//200代表执行成功
                    //将xmlHttpReg.responseText的值赋给ID为resText的元素
                    // document.getElementById("resText").innerHTML = xmlHttpReg.responseText;
                    if (_option.success) {
                        _option.success(formatData(_option.dataType, xmlHttpReg.responseText));
                    }
                } else {
                    if (_option.error) {
                        _option.error({'errorCode': xmlHttpReg.status});
                    }
                }
            }
        }

        // 获取post参数集合
        function postParams() {
            var str = '';
            var _params = _option.params || {};
            for (var prop in _params) {
                str += prop + '=' + _params[prop] + '&'
            }
            return str;
        }

        // 获取get参数集合
        function getParams() {
            var _params = _option.params || {};
            var _url = urlPath + _option.url;
            for (var i in _params) {
                if (_url.indexOf('?') >= 0) {
                    _url += '&' + i + '=' + _params[i];
                } else {
                    _url += '?' + i + '=' + _params[i];
                }
            }
            return _url;
        }

        // 格式化数据
        function formatData(type, data) {
            type = type.toUpperCase();
            // json 数据
            if (type === 'JSON') {
                return JSON.parse(data || new Object())
            }
            return data;
        }

        // extend
        function extend(from, cover) {
            var _from = new Object();
            var _cover = new Object();
            if (from && from.toString() === '[object Object]') {
                for (var i in from) {
                    _from[i] = from[i];
                }
            }
            if (cover && cover.toString() === '[object Object]') {
                for (var i in cover) {
                    _cover[i] = cover[i];
                }
            }
            for (var i in _cover) {
                _from[i] = _cover[i];
            }
            return _from;
        }
    }

    //  cookie设置
    function setCookie(value) {
        var d = new Date();
        var offset = 8;
        var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        var nd = utc + (3600000 * offset);
        var exp = new Date(nd);
        exp.setTime(exp.getTime() + 24 * 7 * 60 * 60 * 1000);
        document.cookie = "ZL_CUSTOMER_LOGIN_NAME=" + escape(value) + ";path=/;expires=" + exp.toGMTString();
    }

    // 业务处理
    var loginCtrl = {
        login: function () {
            var errorMsgEle = document.getElementById('error_msg');
            errorMsgEle.innerHTML = '';
            var loginName = document.getElementById('login_name').value;
            var password = document.getElementById('password').value;
            if (!loginName) {
                document.getElementById('error_msg').innerHTML = '账号不可为空';
                return;
            }
            if (!password) {
                document.getElementById('error_msg').innerHTML = '密码不可为空';
                return;
            }
            // 登录
            ajax({
                url: '/zl/login',
                type: 'post',
                dataType: 'json',
                params: {
                    phone: loginName,
                    password: password
                },
                success: function (data) {
                    if (data.success) {
                        setCookie(data.data);
                        location.href = urlPath + '/zl/index';
                        return;
                    }
                    errorMsgEle.innerHTML = data.message;
                }
            });
        }
    };
</script>
</html>
