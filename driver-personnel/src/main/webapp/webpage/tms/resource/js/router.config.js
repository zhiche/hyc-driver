/**
 * Created by Tuffy on 16/4/19.
 */
'use strict';

angular.module('driverApp', ['ionic', 'driver.controllers', 'driver.services'])

// 初始化全局变量设置
    .constant('Settings', {
        Context: {
            path: contextPath,
        }
    })

    // 初始化
    .run(['$rootScope', '$timeout', '$state', '$location', '$ionicPopup', '$ionicActionSheet', 'Settings', function ($rootScope, $timeout, $state, $location, $ionicPopup, $ionicActionSheet, Settings) {

        // 我的组件
        $rootScope.FIREFLY = {
            toast: function (option) {
                var that = this;
                that.toast.option = angular.extend({
                    title: '',
                    timeOut: 3000,
                    okCallback: null,
                }, option);
                that.toast.flag = true;
                $timeout(function () {
                    that.toast.flag = false;
                    if (that.toast.option.okCallback) {
                        that.toast.option.okCallback();
                    }
                }, that.toast.option.timeOut);
            },
            isMobile: function (phone) {
                var reg = new RegExp('^1\\d{10}$');
                return reg.test(phone);
            }
        };

        // 设置title信息
        document.title = '慧运车-中联客服管理';

    }])

    // 配置
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$ionicConfigProvider', function ($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider) {

        $ionicConfigProvider.platform.android.tabs.position('bottom');
        $ionicConfigProvider.platform.android.navBar.alignTitle('center');
        $ionicConfigProvider.platform.ios.views.transition('ios');
        $ionicConfigProvider.platform.android.views.transition('android');

        // $ionicConfigProvider.views.maxCache(0);
        $stateProvider
            .state('index', {
                url: '/index',
                templateUrl: contextPath + '/webpage/tms/resource/templates/index-view.html',
                controller: 'IndexCtrl'
            })
            .state('orders', {
                url: '/orders',
                templateUrl: contextPath + '/webpage/tms/resource/templates/orders-view.html',
                controller: 'OrdersCtrl',
                cache: false
            })
            .state('query-order', {
                url: '/query-order',
                templateUrl: contextPath + '/webpage/tms/resource/templates/query-order-view.html',
                controller: 'QueryOrderCtrl',
                cache: false
            })
            .state('drivers', {
                url: '/drivers',
                templateUrl: contextPath + '/webpage/tms/resource/templates/drivers-view.html',
                controller: 'DriversCtrl',
                cache: false
            })
            .state('chargeback', {
                url: '/chargeback',
                templateUrl: contextPath + '/webpage/tms/resource/templates/chargeback-view.html',
                controller: 'ChargeBackCtrl',
                cache: false
            })
            .state('user-photo', {
                url: '/user-photo',
                templateUrl: contextPath + '/webpage/tms/resource/templates/user-photo-view.html',
                controller: 'UserPhotoCtrl',
                cache: false
            })

        $urlRouterProvider.otherwise(otherwise);

        // 使angular $http post提交和jQuery一致
        $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

        // Override $http service's default transformRequest
        $httpProvider.defaults.transformRequest = [function (data) {
            /**
             * The workhorse; converts an object to x-www-form-urlencoded serialization.
             * @param {Object} obj
             * @return {String}
             */
            var param = function (obj) {
                var query = '';
                var name, value, fullSubName, subName, subValue, innerObj, i;

                for (name in obj) {
                    value = obj[name];

                    if (value instanceof Array) {
                        for (i = 0; i < value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    } else if (value instanceof Object) {
                        for (subName in value) {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    } else if (value !== undefined && value !== null) {
                        query += encodeURIComponent(name) + '='
                            + encodeURIComponent(value) + '&';
                    }
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            return angular.isObject(data) && String(data) !== '[object File]'
                ? param(data)
                : data;
        }];
    }])