/**
 * Created by Tuffy on 2016/11/10.
 */
'use strict';

angular.module('driver.services', [])

// 全局服务请求service. 调用时请调用此服务, 便于全局管理
    .service('HttpService', ['$http', '$q', '$state', 'Settings', function ($http, $q, $state, Settings) {
        /**
         * 获取cookie值
         * @param name cookie名称
         * @returns {null} cookie值
         */
        function getCookie(name) {
            var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
            if (arr != null)
                return unescape(arr[2]);
            return null;
        }

        var loginCode = '99999';

        return {
            post: function (url, params) {
                var deferred = $q.defer();
                return $http.post(Settings.Context.path + url, params, {
                    headers: {
                        'TmsCustomer': getCookie('ZL_CUSTOMER_LOGIN_NAME')
                    }
                })
                    .success(function (data, status, headers, config) {
                        if (data.messageCode == loginCode) {
                            location.href = Settings.Context.path + '/zl';
                            return;
                        }
                        deferred.resolve(data, status, headers, config);
                    })
                    .error(function (data, status, headers, config) {
                        deferred.reject(data, status, headers, config);
                    })
                return deferred.promise;
            },
            get: function (url) {
                var deferred = $q.defer();
                return $http.get(Settings.Context.path + url, {
                    headers: {
                        'TmsCustomer': getCookie('ZL_CUSTOMER_LOGIN_NAME')
                    }
                })
                    .success(function (data, status, headers, config) {
                        if (data.messageCode == loginCode) {
                            location.href = Settings.Context.path + '/zl';
                            return;
                        }
                        deferred.resolve(data, status, headers, config);
                    })
                    .error(function (data, status, headers, config) {
                        deferred.reject(data, status, headers, config);
                    })
                return deferred.promise;
            }
        };

    }])

    .service('IndexService', ['HttpService', function (HttpService) {
        var url = '/zl';
        return {
            addUser: function (params) {
                var action = '/add';
                return HttpService.post(url + action, params);
            },
            license: function () {
                var action = '/license';
                return HttpService.get(url + action);
            },
            activationUser: function (phone) {
                var action = '/open';
                return HttpService.post(url + action, {
                    phone: phone
                });
            },
            disabledUser: function (phone) {
                var action = '/disable';
                return HttpService.post(url + action, {
                    phone: phone
                });
            },
            cancelOrder: function (params) {
                var action = '/cancel';
                return HttpService.post(url + action, params);
            },
            resetOrder: function (orderCodes) {
                var action = '/reset';
                return HttpService.post(url + action, {
                    orderCodes: orderCodes
                });
            },
            idnoOrder: function (idCard) {
                var action = '/idno?idCard=' + idCard;
                return HttpService.get(url + action);
            },
            exchangeOrder: function (idCard, phone) {
                var action = '/exchange';
                return HttpService.post(url + action, {
                    idCard: idCard,
                    phone: phone
                });
            },
            exceptionOrder: function (orderCodes) {
                var action = '/exception';
                return HttpService.post(url + action, {
                    orderCodes: orderCodes
                });
            },
            orderList: function () {
                var action = '/orders';
                return HttpService.get(url + action);
            },
            driverList: function () {
                var action = '/drivers';
                return HttpService.get(url + action);
            },
            receipt: function (params) {
                var action = '/receipt';
                return HttpService.post(url + action, params);
            },
            chargebackList: function (orderCode) {
                var action = '/chargeback?orderCode=' + orderCode;
                return HttpService.get(url + action);
            },
            logout: function () {
                var action = '/logout';
                return HttpService.get(url + action);
            },
            addOrderToRedis: function (params) {
                var action = '/addordertoredis';
                return HttpService.post(url + action, params);
            },
            queryOrder: function (orderCode) {
                var action = '/queryorder?orderCode=' + orderCode;
                return HttpService.get(url + action);
            },
            userList: function (phone) {
                var action = '/userList?phone=' + phone;
                return HttpService.get(url + action);
            },
            savePic: function (params) {
                var action = '/updatepic';
                return HttpService.post(url + action, params);
            }
        };
    }])

    .service('QiniuService', ['HttpService', function (HttpService) {
        var url = '/qiniu';
        return {
            // 获取上传凭证LOGO
            getUploadToken: function () {
                var action = '/upload/brandlogo';
                return HttpService.get(url + action);
            },
            getUploadQiniuToken: function () {
                var action = '/upload/ticket';
                return HttpService.get(url + action);
            },
            // 获取下载凭证LOGO
            getDownloadTicket: function (params) {
                var action = '/downloadbrand/ticket';
                return HttpService.get(url + action, params);
            },
            // 获取下载授权
            getDownloadQiniuTicket: function (key) {
                var action = '/download/ticket?key=' + key + "&mode=1&w=83&h=83";
                return HttpService.get(url + action);
            }
        };
    }])