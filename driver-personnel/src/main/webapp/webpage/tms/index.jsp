<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String urlPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta name="format-detection" content="telephone=no"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">

    <link href="<%=urlPath %>/webpage/tms/resource/ionic/css/ionic.min.css" type="text/css" rel="stylesheet">
    <link href="<%=urlPath %>/webpage/tms/resource/css/driver.css" type="text/css" rel="stylesheet">
    <title>慧运车-中联客服管理</title>
</head>
<body ng-app='driverApp'>
<ion-nav-view></ion-nav-view>
<div ng-include="'<%=urlPath %>/webpage/tms/resource/templates/components/friefly-toast-components.html'"></div>
<script type="text/javascript" src="<%=urlPath %>/webpage/tms/resource/ionic/js/ionic.bundle.min.js"></script>
<script type="text/javascript">
    var contextPath = '<%=urlPath %>';
    var otherwise = 'index';
</script>
<script type="text/javascript" src="<%=urlPath %>/webpage/tms/resource/js/tools.js"></script>
<script type="text/javascript" src="<%=urlPath %>/webpage/tms/resource/js/router.config.js"></script>
<script type="text/javascript" src="<%=urlPath %>/webpage/tms/resource/js/driver.service.js"></script>
<script type="text/javascript" src="<%=urlPath %>/webpage/tms/resource/js/driver.controllers.js"></script>
<script type="text/javascript"
        src="<%=urlPath %>/webpage/tms/resource/ionic/js/angular/angular-local-storage.min.js"></script>
<script type="text/javascript" src="<%=urlPath %>/webpage/tms/resource/ionic/js/angular/qupload.js"></script>
</body>
</html>
