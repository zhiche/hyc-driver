package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.TmsOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @FileName: cn.huiyunche.driver.controller
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/9 下午8:45
 */
@Controller
@RequestMapping("/tmsorder")
public class TmsOrderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TmsOrderController.class);

    @Autowired
    private TmsOrderService tmsOrderService;


}
