package cn.huiyunche.driver.controller.ils;

import cn.huiyunche.base.service.form.DVehicleClassifyForm;
import cn.huiyunche.base.service.interfaces.DVehicleClassifyService;
import cn.huiyunche.base.service.interfaces.query.DVehicleClassifyQueryConditions;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @FileName: cn.huiyunche.driver.controller
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/2 下午9:09
 */
@Controller
@RequestMapping("/vehicle_classify")
public class DVehicleClassifyController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DVehicleClassifyController.class);

    @Autowired
    private DVehicleClassifyService dVehicleClassifyService;

    @RequestMapping(value = "/page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByPage(PageVo pageVo, @Valid DVehicleClassifyQueryConditions conditions) throws Exception {
        LOGGER.info("DVehicleClassifyController.listByPage params : {}, {}", pageVo, conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dVehicleClassifyService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("DVehicleClassifyController.listByPage error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * List result.
     *
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> list(@Valid DVehicleClassifyQueryConditions conditions) throws Exception {
        LOGGER.info("DVehicleClassifyController.list param : {}", conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dVehicleClassifyService.selectListByConditions(null, conditions).get("list"));
        } catch (Exception e) {
            LOGGER.error("DVehicleClassifyController.list error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Add result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> add(@Valid DVehicleClassifyForm form, BindingResult br) throws Exception {
        LOGGER.info("DVehicleClassifyController.add param : {}", form);
        Result<Object> result = new Result<>(true, null, "添加成功");
        try {
            result.setData(dVehicleClassifyService.add(form, br));
        } catch (Exception e) {
            LOGGER.error("DVehicleClassifyController.add error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Update result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> update(@Valid DVehicleClassifyForm form, BindingResult br) throws Exception {
        LOGGER.info("DVehicleClassifyController.update param : {}", form);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            result.setData(dVehicleClassifyService.update(form, br));
        } catch (Exception e) {
            LOGGER.error("DVehicleClassifyController.update error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Select by id route.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> getById(Integer id) throws Exception {
        LOGGER.info("DVehicleClassifyController.getById param : {}", id);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dVehicleClassifyService.selectByPrimaryId(id));
        } catch (Exception e) {
            LOGGER.error("DVehicleClassifyController.getById error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Enable or disabled route.
     *
     * @param id
     * @param enableOrDisabled
     * @return
     */
    @RequestMapping(value = "/enableOrDisabled", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> enableOrDisabled(Integer id, boolean enableOrDisabled) throws Exception {
        LOGGER.info("route enableOrDisabled params: {}, {}", id, enableOrDisabled);
        Result<Object> result = new Result<>(true, null, "操作成功");
        try {
            result.setData(this.dVehicleClassifyService.enableOrDisabled(id, enableOrDisabled));
        } catch (Exception e) {
            LOGGER.error("route enableOrDisabled error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Del result.
     *
     * @param id the id
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> del(Integer id) throws Exception {
        LOGGER.info("DVehicleClassifyController.del params: {}", id);
        Result<Object> result = new Result<>(true, null, "删除成功");
        try {
            dVehicleClassifyService.del(id);
        } catch (Exception e) {
            LOGGER.error("DVehicleClassifyController.del error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
