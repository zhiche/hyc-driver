package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.model.DVcstyleLicense;
import cn.huiyunche.base.service.vo.DVcstyleLicenseVo;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DVcstyleLicenseService;
import cn.huiyunche.driver.service.TmsAdminService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/tmsadmin")
public class TmsAdminController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TmsAdminController.class);

    @Autowired
    private TmsAdminService tmsAdminService = null;

    @Autowired
    private DVcstyleLicenseService dVcstyleLicenseService;


    /**
     * 时效统计数据列表
     *
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @return 结果集
     */
    @RequestMapping(value = "/aging", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<Map<String, Object>>> getAgingList(String startDate, String endDate, String status) {
        return this.tmsAdminService.getAgingList(startDate, endDate, status);
    }

    /**
     * 获得准驾车型列表
     *
     * @param
     * @return 结果集
     */
    @RequestMapping(value = "/stylelicenselist", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> stylelicenselist(@ModelAttribute PageVo page, DVcstyleLicenseVo dVcstyleLicenseVo) throws BusinessException {
        LOGGER.info("stylelicenselist params page: {}", page);
        Result<Map<String, Object>> result = new Result<>(true, "获得准驾车型列表成功");
        result.setData(dVcstyleLicenseService.getListByPage(page, dVcstyleLicenseVo));
        return result;
    }

    /**
     * 根据车型编号获得准驾车型
     *
     * @param vcCode 车型编号
     * @return 结果集
     */
    @RequestMapping(value = "/getvcstylebystylecode", method = RequestMethod.GET)
    @ResponseBody
    public Result<DVcstyleLicense> getvcstylebystylecode(String vcCode) throws BusinessException {
        if (StringUtils.isBlank(vcCode)) {
            throw new IllegalArgumentException("车型编号 不能为空");
        }
        LOGGER.info("getvcstylebystylecode params vcCode: {}", vcCode);
        Result<DVcstyleLicense> result = new Result<>(true, "根据车型编号获得准驾车型成功");
        DVcstyleLicense dVcstyleLicense = dVcstyleLicenseService.getVcStyleByStyleCode(vcCode);
        result.setData(dVcstyleLicense);
        return result;
    }

    /**
     * 添加准驾车型
     *
     * @param dVcstyleLicense 准驾车型实体
     * @return 结果集
     */
    @RequestMapping(value = "/adddvcstylelicense", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> adddvcstylelicense(@Valid DVcstyleLicense dVcstyleLicense, BindingResult br) throws BusinessException {
        if (br.hasErrors()) {
            String errorMessage = br.getAllErrors().get(0).getDefaultMessage();
            LOGGER.info("editdriver method has error : " + errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }
        LOGGER.info("adddvcstylelicense params dVcstyleLicense: {}", dVcstyleLicense);
        dVcstyleLicenseService.addDvcStyleLicense(dVcstyleLicense);
        Result<Object> result = new Result<>(true, null, "添加准驾车型成功");
        return result;
    }

    /**
     * 修改准驾车型
     *
     * @param dVcstyleLicense 准驾车型实体
     * @return 结果集
     */
    @RequestMapping(value = "/modifydvcstylelicense", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> modifydvcstylelicense(@Valid DVcstyleLicense dVcstyleLicense, BindingResult br) throws BusinessException {
        if (br.hasErrors()) {
            String errorMessage = br.getAllErrors().get(0).getDefaultMessage();
            LOGGER.info("editdriver method has error : " + errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }
        LOGGER.info("modifydvcstylelicense params dVcstyleLicense: {}", dVcstyleLicense);
        if (StringUtils.isBlank(dVcstyleLicense.getId() + "")) {
            throw new BusinessException("id不能为空");
        }
        dVcstyleLicenseService.modifyDvcStyleLicense(dVcstyleLicense);
        Result<Object> result = new Result<>(true, null, "修改准驾车型成功");
        return result;
    }

    /**
     * 删除准驾车型
     *
     * @param id 准驾车型ID
     * @return 结果集
     */
    @RequestMapping(value = "/deletedvcstylelicense", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> deletedvcstylelicense(String id) throws BusinessException {
        LOGGER.info("deletedvcstylelicense params id: {}", id);
        if (StringUtils.isBlank(id)) {
            throw new IllegalArgumentException("id不能为空");
        }
        dVcstyleLicenseService.deleteDvcStyleLicense(id);
        Result<Object> result = new Result<>(true, null, "删除准驾车型成功");
        return result;
    }
}
