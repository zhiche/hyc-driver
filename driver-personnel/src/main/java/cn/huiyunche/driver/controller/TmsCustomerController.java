package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.TmsQueueService;
import cn.huiyunche.base.service.interfaces.UserService;
import cn.huiyunche.base.service.model.BLicenseType;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.base.service.vo.TmsDriverVo;
import cn.huiyunche.driver.service.TmsCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 中联客服控制器
 *
 * @author hdy [Tuffy]
 */
@Controller
@RequestMapping("/zl")
public class TmsCustomerController {

    @Autowired
    private TmsCustomerService tmsCustomerService = null;

    @Autowired
    private TmsQueueService tmsQueueService = null;

    @Autowired
    private UserService userService = null;

    private UserService getUserService() {
        return this.userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String init() {
        return "tms/login";
    }

    /**
     * 登录
     *
     * @param phone    手机号
     * @param password 密码
     * @return 登录信息
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> login(HttpServletRequest request, String phone, String password) {
        return this.tmsCustomerService.login(request, phone, password);
    }

    /**
     * 退出登录
     *
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> logout(HttpServletRequest request) {
        return this.tmsCustomerService.logout(request);
    }

    /**
     * 首页
     *
     * @return 首页信息
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {
        return this.tmsCustomerService.index(request);
    }

    /**
     * 开通账号
     *
     * @param phone 司机手机号
     * @return
     */
    @RequestMapping(value = "/open", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> openAccount(HttpServletRequest request, @RequestParam("phone") String phone) {
        return this.tmsCustomerService.openAccount(request, phone);
    }

    /**
     * 添加司机
     *
     * @param tdv 司机表单
     * @return 结果集
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> addDriver(@Valid TmsDriverVo tdv, BindingResult br) {
        if (br.hasErrors()) {
            String errorMessage = br.getAllErrors().get(0).getDefaultMessage();
            throw new IllegalArgumentException(errorMessage);
        }
        return this.tmsCustomerService.addDriver(tdv);
    }

    /**
     * 获取证件类型列表
     *
     * @return
     */
    @RequestMapping(value = "/license", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<BLicenseType>> getBLicenseList() {
        return this.tmsCustomerService.getBLicenseList();
    }

    /**
     * 修改用户手机号，并开通
     *
     * @param request 请求
     * @param idCard  身份证
     * @param phone   手机号
     * @return 结果集
     */
    @RequestMapping(value = "/exchange", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> exchangeUser(HttpServletRequest request, String idCard, String phone) {
        return this.tmsCustomerService.exchangeAccount(request, idCard, phone);
    }

    /**
     * 通过身份证号码获取用户信息
     *
     * @param request 请求
     * @param idCard  身份证
     * @return 结果集
     */
    @RequestMapping(value = "/idno", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> getAccountByIdNo(HttpServletRequest request, String idCard) {
        return this.tmsCustomerService.getAccountByIdNo(request, idCard);
    }

    /**
     * 禁用账号
     *
     * @param phone 司机手机号
     * @return
     */
    @RequestMapping(value = "/disable", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> disableAccount(HttpServletRequest request, @RequestParam("phone") String phone) {
        return this.tmsCustomerService.disableAccount(request, phone);
    }

    /**
     * Cancel user waybill result.
     *
     * @param orderCode the order code
     * @return the result
     */
    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<String> cancelUserWaybill(HttpServletRequest request, @RequestParam("orderCode") String orderCode, String cancelreason, String cancelflag) {
        return tmsCustomerService.cancelDWaybillWithUser(request, orderCode, cancelreason, cancelflag);
    }

    /**
     * 置顶订单数据
     *
     * @param orderCodes 订单编码
     * @return 结果集
     */
    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> resetQueueOrder(String orderCodes) throws Exception {
        return this.tmsQueueService.resetQueueOrder(orderCodes);
    }

    /**
     * 异常订单
     *
     * @param request    请求
     * @param orderCodes 订单对象
     * @return 结果集
     */
    @RequestMapping(value = "/exception", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> exceptionOrderList(HttpServletRequest request, String orderCodes) {
        return this.tmsCustomerService.exceptionConfirmResult(request, orderCodes);
    }

    /**
     * 获取队列订单信息
     *
     * @param request 请求
     * @return 结果集
     */
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> queueOrderList(HttpServletRequest request) throws Exception {
        return this.tmsCustomerService.queueOrderList(request);
    }

    /**
     * 获取队列司机信息
     *
     * @param request 请求
     * @return 结果集
     */
    @RequestMapping(value = "/drivers", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> queueDriverList(HttpServletRequest request) throws Exception {
        return this.tmsCustomerService.queueDriverList(request);
    }

    /**
     * 回单
     *
     * @param request    请求
     * @param phone      手机号
     * @param orderCodes 订单号
     * @return 结果集
     */
    @RequestMapping(value = "/receipt", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> receipt(HttpServletRequest request, String phone, String orderCodes) throws Exception {
        return this.tmsCustomerService.receipt(request, phone, orderCodes);
    }

    /**
     * 退单列表
     *
     * @param request
     * @param orderCode
     * @return
     */
    @RequestMapping(value = "/chargeback", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> calcenList(HttpServletRequest request, String orderCode) {
        return this.tmsCustomerService.calcenList(request, orderCode);
    }

    /**
     * 订单重新入队
     *
     * @param request
     * @param orderCode
     * @return
     */
    @RequestMapping(value = "/addordertoredis", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> addOrderToRedis(HttpServletRequest request, String orderCode) throws Exception {
        return this.tmsCustomerService.addOrderToRedis(request, orderCode);
    }

    /**
     * 根据订单号查询订单信息
     *
     * @param request
     * @param orderCode
     * @return
     */
    @RequestMapping(value = "/queryorder", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> queryOrder(HttpServletRequest request, String orderCode) {
        return this.tmsCustomerService.queryOrder(request, orderCode);
    }

    /**
     * Select Users By Phone
     *
     * @param request
     * @param phone
     * @return
     */
    @RequestMapping(value = "/userList", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> userList(HttpServletRequest request, String phone) {
        return this.tmsCustomerService.userList(request, phone);
    }

    /**
     * 头像采集
     *
     * @param id
     * @param pic
     * @return
     */
    @RequestMapping(value = "/updatepic", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> updatePic(Long id, String pic) {
        Result<Object> result = new Result<>(true, null, "上传成功");
        result.setData(this.getUserService().updatePicById(id, pic));
        return result;
    }
}
