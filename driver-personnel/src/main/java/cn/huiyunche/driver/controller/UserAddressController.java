package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.DUserAddressService;
import cn.huiyunche.base.service.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/addr")
public class UserAddressController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserAddressController.class);

    @Autowired
    private DUserAddressService dUserAddressService = null;

    private DUserAddressService getDUserAddressService() {
        return this.dUserAddressService;
    }

    /**
     * 用户意愿路线新增，修改
     *
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public Result<Object> modify(String addrs) throws Exception {
        LOGGER.info("modify params: {}", addrs);
        Result<Object> result = new Result<Object>(true, null, "数据操作已成功");
        result.setData(this.getDUserAddressService().modify(addrs));
        return result;
    }

    /**
     * 查询用户意愿路线
     *
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<Object> list() throws Exception {
        Result<Object> result = new Result<Object>(true, null, "查询成功");
        result.setData(this.getDUserAddressService().getList());
        return result;
    }

    /**
     * 删除用户意愿路线
     *
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result<Object> del(Long id) throws Exception {
        LOGGER.info("del params id: {}", id);
        Result<Object> result = new Result<Object>(true, null, "数据删除成功");
        result.setData(this.getDUserAddressService().del(id));
        return result;
    }
}
