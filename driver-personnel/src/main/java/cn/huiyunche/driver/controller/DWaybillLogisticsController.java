package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.model.DWaybillLogistics;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.WaybillLogisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/waybillLogistics")
public class DWaybillLogisticsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DWaybillLogisticsController.class);

    @Autowired
    private WaybillLogisticsService waybillLogisticsService = null;

    private WaybillLogisticsService getWaybillLogisticsService() {
        return this.waybillLogisticsService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByCondition(DWaybillLogistics logistics) {
        LOGGER.info("DWaybillLogisticsController.listByCondition params: {}", logistics);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(this.getWaybillLogisticsService().listByConditions(logistics));
        } catch (Exception e) {
            LOGGER.error("DWaybillLogisticsController.listByCondition error: {}", e);
        }
        return result;
    }

}
