package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.SUserBindDeviceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/binddevice")
public class SUserBindDeviceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SUserBindDeviceController.class);

    @Autowired
    private SUserBindDeviceService sUserBindDeviceService = null;

    private SUserBindDeviceService getSUserBindDeviceService() {
        return this.sUserBindDeviceService;
    }

    /**
     * 账号设备绑定列表
     *
     * @param pageVo
     * @param phone
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<Object> list(PageVo pageVo, String phone) {
        LOGGER.info("list params pageVo: {}, phone: {}", pageVo, phone);
        return this.getSUserBindDeviceService().selectSUserBindDeviceByExample(pageVo, phone);
    }

    /**
     * 账号设备解绑
     *
     * @param pageVo
     * @param phone
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/unbind", method = RequestMethod.POST)
    public Result<Object> unbind(Integer id) {
        LOGGER.info("unbind params id: {}", id);
        return this.getSUserBindDeviceService().updateBindStatus(id);
    }

}
