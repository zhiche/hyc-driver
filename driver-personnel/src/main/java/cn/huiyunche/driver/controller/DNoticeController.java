package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.model.DNotice;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DNoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 通知Controller
 *
 * @author lp
 */
@Controller
@RequestMapping("/notice")
public class DNoticeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DNoticeController.class);

    @Autowired
    private DNoticeService dNoticeService = null;

    private DNoticeService getDNoticeService() {
        return this.dNoticeService;
    }

    /**
     * 显示通知列表
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> getPageInfo(@ModelAttribute PageVo page) {
        LOGGER.info("select dnotice list params page : {}", page);
        Result<Map<String, Object>> result = new Result<>(true, "查询成功！");
        result.setData(this.getDNoticeService().selectDNoticesByUserPage(page));
        return result;
    }

    /**
     * 显示通知内容
     *
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Result<DNotice> getNoticeByID(@PathVariable long id) {
        LOGGER.info("select notice params id : {}", id);
        Result<DNotice> result = new Result<DNotice>(true, "查询成功！");
        this.getDNoticeService().updateDNoticeStatusByID(id);
        result.setData(this.getDNoticeService().selectDNoticeByID(id));
        return result;
    }

}
