package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.CAccntBillService;
import cn.huiyunche.base.service.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * 生成账单Controller－调用接口
 *
 * @author lm
 */
@Controller
@RequestMapping("/accntBill")
public class AccntBillController {

    private static final Logger log = LoggerFactory.getLogger(AccntBillController.class);

    @Autowired
    private CAccntBillService accntBillService = null;

    public CAccntBillService getAccntBillService() {
        return accntBillService;
    }

    /**
     * 司机周账单
     *
     * @param beginDate
     * @param endDate
     * @param updateBalance
     * @param redo
     * @param userId
     * @param userType
     * @return
     */
    @RequestMapping(value = "/genMonthBill", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> genMonthBill(@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date beginDate, @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate, Boolean updateBalance, Boolean redo,
                                       Long userId, int userType) {
        log.info("gen driver week bill controller params beginDate:{}, endDate:{}, upudateBalance:{}, redo:{}, userId:{}, userType:{}.", beginDate, endDate, updateBalance, redo, userId, userType);
        Result<String> result = new Result<String>(true, "生成司机周账单完成.");
        boolean flag = this.getAccntBillService().genMonthBill(beginDate, endDate, updateBalance, redo, userId, userType);
        if (!flag) {
            result.setSuccess(false);
            result.setMessage("系统异常.");
        }
        return result;
    }

}
