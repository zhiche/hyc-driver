package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.DWaybillAttachService;
import cn.huiyunche.base.service.vo.DWaybillAttachVo;
import cn.huiyunche.base.service.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/attach")
public class DWaybillAttachController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DWaybillAttachController.class);

    @Autowired
    private DWaybillAttachService dWaybillAttachService = null;

    private DWaybillAttachService getDWaybillAttachService() {
        return this.dWaybillAttachService;
    }

    /**
     * 带伤发运图片上传
     *
     * @param waybillId
     * @param picKeys
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> add(String waybillId, String picKeys) throws Exception {
        LOGGER.info("add params waybillId: {}, picKeys: {}", waybillId, picKeys);
        return this.getDWaybillAttachService().modify(waybillId, picKeys);
    }

    /**
     * 查询带伤发运图片
     *
     * @param waybillId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/listpic", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<DWaybillAttachVo>> listPic(Long waybillId) throws Exception {
        return this.getDWaybillAttachService().listPic(waybillId);
    }
}
