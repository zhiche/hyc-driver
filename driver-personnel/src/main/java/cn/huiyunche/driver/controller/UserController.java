package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.DWaybillService;
import cn.huiyunche.base.service.interfaces.UserService;
import cn.huiyunche.base.service.vo.*;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService = null;

    @Autowired
    private DWaybillService dWaybillService = null;

    private DWaybillService getDWaybillService() {
        return this.dWaybillService;
    }

    private UserService getUserService() {
        return this.userService;
    }


    /**
     * @return
     * @Title: query
     * @Description: 查询当前用户信息
     * @return: Result<String>
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Result<UserShowVo> query() throws Exception {
        Result<UserShowVo> result = new Result<UserShowVo>(true);
        result.setData(this.getUserService().getCurrentUserInfo());
        return result;
    }

    /**
     * @param form 用户表单
     * @return
     * @throws Exception
     * @Title: updateUser
     * @Description: 更新用户信息
     * @return: Result<String>
     */
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    @ResponseBody
    public Result<UserShowVo> updateUser(@Valid UserUpdateVo form, BindingResult br) throws Exception {
        LOGGER.info("UserController.updateUser param:{}", form.toString());
        Result<UserShowVo> result = new Result<>(true, "修改成功!");
        if (br.hasErrors()) {
            List<ObjectError> list = br.getAllErrors();
            result.setSuccess(false);
            result.setMessage(list.get(0).getDefaultMessage());
            return result;
        }
        UserShowVo userMap = this.getUserService().updateUserInfo(form);
        result.setData(userMap);
        LOGGER.info("UserController.updateUser return :{}", userMap);
        return result;
    }

    /**
     * 用户上线 old 为防止黄牛，将此方法屏蔽掉
     *
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/online", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> onLine() throws Exception {
        LOGGER.error("UserController.online :您的app非最新版本，请去应用市场更新");
        throw new BusinessException("您的app非最新版本，请去应用市场更新");
    }
    /**
     * 用户上线
     *
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/loginOnline", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> loginOnline() throws Exception {
        Result<Object> result = new Result<Object>(true, null, "上线成功");
        result.setData(this.getUserService().online());
        return result;
    }

    /**
     * 用户下线
     *
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/offline", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> offLine() throws Exception {
        Result<Object> result = new Result<Object>(true, null, "下线成功");
        result.setData(this.getUserService().offline());
        ShowOrderVo orderVo = this.getDWaybillService().getIntransitListByPage();
        result.setMessageCode("200");
        if (orderVo != null) {
            result.setMessageCode("500");
        }
        return result;
    }

    /**
     * 刷新排队
     *
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/queue", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> refresh() throws Exception {
        Result<Object> result = new Result<Object>(true, null, "刷新成功");
        result.setData(this.getUserService().refresh());
        return result;
    }

    /**
     * 校验用户token
     *
     * @return
     */
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> checkUser() throws Exception {
        return this.getUserService().checkUser();
    }

    /**
     * 司机位置
     *
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/position", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> position(@Valid UserPositionVo upv, BindingResult br) throws Exception {
        Result<Object> result = new Result<Object>(true, null, "上报成功");
        if (br.hasErrors()) {
            List<ObjectError> list = br.getAllErrors();
            result.setSuccess(false);
            result.setMessage(list.get(0).getDefaultMessage());
            return result;
        }
        result.setData(this.getUserService().addUserPosition(upv));
        return result;
    }

}
