package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.DWaybillAbandonedLogService;
import cn.huiyunche.base.service.query.DWaybillAbandonedLogConditions;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.controller
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/30 上午11:22
 */
@Controller
@RequestMapping("/abandoned")
public class DWaybillAbandonedLogController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DWaybillAbandonedLogController.class);

    @Autowired
    private DWaybillAbandonedLogService dWaybillAbandonedLogService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @RequestMapping(value = "/page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByPage(PageVo pageVo, @Valid DWaybillAbandonedLogConditions conditions) throws Exception {
        LOGGER.info("DWaybillAbandonedLogController.listByPage params : {}, {}", pageVo, conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dWaybillAbandonedLogService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("DWaybillAbandonedLogController.listByPage error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
