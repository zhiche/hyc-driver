package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.TmsQueueService;
import cn.huiyunche.base.service.model.DUserExt;
import cn.huiyunche.base.service.model.DWaybill;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.QueueDriverVo;
import cn.huiyunche.base.service.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

/**
 * 人送 队列管理
 *
 * @author hdy [Tuffy]
 */
@Controller
@RequestMapping("/tmsqueue")
public class TmsQueueController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TmsQueueController.class);

    @Autowired
    private TmsQueueService tmsQueueService = null;

    /**
     * 订单重置顶排队
     *
     * @param orderCodes 订单编号
     * @return 结果集
     */
    @RequestMapping(value = "/top", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> topQueueOrder(String orderCodes) throws Exception {
        return this.tmsQueueService.resetQueueOrder(orderCodes);
    }

    /**
     * 获取队列订单信息
     *
     * @return 结果集
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<DWaybill>> queueOrderList() throws Exception {
        return this.tmsQueueService.queueOrderList();
    }


    /**
     * 删除队列订单信息
     *
     * @param orderCodes 订单编号
     * @return 结果集
     */
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> removeQueueOrder(String orderCodes) throws Exception {
        return this.tmsQueueService.removeQueueOrder(orderCodes);
    }

    /**
     * 获取队列司机信息
     *
     * @return 结果集
     */
    @RequestMapping(value = "/listdriver", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<DUserExt>> queueDriverList() throws Exception {
        return this.tmsQueueService.queueDriverList();
    }

    /**
     * 查询队列司机
     *
     * @param vo 查询条件
     * @param pageVo 分页参数
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/listQueueDriver", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> listQueueDriver(QueueDriverVo vo, PageVo pageVo) throws Exception {
        Result<Object> result = new Result<>(true, null, "查询数据成功");
        result.setData(tmsQueueService.listQueueDriverByCondition(vo, pageVo));
        return result;
    }

    @RequestMapping(value = "/exportExecl", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> exportExecl(HttpServletRequest request, HttpServletResponse response, QueueDriverVo vo) throws Exception {
        Result<Object> result = new Result<Object>(true, null, "导出成功");
        try {
            result.setData(tmsQueueService.exportExecl(vo));
        } catch (IOException e) {
            result.setSuccess(false);
            result.setMessage("导出失败");
            LOGGER.error("TmsQueueController.exportExecl error: {}", e);
        }
        return result;
    }

    /**
     * 删除队列司机信息
     *
     * @param phone 订单编号
     * @return 结果集
     */
    @RequestMapping(value = "/removedriver", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> removeQueueDriver(String phone) throws Exception {
        this.tmsQueueService.removeDriver(phone);
        return new Result<>(true, null, "删除成功");
    }

}