package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.DVehicleClassifyService;
import cn.huiyunche.base.service.interfaces.DWaybillFeeService;
import cn.huiyunche.base.service.model.DVehicleClassify;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DRouteService;
import cn.huiyunche.driver.service.dto.DestplaceDto;
import cn.huiyunche.driver.service.dto.StartplaceDto;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lenovo on 2017/10/19.
 */
@Controller
@RequestMapping("/settlementPrice")
public class SettlementPriceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SettlementPriceController.class);

    @Autowired
    private DWaybillFeeService dWaybillFeeService;

    @Autowired
    private DVehicleClassifyService dVehicleClassifyService;

    @Autowired
    private DRouteService dRouteService;

    /**
     * 查询结算价格
     * @param oProvince
     * @param tProvince
     * @param oTag
     * @param tTag
     * @param styleInfo
     * @return
     */
    @RequestMapping(value = "/findSettlementPrice", method = RequestMethod.GET)
    public @ResponseBody Result<Object> findSettlementPrice(String oProvince, String tProvince, String oTag, String tTag, String styleInfo) {
        LOGGER.info("SettlementPriceController.findSettlementPrice param: {0,1,2,3}", oProvince, tProvince, oTag, tTag, styleInfo);
        Result<Object> result = null;

        try {
            if (StringUtils.isEmpty(tProvince))
                throw new BusinessException("目的省不能为空");
            if (StringUtils.isEmpty(tTag))
                throw new BusinessException("目的地点不能为空");
            if (StringUtils.isEmpty(styleInfo))
                throw new BusinessException("车型不能为空");
            if (StringUtils.isEmpty(oProvince)) {
                oProvince = "江西";
            } else {
                oProvince = (new String(oProvince.getBytes("ISO-8859-1"), "utf-8"));
            }
            if (StringUtils.isEmpty(oTag)) {
                oTag = "南昌";
            }
            else {
                oTag = (new String(oTag.getBytes("ISO-8859-1"), "utf-8"));
            }

//            tProvince = (new String(tProvince.getBytes("ISO-8859-1"), "utf-8"));
//            tTag = (new String(tTag.getBytes("ISO-8859-1"), "utf-8"));
//            styleInfo = (new String(styleInfo.getBytes("ISO-8859-1"), "utf-8"));

            Map<String, Object> dataMap = dWaybillFeeService.findPrice(oProvince, tProvince, oTag, tTag, styleInfo);
            List<Map<String, Object>> listMap = new ArrayList<>();
            listMap.add(dataMap);
            result = new Result(true, listMap, "成功查询价格!");
        } catch (BusinessException e) {
            LOGGER.error("SettlementPriceController.findSettlementPrice businessException", e);
            result = new Result(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("SettlementPriceController.findSettlementPrice error: {}", e);
            result = new Result(false, "查询价格系统异常...");
        }

        return result;
    }

    /**
     * 获取车型
     * @return
     */
    @RequestMapping(value = "/findVehicleClassify", method = RequestMethod.GET)
    public @ResponseBody Result<Object> findVehicleClassify(String vehicleClassifyName) {
        LOGGER.info("SettlementPriceController.findSettlementPrice param: {0}", vehicleClassifyName);
        Result<Object> result = null;

        try {
            if (StringUtils.isEmpty(vehicleClassifyName))
                return null;
//            vehicleClassifyName = (new String(vehicleClassifyName.getBytes("ISO-8859-1"), "utf-8"));
            List<DVehicleClassify> dVehicleClassifyList = dVehicleClassifyService.findVehicleClassify(vehicleClassifyName);
            result = new Result(true, dVehicleClassifyList, "成功查询价格!");
        } catch (BusinessException e) {
            LOGGER.error("SettlementPriceController.findSettlementPrice businessException", e);
            result = new Result(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("SettlementPriceController.findSettlementPrice error: {}", e);
            result = new Result(false, "查询价格系统异常...");
        }

        return result;
    }

    /**
     * 获取目的地
     * @return
     */
    @RequestMapping(value = "/getStartInfo", method = RequestMethod.GET)
    public @ResponseBody Result<Object> getStartInfo(String oTag) {
        LOGGER.info("SettlementPriceController.getStartInfo param: {0}", oTag);
        Result<Object> result = null;

        try {
            if (StringUtils.isEmpty(oTag))
                return null;
//            oTag = (new String(oTag.getBytes("ISO-8859-1"), "utf-8"));
            List<StartplaceDto> startList = dRouteService.findByStartTag(oTag);
            result = new Result(true, startList, "成功查询起始地信息");
        } catch (BusinessException e) {
            LOGGER.error("SettlementPriceController.getStartInfo businessException", e);
            result = new Result(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("SettlementPriceController.getStartInfo error: {}", e);
            result = new Result(false, "获取起始地信息系统异常...");
        }

        return result;
    }

    /**
     * 获取目的地
     * @return
     */
    @RequestMapping(value = "/getDestInfo", method = RequestMethod.GET)
    public @ResponseBody Result<Object> getDestInfo(String dTag) {
        LOGGER.info("SettlementPriceController.getDestInfo param: {0}", dTag);
        Result<Object> result = null;

        try {
            if (StringUtils.isEmpty(dTag))
                return null;
//            dTag = (new String(dTag.getBytes("ISO-8859-1"), "utf-8"));
            List<DestplaceDto> destList = dRouteService.findByDestTag(dTag);
            result = new Result(true, destList, "成功查询目的地信息");
        } catch (BusinessException e) {
            LOGGER.error("SettlementPriceController.getDestInfo businessException", e);
            result = new Result(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("SettlementPriceController.getDestInfo error: {}", e);
            result = new Result(false, "获取目的地信息系统异常...");
        }

        return result;
    }
}
