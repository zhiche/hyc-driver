package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.TmsDUserService;
import cn.huiyunche.base.service.vo.DUserVo;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/driver")
public class DriverController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DriverController.class);

    @Autowired
    private TmsDUserService tmsDUserService = null;

    private TmsDUserService getTmsDUserService() {
        return this.tmsDUserService;
    }

    /**
     * 查询用户列表
     *
     * @param pageVo
     * @param dUserVo
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> list(PageVo pageVo, DUserVo dUserVo) throws Exception {
        LOGGER.info("list params PageVo: {}, DUserVo: {}", pageVo, dUserVo);
        Result<Object> result = new Result<>(true, null, "数据加载成功");
        result.setData(this.getTmsDUserService().userList(pageVo, dUserVo));
        return result;
    }

    /**
     * 禁用用户
     *
     * @param sId
     * @param dId
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/disabled", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> disabled(Long sId, Long dId) throws Exception {
        LOGGER.info("disabled params sId: {}, dId: {}", sId, dId);
        Result<Object> result = new Result<>(true, null, "禁用成功");
        result.setData(this.getTmsDUserService().disabled(sId, dId));
        return result;
    }

    /**
     * 启用用户
     *
     * @param sId
     * @param dId
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/enabled", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> enabled(Long sId, Long dId) throws Exception {
        LOGGER.info("enabled params sId: {}, dId: {}", sId, dId);
        Result<Object> result = new Result<>(true, null, "启用成功");
        result.setData(this.getTmsDUserService().enabled(sId, dId));
        return result;
    }

    /**
     * 修改手机号
     *
     * @param sId
     * @param dId
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/modifyphone", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> modifyphone(Long sId, Long dId, String phone) throws Exception {
        LOGGER.info("modifyphone params sId: {}, dId: {}, phone: {}", sId, dId, phone);
        Result<Object> result = new Result<>(true, null, "手机号修改成功");
        result.setData(this.getTmsDUserService().modifyPhone(sId, dId, phone));
        return result;
    }

    /**
     * 绑定油卡
     *
     * @param sId
     * @param oilCardCode
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/bindoilcard", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> bindOilCard(Long sId, String oilCardCode) throws Exception {
        LOGGER.info("bindOilCard params sId: {}, oilCardCode: {}", sId, oilCardCode);
        Result<Object> result = new Result<>(true, null, "油卡绑定成功");
        result.setData(this.getTmsDUserService().bindOilCard(sId, oilCardCode));
        return result;
    }
}
