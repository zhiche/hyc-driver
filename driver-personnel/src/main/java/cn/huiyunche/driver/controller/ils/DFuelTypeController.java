package cn.huiyunche.driver.controller.ils;

import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DFuelTypeService;
import cn.huiyunche.driver.service.form.DFuelTypeForm;
import cn.huiyunche.driver.service.query.DFuelTypeQueryConditions;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @FileName: cn.huiyunche.driver.controller.ils
 * @Description: 燃油类型控制器
 * @author: Aaron
 * @date: 2017/2/27 下午10:16
 */
@Controller
@RequestMapping("/fuel")
public class DFuelTypeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DFuelTypeController.class);

    @Autowired
    private DFuelTypeService dFuelTypeService;

    /**
     * List by page result.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByPage(PageVo pageVo, DFuelTypeQueryConditions conditions) throws Exception {
        LOGGER.info("DFuelTypeController.listByPage params : {}, {}", pageVo, conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dFuelTypeService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("DFuelTypeController.listByPage error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Add result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> add(@Valid DFuelTypeForm form, BindingResult br) throws Exception {
        LOGGER.info("DFuelTypeController.add param : {}", form);
        Result<Object> result = new Result<>(true, null, "添加成功");
        try {
            result.setData(dFuelTypeService.add(form, br));
        } catch (Exception e) {
            LOGGER.error("DFuelTypeController.add error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Update result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> update(DFuelTypeForm form, BindingResult br) throws Exception {
        LOGGER.info("DFuelTypeController.update param : {}", form);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            result.setData(dFuelTypeService.update(form, br));
        } catch (Exception e) {
            LOGGER.error("DFuelTypeController.add error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Del result.
     *
     * @param id the id
     * @return the result
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> del(Integer id) throws Exception {
        LOGGER.info("DFuelTypeController.del param : {}", id);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            dFuelTypeService.delete(id);
        } catch (Exception e) {
            LOGGER.error("DFuelTypeController.del error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Select FuelType By Id
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> getById(Integer id) throws Exception {
        LOGGER.info("DFuelTypeController.getById param: {}", id);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dFuelTypeService.selectByPrimaryKey(id));
        } catch (Exception e) {
            LOGGER.error("DFuelTypeController.getById error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }


    /**
     * List by conditions result.
     *
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> list(DFuelTypeQueryConditions conditions) throws Exception {
        LOGGER.info("DFuelTypeController.list params : {}", conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dFuelTypeService.selectListByConditions(conditions));
        } catch (Exception e) {
            LOGGER.error("DFuelTypeController.list error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
