package cn.huiyunche.driver.controller.ils;

import cn.huiyunche.base.service.form.ScPriceconfMileForm;
import cn.huiyunche.base.service.interfaces.ScPriceconfMileService;
import cn.huiyunche.base.service.query.ScPriceconfMileQueryConditions;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The type Sc priceconf freight controller.
 *
 * @FileName: cn.huiyunche.driver.controller.ils
 * @Description: 线路价格变更
 * @author: Aaron
 * @date: 2017 /3/13 下午3:51
 */
@Controller
@RequestMapping("/route_mile")
public class ScPriceconfMileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScPriceconfMileController.class);

    @Autowired
    private ScPriceconfMileService scPriceconfMileService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    /**
     * List by page result.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByPage(PageVo pageVo, @Valid ScPriceconfMileQueryConditions conditions) throws Exception {
        LOGGER.info("ScPriceconfMileController.listByPage params : {}, {}", pageVo, conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(scPriceconfMileService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("ScPriceconfMileController.listByPage error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     * @throws Exception the exception
     */
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> getById(Integer id) throws Exception {
        LOGGER.info("ScPriceconfMileController.getById param : {}", id);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(scPriceconfMileService.selectByPrimaryKey(id));
        } catch (Exception e) {
            LOGGER.error("ScPriceconfMileController.getById error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Add result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> add(@Valid ScPriceconfMileForm form, BindingResult br) throws Exception {
        LOGGER.info("ScPriceconfMileController.add param: {}", form);
        Result<Object> result = new Result<>(true, null, "添加成功");
        try {
            result.setData(scPriceconfMileService.add(form, br));
        } catch (Exception e) {
            LOGGER.error("ScPriceconfMileController.add error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Update result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> update(@Valid ScPriceconfMileForm form, BindingResult br) throws Exception {
        LOGGER.info("ScPriceconfMileController.update param : {}", form);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            scPriceconfMileService.update(form, br);
        } catch (Exception e) {
            LOGGER.error("ScPriceconfMileController.update error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Del result.
     *
     * @param id the id
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> del(Integer id) throws Exception {
        LOGGER.info("ScPriceconfMileController.del param : {}", id);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            scPriceconfMileService.del(id);
        } catch (Exception e) {
            LOGGER.error("ScPriceconfMileController.del error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
