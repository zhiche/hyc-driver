package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.form.SubCWGetForm;
import cn.huiyunche.base.service.interfaces.DSubSupplierService;
import cn.huiyunche.base.service.model.DWaybillLogistics;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.base.service.vo.SupplierRouteVo;
import cn.huiyunche.base.service.vo.TmsQueueOrderVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * The type Sub supplier controller.
 *
 * @FileName: cn.huiyunche.driver.controller.subsupplier
 * @Description: 分供方模块
 * @author: Aaron
 * @date: 2016 /12/6 下午2:02
 */
@Controller
@RequestMapping("/sub")
public class SubSupplierController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubSupplierController.class);

    @Autowired
    private DSubSupplierService subSupplierService;

    /**
     * Get cw list result.
     * 根据用户上报运力分配运单
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/cwlist", method = RequestMethod.POST)
    @ResponseBody
    public Result<List<TmsQueueOrderVo>> getDWList(@Valid SubCWGetForm form, BindingResult br) throws Exception {
        LOGGER.info("SubSupplierController.getDWList params : {}", form);

        if (br.hasErrors()) {
            LOGGER.error("SubSupplierController.getDWList error : {}", br.getAllErrors().get(0).getDefaultMessage());
            throw new IllegalArgumentException(br.getAllErrors().get(0).getDefaultMessage());
        }

        //拆分codes和names
        String[] codes = form.getCodes().split("-");
        String[] names = form.getNames().split("-");
        form.setOrginCode(codes[0]);
        form.setDestCode(codes[1]);
        form.setOrginName(names[0]);
        form.setDestName(names[1]);

        Result<List<TmsQueueOrderVo>> result = new Result<>(true, "申请成功");
        result.setData(subSupplierService.filterDWList(form));
        return result;
    }

    /**
     * Get waiting dw list result.
     * 我的运单-待接单列表
     *
     * @param page the page
     * @return the result
     */
    @RequestMapping(value = "/waiting", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> getWaitingDWList(@ModelAttribute PageVo page) throws Exception {
        Result<Map<String, Object>> result = new Result<>(true, "查询成功");
        result.setData(subSupplierService.getWaitingList(page));
        return result;
    }

    /**
     * Get execution dw list result.
     * 我的运单-执行中列表
     *
     * @param page the page
     * @return the result
     */
    @RequestMapping(value = "/execution", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> getExecutionDWList(@ModelAttribute PageVo page) throws Exception {
        Result<Map<String, Object>> result = new Result<>(true, "查询成功");
        result.setData(subSupplierService.getExecutionDWList(page));
        return result;
    }

    /**
     * Get complete dw list result.
     * 我的运单-已完成列表
     *
     * @param page the page
     * @return the result
     */
    @RequestMapping(value = "/complete", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> getCompleteDWList(@ModelAttribute PageVo page) throws Exception {
        Result<Map<String, Object>> result = new Result<>(true, "查询成功");
        result.setData(subSupplierService.getCompleteDWList(page));
        return result;
    }

    /**
     * Get drivers result.
     * 指派司机列表
     *
     * @param page the page
     * @return the result
     */
    @RequestMapping(value = "/freedrivers", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> getFreeDrivers(@ModelAttribute PageVo page) throws Exception {
        Result<Map<String, Object>> result = new Result<>(true, "查询成功");
        result.setData(subSupplierService.getDriversByUser(page, true));
        return result;
    }

    /**
     * Get drivers result.
     * 所有司机列表
     *
     * @param page the page
     * @return the result
     */
    @RequestMapping(value = "/drivers", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> getDrivers(@ModelAttribute PageVo page) throws Exception {
        Result<Map<String, Object>> result = new Result<>(true, "查询成功");
        result.setData(subSupplierService.getDriversByUser(page, false));
        return result;
    }

    /**
     * Specify driver result.
     *
     * @param wid the wid 运单主键
     * @param uid the uid 司机主键
     * @return the result
     */
    @RequestMapping(value = "/driver/specify", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> specifyDriver(@RequestParam("wid") Long wid, @RequestParam("uid") Long uid) throws Exception {
        Result<String> result = new Result<>(true, "派单成功");
        subSupplierService.specifyDriver(wid, uid);
        return result;
    }

    /**
     * Gets logistics.
     * 查询运单的物流信息
     *
     * @param id the id
     * @return the logistics
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/logistics", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<DWaybillLogistics>> getLogistics(@RequestParam("id") Long id) {
        Result<List<DWaybillLogistics>> result = new Result<>(true, "查询成功");
        result.setData((List<DWaybillLogistics>) subSupplierService.getLogisticsByDWId(null, id).get("list"));
        return result;
    }

    /**
     * Get routes result.
     * 查询分供方线路
     *
     * @return the result
     */
    @RequestMapping(value = "/routes", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<SupplierRouteVo>> getRoutes() throws Exception {
        Result<List<SupplierRouteVo>> result = new Result<>(true, "查询成功");
        result.setData(subSupplierService.getRoutes());
        return result;
    }
}
