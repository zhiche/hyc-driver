package cn.huiyunche.driver.controller;


import cn.huiyunche.base.service.constant.TmsUrlConstant;
import cn.huiyunche.base.service.enums.TmsUrlTypeEnum;
import cn.huiyunche.base.service.interfaces.TmsOrderCallHistoryService;
import cn.huiyunche.base.service.utils.DateUtils;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.schedule.QuartzClusterFactory;
import cn.huiyunche.driver.service.TmsQueryBillService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.apache.commons.collections4.map.HashedMap;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.Map;

/**
 * Created by Ligl on 2017/3/7.
 * author ligl
 */
@Controller
@RequestMapping("/tmsQueryBill")
public class TmsQueryBillController {

    private final static Logger LOGGER = LoggerFactory.getLogger(TmsQueryBillController.class);

    @Autowired
    private TmsQueryBillService tmsQueryBillService;

    /**
     * 查询tms账单
     *
     * @return 登录信息
     */
    @RequestMapping(value = "/queryBill", method = RequestMethod.POST)
    @ResponseBody
    public void queryBill() {
        Result<Map> result = new Result<>(true, "查询成功");
        LOGGER.info("queryBill begin: {}", new DateTime().toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));
        final String url = TmsUrlConstant.PULL_ORDER_BILL;

        TmsOrderCallHistoryService tmsOrderCallHistoryService = QuartzClusterFactory.getTmsOrderCallHistoryService();

        Map<String, Object> paramsMap = new HashedMap<>();
        String beginDateTime = DateTime.now().withDayOfWeek(1).minusDays(7).hourOfDay().withMinimumValue().minuteOfHour().withMinimumValue()
                .secondOfMinute().withMinimumValue().millisOfSecond().withMinimumValue().toString("yyyy-MM-dd HH:mm:ss");

        String endDateTime = DateTime.now().withDayOfWeek(1).hourOfDay().withMinimumValue().minuteOfHour().withMinimumValue()
                .secondOfMinute().withMinimumValue().millisOfSecond().withMinimumValue().toString("yyyy-MM-dd HH:mm:ss");
        paramsMap.put("beginDateTime",beginDateTime);
        paramsMap.put("endDateTime",endDateTime);

        String resultStr = tmsOrderCallHistoryService.urlCall(url, TmsUrlTypeEnum.PULL_ORDER_BILL.getValue(), paramsMap, 60000);


        LOGGER.info("zl queryBill  finish job result : {}.", resultStr);

        try {
            tmsQueryBillService.tmsQueryBill(resultStr);

        } catch (Exception e) {
            throw new BusinessException("查询tms账单错误" + e);
        }
        LOGGER.info("queryBill end: {}", new DateTime().toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));

    }
}
