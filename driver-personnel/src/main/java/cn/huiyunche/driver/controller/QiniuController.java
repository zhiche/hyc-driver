package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.framework.utils.QiniuUtils;
import cn.huiyunche.base.service.vo.Result;
import com.qiniu.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/qiniu")
public class QiniuController {

    private static final Logger logger = LoggerFactory.getLogger(QiniuController.class);

    /**
     * 获取简单上传授权
     *
     * @return jeson data
     */
    @RequestMapping(value = "/uploadlogo/ticket", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> getLogoSimpleUploadToken() {
        Result<String> result = new Result<String>(true, null, null);
        result.setData(QiniuUtils.generateSimpleUploadTicket());
        return result;
    }

    /**
     * 获取简单上传授权
     *
     * @return jeson data
     */
    @RequestMapping(value = "/upload/ticket", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> getSimpleUploadToken() {
        /** Result<String> result = new Result<String>(true, null, null);
         // result.setData(QiniuUtils.generateSimpleUploadTicket());
         result.setData(QiniuUtils.generateSimpleUploadTicket(QiniuConstant.QINIU_SENDBYDRIVER_BUCKET));
         return result; */
        Result<String> result = new Result<String>(true, null, null);
        result.setData(QiniuUtils.generateSimpleUploadTicket());
        return result;
    }

    /**
     * 获取下载授权
     *
     * @return jeson data
     * @throws Exception
     */
    @RequestMapping(value = "/download/ticket", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> getDownloadUrl(String key, String mode, String w, String h) {
        Result<String> result = new Result<String>(false, null, null);
        String treatMethod = "";
        if (!StringUtils.isNullOrEmpty(mode)) {
            if (!StringUtils.isNullOrEmpty(w)) {
                treatMethod = treatMethod + "/w/" + w;
            }
            if (!StringUtils.isNullOrEmpty(h)) {
                treatMethod = treatMethod + "/h/" + h;
            }
            if (!StringUtils.isNullOrEmpty(treatMethod)) {
                treatMethod = "?imageView2/" + mode + treatMethod;
            }
        }
        if (StringUtils.isNullOrEmpty(key)) {
            QiniuController.logger.error("七牛获取下载url，key为空");
            result.setMessage("key为空");
            return result;
        }
        result.setSuccess(true);
        result.setData(QiniuUtils.generateDownloadTicket(key, treatMethod));
        return result;
    }
}
