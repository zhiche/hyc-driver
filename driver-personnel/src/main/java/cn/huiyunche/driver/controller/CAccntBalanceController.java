package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.BScoresService;
import cn.huiyunche.base.service.interfaces.CAccntBalanceService;
import cn.huiyunche.base.service.interfaces.UserService;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 账户余额Controller
 *
 * @author lm
 */
@Controller
@RequestMapping("/accntLedger")
public class CAccntBalanceController {

    private static final Logger log = LoggerFactory.getLogger(CAccntBalanceController.class);

    @Autowired
    private CAccntBalanceService accntLedgerService = null;

    @Autowired
    private UserService userService = null;

    @Autowired
    private BScoresService bScoresService = null;

    private CAccntBalanceService getAccntLedgerService() {
        return this.accntLedgerService;
    }

    private UserService getUserService() {
        return this.userService;
    }

    private BScoresService getBScoresService() {
        return this.bScoresService;
    }

    /**
     * 设置账户密码
     *
     * @param phone
     * @param usertype
     * @param pwd
     * @param code
     * @return
     */
    @RequestMapping(value = "/set/password", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> setPassword(@RequestParam("phone") String phone, @RequestParam("usertype") String usertype,
                                      @RequestParam("pwd") String pwd, @RequestParam("code") String code) {
        log.info("balance set password Controller.phone:{},usertype:{},pwd:{},code:{}.", phone, usertype, pwd, code);
        Result<Object> result = new Result<>();
        if (StringUtils.isEmpty(pwd)) {
            throw new BusinessException("密码不能为空!");
        }
        if (StringUtils.isEmpty(code)) {
            throw new BusinessException("验证码不能为空!");
        }

        // 获取redis中存储的验证码和用户输入的验证码比较
        String authCode = null;
        try {
            authCode = this.getUserService().getAuthCode(phone, Integer.valueOf(usertype));
        } catch (Exception e) {
            log.error("AccntLedgerController.setPassword getAuthCode error : {}", authCode);
            throw new BusinessException("系统错误！");
        }
        // if (StringUtils.isEmpty(authCode) && !"123456".equals(code)) {
        if (StringUtils.isEmpty(authCode)) {
            throw new BusinessException("验证码错误！");
        } else {
            // if (code.equals(authCode) || "123456".equals(code)) {
            if (code.equals(authCode)) {
                try {
                    result = this.getAccntLedgerService().addOrUpdatePwd(pwd);
                    log.info("balance set password Controller finish.");
                } catch (Exception e) {
                    log.error("CAccntBalanceController.setPassword method addOrUpdatePwd error : {}", e);
                    throw new BusinessException("系统异常,请稍后重试!");
                }
            } else {
                throw new BusinessException("验证码错误！");
            }
        }
        return result;
    }

    /**
     * 发送验证码
     *
     * @param phone
     * @param usertype
     * @return
     */
    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> sendCaptcha(@RequestParam("phone") String phone, @RequestParam("usertype") int usertype) {
        return this.getAccntLedgerService().sendCaptcha(phone, usertype);
    }

    /**
     * 查询当前用户余额
     *
     * @return
     */
    @RequestMapping(value = "/balances", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> getBalances() {
        log.info("select current user balance.");
        return this.getAccntLedgerService().getBalances();
    }

    /**
     * 验证用户输入密码是否正确
     *
     * @param pwd
     * @return
     */
    @RequestMapping(value = "/check/pwd", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> checkPwd(@RequestParam("pwd") String pwd) {
        log.info("修改密码,验证原密码是否正确.pwd:{}.", pwd);
        return this.getAccntLedgerService().checkPwd(pwd);
    }

    /**
     * 修改账户密码
     *
     * @param pwd
     * @return
     */
    @RequestMapping(value = "/update/pwd", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> updatePwd(@RequestParam("pwd") String pwd) {
        log.info("修改密码,输入新密码.pwd:{}.", pwd);
        return this.getAccntLedgerService().updatePwd(pwd);
    }

    /**
     * 判断账户是否已经设置密码
     *
     * @return
     */
    @RequestMapping(value = "/exist/pwd", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> isExistPwd() {
        log.info("判断登录用户是否已经设置提现密码Controller.");
        return this.getAccntLedgerService().isExistPwd();
    }

    /**
     * 快速提现
     * 查询手续费(现金抵扣/积分抵扣)
     *
     * @param amount
     * @return
     */
    @RequestMapping(value = "/withdrawfee", method = RequestMethod.POST)
    @ResponseBody
    public Result<Map<String, Object>> getWithdrawalFee(BigDecimal amount) {
        log.info("select quick withdrawal fee params amount : {}.", amount);
        Result<Map<String, Object>> result = new Result<Map<String, Object>>(true, null, "查询提现手续费完成");
        result.setData(this.getBScoresService().selectWithdrawalFee(amount));
        return result;
    }

}
