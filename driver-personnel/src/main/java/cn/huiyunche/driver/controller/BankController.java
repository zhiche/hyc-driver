package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.BankService;
import cn.huiyunche.base.service.vo.OutBankVo;
import cn.huiyunche.base.service.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 银行卡控制器
 *
 * @author hdy [Tuffy]
 */
@Controller
@RequestMapping("/bank")
public class BankController {

    @Autowired
    private BankService bankService = null;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<OutBankVo>> list() {
        Result<List<OutBankVo>> r = new Result<>(true);
        r.setData(this.bankService.list());
        return r;
    }
}
