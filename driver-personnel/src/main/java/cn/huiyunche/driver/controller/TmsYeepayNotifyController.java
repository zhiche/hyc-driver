package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.YeepayService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 易宝回调接口
 *
 * @author hdy
 */
@RequestMapping("/yeepay")
@Controller
public class TmsYeepayNotifyController {

    @Autowired
    private YeepayService yeepayService = null;

    /**
     * 接口回调
     *
     * @param request  the request
     * @param response the response
     * @return 结果集
     */
    @RequestMapping(value = "/notify", method = RequestMethod.POST)
    public void notify(HttpServletRequest request, HttpServletResponse response) {
        try {
            this.yeepayService.yeepNotify(request, response);
        } catch (Exception e) {
            throw new BusinessException("回调异常");
        }
    }

}
