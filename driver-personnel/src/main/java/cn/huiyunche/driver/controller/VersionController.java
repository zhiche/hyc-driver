package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.enums.ProjectEnum;
import cn.huiyunche.base.service.interfaces.VersionService;
import cn.huiyunche.base.service.model.SysVersionNote;
import cn.huiyunche.base.service.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by LYN on 6/18/16.
 */
@Controller
@RequestMapping("/version")
public class VersionController {

    @Autowired
    private VersionService versionService;

    /**
     * 获取最新的ANDROID版本信息
     *
     * @return
     */
    @RequestMapping(value = "/android/latest", method = RequestMethod.GET)
    @ResponseBody
    public Result<SysVersionNote> latestVersionAndroid() {
        Result<SysVersionNote> result = new Result<SysVersionNote>();
        SysVersionNote versionNote = getVersionService().getLasterVersionAndroid(ProjectEnum.DRIVER.getValue());
        result.setSuccess(true);
        result.setData(versionNote);
        if (versionNote == null) {
            result.setSuccess(false);
            result.setMessage("已是最新版本!");
        }
        return result;
    }

    /**
     * 获取最新的IOS版本信息
     *
     * @return
     */
    @RequestMapping(value = "/ios/latest", method = RequestMethod.GET)
    @ResponseBody
    public Result<SysVersionNote> latestVersionIOS() {
        Result<SysVersionNote> result = new Result<SysVersionNote>();
        SysVersionNote versionNote = getVersionService().getLasterVersionIOS(ProjectEnum.DRIVER.getValue());
        result.setSuccess(true);
        result.setData(versionNote);
        if (versionNote == null) {
            result.setSuccess(false);
            result.setMessage("已是最新版本!");
        }
        return result;
    }

    public VersionService getVersionService() {
        return versionService;
    }
}
