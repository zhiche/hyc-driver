package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.vo.DWaybillErrorMsgVo;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DWaybillErrorMsgService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/waybillError")
public class DWaybillErrorExtController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DWaybillErrorExtController.class);

    @Autowired
    private DWaybillErrorMsgService dWaybillErrorMsgService = null;

    private DWaybillErrorMsgService getDWaybillErrorMsgService() {
        return this.dWaybillErrorMsgService;
    }

    @RequestMapping(value = "page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> pageList(PageVo pageVo, DWaybillErrorMsgVo vo) {
        LOGGER.info("DWaybillErrorExtController.pageList params: {}, {}", pageVo, vo);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(this.getDWaybillErrorMsgService().listByCondition(pageVo, vo));
        } catch (Exception e) {
            LOGGER.error("DWaybillErrorExtController.pageList error: {}", e);
            throw new BusinessException("查询异常");
        }
        return result;
    }
}
