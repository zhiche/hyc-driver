package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.OrderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @FileName: cn.huiyunche.driver.controller
 * @Description: Description
 * @author: Aaron
 * @date: 2017/2/8 下午5:44
 */
@Controller
@RequestMapping("/manually")
public class ManuallyContoller {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManuallyContoller.class);

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/recalc", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> recalculateOrderCost(@RequestParam("waybillIds") String waybillIds) throws Exception {
        Result<String> result = new Result<>(true, "执行成功");

        if (StringUtils.isBlank(waybillIds)) {
            LOGGER.error("ManuallyContoller.recalculateOrderCost waybillIds must not be null");
            throw new IllegalArgumentException("orderLineIds不能为空");
        }

        orderService.recalculateOrderCostByWaybillIds(waybillIds);

        return result;
    }
}
