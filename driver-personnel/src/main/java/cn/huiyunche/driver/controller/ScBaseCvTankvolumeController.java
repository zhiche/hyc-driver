package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.form.ScBaseCvTankvolumeForm;
import cn.huiyunche.base.service.interfaces.ScBaseCvTankvolumeService;
import cn.huiyunche.base.service.interfaces.query.ScBaseCvQueryConditions;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @FileName: cn.huiyunche.driver.controller
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/2 下午9:09
 */
@Controller
@RequestMapping("/cv_TankVolume")
public class ScBaseCvTankvolumeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScBaseCvTankvolumeController.class);

    @Autowired
    private ScBaseCvTankvolumeService scBaseCvTankvolumeService;

    @RequestMapping(value = "/page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByPage(PageVo pageVo,ScBaseCvQueryConditions conditions) throws Exception {
        LOGGER.info("ScBaseCvController.listByPage params : {}, {}", pageVo, conditions);
         Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(scBaseCvTankvolumeService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("ScBaseCvController.listByPage error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * List result.
     *
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> list(ScBaseCvQueryConditions conditions) throws Exception {
        LOGGER.info("ScBaseCvController.list param : {}", conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(scBaseCvTankvolumeService.selectListByConditions(null, conditions).get("list"));
        } catch (Exception e) {
            LOGGER.error("ScBaseCvController.list error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Add result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> add(@Valid ScBaseCvTankvolumeForm form, BindingResult br) throws Exception {
        LOGGER.info("ScBaseCvController.add param : {}", form);
        Result<Object> result = new Result<>(true, null, "添加成功");
        try {
            result.setData(scBaseCvTankvolumeService.add(form, br));
        } catch (Exception e) {
            LOGGER.error("ScBaseCvController.add error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Update result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
   @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> update(@Valid ScBaseCvTankvolumeForm form, BindingResult br) throws Exception {
        LOGGER.info("ScBaseCvController.update param : {}", form);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            scBaseCvTankvolumeService.update(form, br);
        } catch (Exception e) {
            LOGGER.error("ScBaseCvController.update error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Select by id route.
     *
     * @param id
     * @return
     */
   @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> getById(Integer id) throws Exception {
        LOGGER.info("ScBaseCvController.getById param : {}", id);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(scBaseCvTankvolumeService.selectByPrimaryId(id));
        } catch (Exception e) {
            LOGGER.error("ScBaseCvController.getById error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }


    /**
     * Del result.
     *
     * @param id the id
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> del(Integer id) throws Exception {
        LOGGER.info("ScBaseCvController.del params: {}", id);
        Result<Object> result = new Result<>(true, null, "删除成功");
        try {
            scBaseCvTankvolumeService.del(id);
        } catch (Exception e) {
            LOGGER.error("ScBaseCvController.del error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
