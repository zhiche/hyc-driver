package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.WaybillGenerateResultsService;
import cn.huiyunche.driver.service.query.WaybillGenerateResultsQueryConditions;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.Map;

/**
 * @FileName: cn.huiyunche.driver.controller
 * @Description: Description
 * @author: Aaron
 * @date: 2017/3/14 下午9:10
 */
@Controller
@RequestMapping("/generate_result")
public class WaybillGenerateResultsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WaybillGenerateResultsController.class);

    @Autowired
    private WaybillGenerateResultsService waybillGenerateResultsService;

    /**
     * List by page result.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Map<String, Object>> listByPage(PageVo pageVo, @Valid WaybillGenerateResultsQueryConditions conditions) throws Exception {
        LOGGER.info("WaybillGenerateResultsController.listByPage params : {}, {}", pageVo, conditions);
        Result<Map<String, Object>> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(waybillGenerateResultsService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("WaybillGenerateResultsController.listByPage error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Regenerate result.
     * 重新生成运单
     *
     * @param ids the ids
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/regenerate", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> regenerate(String ids) throws Exception {
        LOGGER.info("WaybillGenerateResultsController.regenerate param : {}", ids);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            waybillGenerateResultsService.regenerate(ids);
        } catch (Exception e) {
            LOGGER.error("WaybillGenerateResultsController.regenerate error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

}
