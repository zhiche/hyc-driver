package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.DWaybillAttachService;
import cn.huiyunche.base.service.interfaces.DWaybillService;
import cn.huiyunche.base.service.model.DWaybill;
import cn.huiyunche.base.service.vo.DWaybillAttachVo;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/tmswaybill")
public class TMSWaybillController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TMSWaybillController.class);

    @Autowired
    private DWaybillService dWaybillService = null;

    @Autowired
    private DWaybillAttachService dWaybillAttachService = null;

    private DWaybillAttachService getDWaybillAttachService() {
        return this.dWaybillAttachService;
    }

    private DWaybillService getDWaybillService() {
        return this.dWaybillService;
    }

    /**
     * 带伤发运－运单列表
     *
     * @param pageVo
     * @param dWaybill
     * @return
     * @throws BusinessException
     */
    @ResponseBody
    @RequestMapping(value = "/damagelist", method = RequestMethod.GET)
    public Result<Map<String, Object>> damageList(PageVo pageVo, DWaybill dWaybill) throws Exception {
        LOGGER.info("list params: pageVo: {}, dwaybill: {}", pageVo, dWaybill);
        return this.getDWaybillService().list(pageVo, dWaybill);
    }

    /**
     * 运单列表
     *
     * @param pageVo
     * @param dWaybill
     * @return
     * @throws BusinessException
     */
    @ResponseBody
    @RequestMapping(value = "/tmslist", method = RequestMethod.GET)
    public Result<Map<String, Object>> tmsList(PageVo pageVo, DWaybill dWaybill) throws Exception {
        LOGGER.info("list params: pageVo: {}, dwaybill: {}", pageVo, dWaybill);
        return this.getDWaybillService().tmsList(pageVo, dWaybill);
    }

    /**
     * 退单
     *
     * @param waybillId
     * @return the result
     */
    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<String> cancelUserWaybill(Long waybillId) throws Exception {
        LOGGER.info("cancelUserWaybill params waybillId: {}", waybillId);
        return this.getDWaybillService().cancelDWaybillWithUser(waybillId);
    }

    /**
     * 带伤发运运单审核
     *
     * @param waybillId
     * @return
     * @throws BusinessException
     */
    @ResponseBody
    @RequestMapping(value = "/damageaudit", method = RequestMethod.POST)
    public Result<Object> damageAudit(Long waybillId, int status) throws Exception {
        LOGGER.info("damageAudit params waybillId: {}, status: {}", waybillId, status);
        return this.getDWaybillService().damageAudit(waybillId, status);
    }

    /**
     * 查询带伤发运图片
     *
     * @param waybillId
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/listpic", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<DWaybillAttachVo>> listPic(Long waybillId) throws Exception {
        return this.getDWaybillAttachService().listPic(waybillId);
    }
}
