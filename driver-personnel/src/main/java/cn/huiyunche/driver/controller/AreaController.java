package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.BAreaService;
import cn.huiyunche.base.service.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/area")
public class AreaController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AreaController.class);

    @Autowired
    private BAreaService bAreaService = null;

    private BAreaService getBAreaService() {
        return this.bAreaService;
    }

    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<Object> area(String params) throws Exception {
        LOGGER.info("area params : {}", params);
        Result<Object> result = new Result<Object>(true, null, "查询成功");
        result.setData(this.getBAreaService().selectBareaByParams(params));
        return result;
    }
}
