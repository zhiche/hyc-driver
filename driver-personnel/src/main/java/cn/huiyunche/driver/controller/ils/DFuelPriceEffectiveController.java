package cn.huiyunche.driver.controller.ils;

import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DFuelPriceEffectiveService;
import cn.huiyunche.driver.service.form.DFuelPriceEffectiveForm;
import cn.huiyunche.driver.service.query.DFuelPriceEffectiveQueryConditions;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @FileName: cn.huiyunche.driver.controller.ils
 * @Description: 燃油价格变动控制器
 * @author: Aaron
 * @date: 2017/2/27 下午10:16
 */
@Controller
@RequestMapping("/fuel_price")
public class DFuelPriceEffectiveController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DFuelPriceEffectiveController.class);

    @Autowired
    private DFuelPriceEffectiveService dFuelPriceEffectiveService;

    /**
     * List by page result.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByPage(PageVo pageVo, DFuelPriceEffectiveQueryConditions conditions) throws Exception {
        LOGGER.info("DFuelPriceEffectiveController.listByPage params : {}, {}", pageVo, conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dFuelPriceEffectiveService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("DFuelPriceEffectiveController.listByPage error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * List result.
     *
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> list(@Valid DFuelPriceEffectiveQueryConditions conditions) throws Exception {
        LOGGER.info("DFuelPriceEffectiveController.list param : {}", conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dFuelPriceEffectiveService.selectListByConditions(null, conditions).get("list"));
        } catch (Exception e) {
            LOGGER.error("DFuelPriceEffectiveController.list error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Add result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> add(DFuelPriceEffectiveForm form, BindingResult br) throws Exception {
        LOGGER.info("DFuelPriceEffectiveController.add param : {}", form);
        Result<Object> result = new Result<>(true, null, "添加成功");
        try {
            result.setData(dFuelPriceEffectiveService.add(form, br));
        } catch (Exception e) {
            LOGGER.error("DFuelPriceEffectiveController.add error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     */
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> getById(Integer id) throws Exception {
        LOGGER.info("DFuelPriceEffectiveController.getById param : {}", id);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dFuelPriceEffectiveService.selectByPrimaryKey(id));
        } catch (Exception e) {
            LOGGER.error("DFuelPriceEffectiveController.getById error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Update result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> update(DFuelPriceEffectiveForm form, BindingResult br) throws Exception {
        LOGGER.info("DFuelPriceEffectiveController.update param : {}", form);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            result.setData(dFuelPriceEffectiveService.update(form, br));
        } catch (Exception e) {
            LOGGER.error("DFuelPriceEffectiveController.update error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }


    /**
     * Del result.
     *
     * @param id the id
     * @return the result
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> del(Integer id) throws Exception {
        LOGGER.info("DFuelPriceEffectiveController.del param : {}", id);
        Result<Object> result = new Result<>(true, null, "删除成功");
        try {
            dFuelPriceEffectiveService.del(id);
        } catch (Exception e) {
            LOGGER.error("DFuelPriceEffectiveController.del error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

}
