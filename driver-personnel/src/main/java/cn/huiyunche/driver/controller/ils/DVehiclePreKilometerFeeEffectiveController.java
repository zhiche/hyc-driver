package cn.huiyunche.driver.controller.ils;

import cn.huiyunche.base.service.model.DVehiclePreKilometerFeeEffective;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DVehiclePreKilometerFeeEffectiveService;
import cn.huiyunche.driver.service.query.DVehiclePreKilometerFeeEffectiveQueryConditions;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.controller.ils
 * @Description: 线路控制器
 * @author: Aaron
 * @date: 2017/2/27 下午10:16
 */
@Controller
@RequestMapping("/vehicle_pkfe")
public class DVehiclePreKilometerFeeEffectiveController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DVehiclePreKilometerFeeEffectiveController.class);

    @Autowired
    private DVehiclePreKilometerFeeEffectiveService dVehiclePreKilometerFeeEffectiveService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }


    /**
     * List result.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> list(PageVo pageVo, DVehiclePreKilometerFeeEffectiveQueryConditions conditions) throws Exception {
        LOGGER.info("DVehiclePreKilometerFeeEffectiveController.list params : {}, {}", pageVo, conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dVehiclePreKilometerFeeEffectiveService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("DVehiclePreKilometerFeeEffectiveController.list error : {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Exist result.
     *
     * @param vehicleTypeId the vehicle type id
     * @param effectiveDate the effective date
     * @return the result
     */
    @RequestMapping(value = "/exist", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Boolean> exist(Integer vehicleTypeId, Date effectiveDate) throws Exception {
        LOGGER.info("DVehiclePreKilometerFeeEffectiveController.exist params : {}, {}", vehicleTypeId, effectiveDate);
        Result<Boolean> result = new Result<>(true, null, "查询成功");
        try {
            DVehiclePreKilometerFeeEffective vehiclePreKilometerFeeEffective = dVehiclePreKilometerFeeEffectiveService.selectActivedByEffectiveDate(vehicleTypeId, effectiveDate);
            result.setData(null == vehiclePreKilometerFeeEffective ? false : true);
        } catch (Exception e) {
            LOGGER.error("DVehiclePreKilometerFeeEffectiveController.exist error : {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
