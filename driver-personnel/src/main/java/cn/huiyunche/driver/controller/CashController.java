package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.CashService;
import cn.huiyunche.base.service.vo.BindBankCardVo;
import cn.huiyunche.base.service.vo.OutUserBankCardVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * 银行卡提现
 *
 * @author hdy [Tuffy]
 */
@Controller
@RequestMapping("/cash")
public class CashController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CashController.class);

    @Autowired
    private CashService cashService = null;

    /**
     * 绑定银行卡
     *
     * @param bbcv 绑定对象
     * @param br   错误处理对象
     * @return 结果集
     */
    @RequestMapping(value = "/bind", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> bindCard(@Valid BindBankCardVo bbcv, BindingResult br) throws Exception {
        if (br.hasErrors()) {
            Result<String> r = new Result<>();
            List<ObjectError> list = br.getAllErrors();
            r.setSuccess(false);
            r.setMessage(list.get(0).getDefaultMessage());
            return r;
        }
        return this.getCashService().bindBankCard(bbcv);
    }

    /**
     * 解绑银行卡
     *
     * @param cardId  银行卡id
     * @param cashPwd 提现密码
     * @return
     */
    @RequestMapping(value = "/unbind", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> unbindCard(String cardId, String cashPwd) {
        if (StringUtils.isBlank(cardId) || StringUtils.isBlank(cashPwd)) {
            Result<String> r = new Result<>(false);
            r.setMessage("卡号或者密码错误");
            return r;
        }
        return this.getCashService().unBindBankCard(cardId, cashPwd);
    }

    /**
     * 我的银行卡列表
     *
     * @return 结果集
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<OutUserBankCardVo>> MyCardList() throws Exception {
        return this.getCashService().myBankCards();
    }

    /**
     * 提现操作
     *
     * @param cardId    提现卡id
     * @param cashPwd   提现密码
     * @param isUrgency 是否加急
     * @param quickcash 是否是快速提现 0:不是 1:是
     * @param score     是否使用积分 0:不使用 1:使用
     * @return 结果集
     */
    @RequestMapping(value = "/cost", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> cashMyCost(String cardId, String cashPwd, String isUrgency, BigDecimal amount, Integer quickcash, Integer score) {
        LOGGER.info("cash cost params cardId : {}, cashPwd : {}, isUrgency : {}, amount : {}, quickcash : {}, score : {}.", cardId, cashPwd, isUrgency, amount, quickcash, score);
        try {
            return this.getCashService().submitSettlementAppli(cashPwd, amount);
        } catch (Exception e) {
            LOGGER.info("cash cost params cardId : {}, cashPwd : {}, isUrgency : {}, amount : {}, quickcash : {}, score : {}, error : {}.", cardId, cashPwd, isUrgency, amount, quickcash, score, e);
            throw new BusinessException("提现异常");
        }
    }

    /**
     * 卡信息校验
     *
     * @param card 卡信息
     * @return 结果集
     */
    @RequestMapping(value = "/check", method = RequestMethod.POST)
    @ResponseBody
    public Result<Object> checkBankCard(String card) {
        return this.getCashService().checkBankCard(card);
    }

    private CashService getCashService() {
        return this.cashService;
    }

}
