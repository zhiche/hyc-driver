package cn.huiyunche.driver.controller.ils;

import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DRouteVehiclePriceEffectiveService;
import cn.huiyunche.driver.service.form.DRouteVehiclePriceEffectiveForm;
import cn.huiyunche.driver.service.query.DRouteVehiclePriceEffectiveQueryConditions;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @FileName: cn.huiyunche.driver.controller.ils
 * @Description: 车型线路价格变动控制器
 * @author: Aaron
 * @date: 2017/2/27 下午10:16
 */
@Controller
@RequestMapping("/route_vpe")
public class DRouteVehiclePriceEffectiveController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DRouteVehiclePriceEffectiveController.class);

    @Autowired
    private DRouteVehiclePriceEffectiveService dRouteVehiclePriceEffectiveService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    /**
     * List result.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> list(PageVo pageVo, DRouteVehiclePriceEffectiveQueryConditions conditions) throws Exception {
        LOGGER.info("DRouteVehiclePriceEffectiveController.list params : {}, {}", pageVo, conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dRouteVehiclePriceEffectiveService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("DRouteVehiclePriceEffectiveController.list error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Add result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> add(@Valid DRouteVehiclePriceEffectiveForm form, BindingResult br) throws Exception {
        LOGGER.info("DRouteVehiclePriceEffectiveController.add param : {}", form);
        Result<Object> result = new Result<>(true, null, "添加成功");
        try {
            result.setData(dRouteVehiclePriceEffectiveService.add(form, br));
        } catch (Exception e) {
            LOGGER.error("DRouteVehiclePriceEffectiveController.add error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Del result.
     *
     * @param ids the ids
     * @return the result
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> del(String ids) throws Exception {
        LOGGER.info("DRouteVehiclePriceEffectiveController.del param : {}", ids);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            dRouteVehiclePriceEffectiveService.delete(ids);
        } catch (Exception e) {
            LOGGER.error("DRouteVehiclePriceEffectiveController.del error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> update(@Valid DRouteVehiclePriceEffectiveForm form, BindingResult br) throws Exception {
        LOGGER.info("route update params: {}", form);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            result.setData(dRouteVehiclePriceEffectiveService.update(form, br));
        } catch (Exception e) {
            LOGGER.error("route update error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Calc total price result.
     *
     * @param vehicleTypeId       the vehicle type id
     * @param labourServicesPrice the labour services price
     * @return the result
     */
    @RequestMapping(value = "/calctotal", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> calcTotalPrice(Integer vehicleTypeId, BigDecimal labourServicesPrice, Date effectiveDate) throws Exception {
        LOGGER.info("DRouteVehiclePriceEffectiveController.calcTotalPrice params : {}, {}, {}", vehicleTypeId, labourServicesPrice, effectiveDate);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            result.setData(dRouteVehiclePriceEffectiveService.calcTotalPrice(vehicleTypeId, labourServicesPrice, effectiveDate));
        } catch (Exception e) {
            LOGGER.error("DRouteVehiclePriceEffectiveController.calcTotalPrice error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Select DRouteVehiclePriceEffective by id
     *
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getbyid", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> getById(Integer id) throws Exception {
        LOGGER.info("DRouteVehiclePriceEffectiveController.getById param id : {}", id);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(this.dRouteVehiclePriceEffectiveService.getById(id));
        } catch (Exception e) {
            LOGGER.error("DRouteVehiclePriceEffectiveController.getById error : {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

}
