package cn.huiyunche.driver.controller.ils;


import cn.huiyunche.base.service.form.ScPriceconfFreightVlidateLogForm;
import cn.huiyunche.base.service.interfaces.LaborPriceImportService;
import cn.huiyunche.base.service.query.ScPriceconfFreightValidateQueryConditions;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.base.service.vo.ScPriceconfFreightValidateLogVo;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 劳务价格导入
 * Created by Felix on 2017/5/8.
 */
@Controller
@RequestMapping("/labor_price_import")
public class LaborPriceImportController {

    private static final Logger logger = LoggerFactory.getLogger(LaborPriceImportController.class);

    @Autowired
    private LaborPriceImportService laborPriceImportService;

    /**
     * 导入excel文件
     *
     * @param
     * @throws Exception
     */
    @RequestMapping(value = "/import", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Result<Object> uploadExcel(@RequestParam("excelData") String excelData) throws Exception {
        logger.info("import start: LaborPriceImportController job time: {}", new Date().toString());
        Result<Object> result = new Result<>(true, "导入成功!");

        if (StringUtils.isEmpty(excelData)) {
            logger.error("excel数据异常");
            result.setSuccess(false);
            throw new BusinessException("excel数据异常");
        } else {
            List<ScPriceconfFreightValidateLogVo> list = new ArrayList<ScPriceconfFreightValidateLogVo>();
            list = JSONObject.parseArray(excelData, ScPriceconfFreightValidateLogVo.class);
            if (list.size() == 0) {
                logger.error("excel数据为空");
                result.setSuccess(false);
                throw new BusinessException("excel数据为空");
            } else {
                try {
                    String batchId = UUID.randomUUID().toString();
                    laborPriceImportService.importExcel(list, batchId);
                    result.setData(batchId);
                    checkData(batchId);
                } catch (Exception e) {
                    logger.error("LaborPriceImportController.import error: {}", e);
                    result.setMessage("excel数据导入失败！");
                }
            }
        }

        return result;
    }

    /**
     * List by page result.
     *
     * @param pageVo the page vo
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/loadExcelData", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByPage(PageVo pageVo, @Valid ScPriceconfFreightValidateQueryConditions conditions) throws Exception {
        logger.info("LaborPriceImportController.listByPage params : {}, {}", pageVo);
        Result<Object> result = new Result<>(true, null, "查询成功！");
        try {
            result.setData(laborPriceImportService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            logger.error("LaborPriceImportController.listByPage error: {}", e);
            throw new BusinessException("加载数据失败!");
        }
        return result;
    }

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     * @throws Exception the exception
     */
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> getById(Integer id) throws Exception {
        logger.info("LaborPriceImportController.getById param : {}", id);
        Result<Object> result = new Result<>(true, null, "查询成功！");
        try {
            result.setData(laborPriceImportService.selectByPrimaryKey(id));
        } catch (Exception e) {
            logger.error("LaborPriceImportController.getById error: {}", e);
            throw new BusinessException("获取修改数据失败！");
        }
        return result;
    }

    /**
     * Update result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> update(@Valid ScPriceconfFreightVlidateLogForm form, BindingResult br) throws Exception {
        logger.info("LaborPriceImportController.update param : {}", form);
        Result<Object> result = new Result<>(true, null, "更新成功！");
        try {
            laborPriceImportService.update(form, br);
            checkData(form.getBatchId());
        } catch (Exception e) {
            logger.error("LaborPriceImportController.update error: {}", e);
            throw new BusinessException("修改失败！");
        }
        return result;
    }

    /**
     * Del result.
     *
     * @param id the id
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> del(Integer id,String batchId) throws Exception {
        logger.info("LaborPriceImportController.del param : {}", id);
        Result<Object> result = new Result<>(true, null, "删除成功！");
        try {
            laborPriceImportService.del(id);
            checkData(batchId);
        } catch (Exception e) {
            logger.error("LaborPriceImportController.del error: {}", e);
            throw new BusinessException("删除失败！");
        }
        return result;
    }

    /**
     * deleteAll result.
     *
     * @param id the id
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> delete(Integer id,String batchId) throws Exception {
        logger.info("LaborPriceImportController.delete param : {}", id);
        Result<Object> result = new Result<>(true, null, "取消导入成功！");
        try {
            laborPriceImportService.delete();
            checkData(batchId);
        } catch (Exception e) {
            logger.error("LaborPriceImportController.delete error: {}", e);
            throw new BusinessException("取消导入失败！");
        }
        return result;
    }

    /**
     * 校验数据
     *
     * @param
     * @throws Exception
     */
    @RequestMapping(value = "/check", method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    Result<Object> checkData(String batchId) throws Exception {
        logger.info("check start: LaborPriceImportController job time: {}", new Date().toString());
        Result<Object> result = new Result<>(true, null, "校验成功！");
        try {
            laborPriceImportService.checkData(batchId);
        } catch (Exception e) {
            logger.error("LaborPriceImportController.del error: {}", e);
            throw new BusinessException("校验数据失败");
        }
        return result;
    }

    /**
     * 插入数据
     *
     * @param
     * @throws Exception
     */

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> insertData(String batchId) throws Exception {
        logger.info("insert start: LaborPriceImportController job time: {}", new Date().toString());
        Result<Object> result = new Result<>(true, null, "导入成功！");
        try {
            laborPriceImportService.importFreightAndMile(batchId);
            laborPriceImportService.checkInvalidDate();
//            laborPriceImportService.deleteAll();
        } catch (Exception e) {
            logger.error("LaborPriceImportController.del error: {}", e);
            throw new BusinessException("导入数据失败！");
        }
        return result;
    }

    /**
     * 更新失效日期
     *
     * @param
     * @throws Exception
     */
    @RequestMapping(value = "/updateInvalidDate", method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    Result<Object> updateInvalidDate(String batchId) throws Exception {
        logger.info("updateInvalidDate start: LaborPriceImportController job time: {}", new Date().toString());
        Result<Object> result = new Result<>(true, null, "更新失效日期成功！");
        try {
            laborPriceImportService.checkInvalidDate();
        } catch (Exception e) {
            logger.error("LaborPriceImportController.del error: {}", e);
            throw new BusinessException("更新失效日期失败");
        }
        return result;
    }
}
