package cn.huiyunche.driver.controller.ils;

import cn.huiyunche.base.service.interfaces.UserService;
import cn.huiyunche.base.service.mappers.DWaybillMapper;
import cn.huiyunche.base.service.model.*;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.base.service.vo.UserVo;
import cn.huiyunche.driver.service.CMdvBillDetailService;
import cn.huiyunche.driver.service.CMdvBillSummaryService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * app查询账单
 *
 * @author hdy [Tuffy]
 */
@Controller
@RequestMapping("/billSummary")
public class CMdvBillSummaryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CMdvBillSummaryController.class);

    @Autowired
    private CMdvBillSummaryService cMdvBillSummaryService;


    /**
     * 获取用户下单周账单列表
     *
     * @param request
     * @param beginDate
     * @param endDate
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/summaryList", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> summaryList(HttpServletRequest request, @RequestParam("beginDate") String beginDate, @RequestParam("endDate") String endDate) throws Exception {
        LOGGER.info(" CMdvBillSummaryController.summaryList{}", beginDate, endDate);
        Result<Object> result = cMdvBillSummaryService.getSummaryList(beginDate, endDate);


        return result;
    }

    /**
     * 获取用户下单周运单详细信息
     *
     * @param request
     * @param orderCode
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/billDetailList", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> billDetail(HttpServletRequest request, @RequestParam("orderCode") String orderCode) throws Exception {
        LOGGER.info(" CMdvBillSummaryController.summaryList{}", orderCode);
        Result<Object> result = cMdvBillSummaryService.getBillDetailList(orderCode);
        return result;
    }

}
