package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.CAccntTurnOverService;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 账单流水明细Controller
 *
 * @author lm
 */
@Controller
@RequestMapping("/accntDetail")
public class AccntDetailController {

    private static final Logger log = LoggerFactory.getLogger(AccntDetailController.class);

    @Autowired
    private CAccntTurnOverService accntDetailService = null;

    public CAccntTurnOverService getAccntDetailService() {
        return this.accntDetailService;
    }

    /**
     * 查询账户流水
     *
     * @param page
     * @return
     */
    @RequestMapping(value = "/details", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> getDetails(@ModelAttribute PageVo page) {
        log.info("select current user accntDetails.");
        return this.getAccntDetailService().getDetails(page);
    }

    /**
     * 查询账户流水
     *
     * @param page
     * @return
     */
    @RequestMapping(value = "/monthdetails", method = RequestMethod.GET)
    @ResponseBody
    public Result<Map<String, Object>> getDetailsGroupMonth(@ModelAttribute PageVo page) {
        log.info("select current user accntDetails.");
        return this.getAccntDetailService().getDetailsGroupMonth(page);
    }

}
