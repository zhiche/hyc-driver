package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.BannerService;
import cn.huiyunche.base.service.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/banner")
public class BannerController {

    @Autowired
    private BannerService bannerService = null;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Result<Object> list(String clienttype, String apptype, String w, String h) throws Exception {
        Result<Object> result = new Result<Object>(true, null, "数据加载成功");
        result.setData(this.getBannerService().list(clienttype, apptype, w, h));
        return result;
    }

    private BannerService getBannerService() {
        return this.bannerService;
    }

}
