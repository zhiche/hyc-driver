package cn.huiyunche.driver.controller.ils;

import cn.huiyunche.base.service.model.DRoute;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.DRouteService;
import cn.huiyunche.driver.service.form.DRouteForm;
import cn.huiyunche.driver.service.query.DRouteQueryConditions;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

/**
 * The type D route controller.
 */
@Controller
@RequestMapping("/route")
public class DRouteController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DRouteController.class);

    @Autowired
    private DRouteService dRouteService = null;

    private DRouteService getDRouteService() {
        return this.dRouteService;
    }

    /**
     * List by page result.
     *
     * @param pageVo the page vo
     * @param route  the route
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByPage(PageVo pageVo, DRouteQueryConditions route) throws Exception {
        LOGGER.info("DRouteController.listByPage params: {}", route);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(this.getDRouteService().selectListByConditions(pageVo, route));
        } catch (Exception e) {
            LOGGER.error("DRouteController.listByPage error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * List result.
     *
     * @param route the route
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<List<DRoute>> list(DRouteQueryConditions route) throws Exception {
        LOGGER.info("DRouteController.list params: {}", route);
        Result<List<DRoute>> result = new Result<>(true, null, "查询成功");
        try {
            result.setData((List<DRoute>) this.getDRouteService().selectListByConditions(null, route).get("list"));
        } catch (Exception e) {
            LOGGER.error("DRouteController.list error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * add route
     *
     * @param form
     * @param br
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> add(@Valid DRouteForm form, BindingResult br) throws Exception {
        LOGGER.info("DRouteController.add params: {}", form);
        Result<Object> result = new Result<>(true, null, "添加成功");
        try {
            result.setData(this.getDRouteService().add(form, br));
        } catch (Exception e) {
            LOGGER.error("DRouteController.add error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * update route
     *
     * @param form
     * @param br
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> update(@Valid DRouteForm form, BindingResult br) throws Exception {
        LOGGER.info("DRouteController.update params: {}", form);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            result.setData(this.getDRouteService().update(form, br));
        } catch (Exception e) {
            LOGGER.error("DRouteController.update error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Enable or disabled route.
     *
     * @param id
     * @param enableOrDisabled
     * @return
     */
    @RequestMapping(value = "/enableOrDisabled", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> enableOrDisabled(Integer id, boolean enableOrDisabled) throws Exception {
        LOGGER.info("DRouteController.enableOrDisabled params: {}, {}", id, enableOrDisabled);
        Result<Object> result = new Result<>(true, null, "操作成功");
        try {
            result.setData(this.getDRouteService().enableOrDisabled(id, enableOrDisabled));
        } catch (Exception e) {
            LOGGER.error("DRouteController.enableOrDisabled error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     * @throws Exception the exception
     */
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> getById(Integer id) throws Exception {
        LOGGER.info("DRouteController.getById params: {}", id);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(dRouteService.selectByPrimaryKey(id));
        } catch (Exception e) {
            LOGGER.error("DRouteController.getById error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Del result.
     *
     * @param id the id
     * @return the result
     * @throws Exception the exception
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> del(Integer id) throws Exception {
        LOGGER.info("DRouteController.del params: {}", id);
        Result<Object> result = new Result<>(true, null, "删除成功");
        try {
            dRouteService.del(id);
        } catch (Exception e) {
            LOGGER.error("DRouteController.del error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
