package cn.huiyunche.driver.controller.ils;

import cn.huiyunche.base.service.form.BVehicleTypeForm;
import cn.huiyunche.base.service.interfaces.BLicenseTypeService;
import cn.huiyunche.base.service.interfaces.BVehicleTypeService;
import cn.huiyunche.base.service.model.BLicenseTypeExample;
import cn.huiyunche.base.service.query.BVehicleTypeQueryConditions;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * @FileName: cn.huiyunche.driver.controller.ils
 * @Description: 车型类型控制器
 * @author: Aaron
 * @date: 2017/2/27 下午10:16
 */
@Controller
@RequestMapping("/vehicle")
public class BVehicleTypeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BVehicleTypeController.class);

    @Autowired
    private BVehicleTypeService bVehicleTypeService;

    @Autowired
    private BLicenseTypeService bLicenseTypeService = null;

    /**
     * List by page result.
     *
     * @param pageVo     the page vo
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/page_list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> listByPage(PageVo pageVo, @Valid BVehicleTypeQueryConditions conditions) throws Exception {
        LOGGER.info("BVehicleTypeController.listByPage params : {}, {}", pageVo, conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(bVehicleTypeService.selectListByConditions(pageVo, conditions));
        } catch (Exception e) {
            LOGGER.error("BVehicleTypeController.listByPage error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * List result.
     *
     * @param conditions the conditions
     * @return the result
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> list(@Valid BVehicleTypeQueryConditions conditions) throws Exception {
        LOGGER.info("BVehicleTypeController.list param : {}", conditions);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(bVehicleTypeService.selectListByConditions(null, conditions).get("list"));
        } catch (Exception e) {
            LOGGER.error("BVehicleTypeController.list error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Add result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> add(@Valid BVehicleTypeForm form, BindingResult br) throws Exception {
        LOGGER.info("BVehicleTypeController.add param : {}", form);
        Result<Object> result = new Result<>(true, null, "添加成功");
        try {
            result.setData(bVehicleTypeService.add(form, br));
        } catch (Exception e) {
            LOGGER.error("BVehicleTypeController.add error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Update result.
     *
     * @param form the form
     * @param br   the br
     * @return the result
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Result<Object> update(@Valid BVehicleTypeForm form, BindingResult br) throws Exception {
        LOGGER.info("BVehicleTypeController.update param : {}", form);
        Result<Object> result = new Result<>(true, null, "更新成功");
        try {
            result.setData(bVehicleTypeService.update(form, br));
        } catch (Exception e) {
            LOGGER.error("BVehicleTypeController.update error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * Select by id route.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> getById(Long id) throws Exception {
        LOGGER.info("BVehicleTypeController.getById param : {}", id);
        Result<Object> result = new Result<>(true, null, "查询成功");
        try {
            result.setData(bVehicleTypeService.selectByPrimaryId(id));
        } catch (Exception e) {
            LOGGER.error("BVehicleTypeController.getById error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 查询驾照类型列表
     *
     * @return
     */
    @RequestMapping(value = "licenseTypeList", method = RequestMethod.GET)
    public
    @ResponseBody
    Result<Object> licenseTypeList() throws Exception {
        Result<Object> result = new Result<>(true, null, "查询成功");
        result.setData(this.getBLicenseTypeService().selectByExample(new BLicenseTypeExample()));
        return result;
    }

    private BLicenseTypeService getBLicenseTypeService() {
        return this.bLicenseTypeService;
    }
}
