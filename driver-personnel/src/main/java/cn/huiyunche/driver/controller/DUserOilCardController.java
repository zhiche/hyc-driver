package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.DUserOilCardService;
import cn.huiyunche.base.service.vo.DUserOilCardVo;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/oilcard")
public class DUserOilCardController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DUserOilCardController.class);

    @Autowired
    private DUserOilCardService dUserOilCardService = null;

    private DUserOilCardService getDUserOilCardService() {
        return this.dUserOilCardService;
    }

    /**
     * 司机-油卡列表
     *
     * @param pageVo
     * @param oilCardVo
     * @return
     * @throws BusinessException
     */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<Map<String, Object>> list(PageVo pageVo, DUserOilCardVo oilCardVo) throws Exception {
        LOGGER.info("list params pageVo: {}, oilCardVo: {}", pageVo.toString(), oilCardVo.toString());
        return this.getDUserOilCardService().list(pageVo, oilCardVo);
    }

    /**
     * 司机-油卡状态变更
     *
     * @param id
     * @param status
     * @return
     * @throws BusinessException
     */
    @ResponseBody
    @RequestMapping(value = "/modifystatus", method = RequestMethod.POST)
    public Result<Object> modifyStatus(Long id, String status) throws Exception {
        LOGGER.info("modifyStatus params id: {}, status: {}", id, status);
        return this.getDUserOilCardService().modifyStatus(id, status);
    }
}
