package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.interfaces.TmsDAccidentService;
import cn.huiyunche.base.service.vo.DAccidentVo;
import cn.huiyunche.base.service.vo.PageVo;
import cn.huiyunche.base.service.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/accident")
public class DAccidentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DAccidentController.class);

    @Autowired
    private TmsDAccidentService tmsDAccidentService = null;

    private TmsDAccidentService getTmsDAccidentService() {
        return this.tmsDAccidentService;
    }

    /**
     * 事故申报
     *
     * @param waybillId
     * @param description
     * @param picKeys
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public Result<Object> modify(Long id, Long waybillId, String description, String picKeys) throws Exception {
        LOGGER.info("add params id: {}, waybillId: {}, description: {}, picKeys: {}", id, waybillId, description,
                picKeys);
        return this.getTmsDAccidentService().modify(id, waybillId, description, picKeys);
    }

    /**
     * 查询事故历史记录
     *
     * @param pageVo
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<Map<String, Object>> list(PageVo pageVo) throws Exception {
        LOGGER.info("List params pageVo: {}", pageVo);
        return this.getTmsDAccidentService().list(pageVo);
    }

    /**
     * 查询事故记录
     *
     * @param waybillId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/{waybillId}", method = RequestMethod.GET)
    public Result<DAccidentVo> selectDAccident(@PathVariable Long waybillId) throws Exception {
        LOGGER.info("selectDAccident params waybillId: {}", waybillId);
        return this.getTmsDAccidentService().selectDAccident(waybillId);
    }
}