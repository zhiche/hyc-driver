package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.model.CrmCustomerServiceContact;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.driver.service.CrmCustomerServiceContactService;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 人送车 关于接口
 *
 * @author hdy [Tuffy]
 */
@Controller
@RequestMapping("/about")
public class TmsAboutController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TmsAboutController.class);

    @Autowired
    private CrmCustomerServiceContactService crmCustomerServiceContactService;

    /**
     * 获取客服中心数据
     *
     * @return 结果集
     */
    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<JSONObject>> customerCenter() {
        Result<List<JSONObject>> r = new Result<>(true);
        List<JSONObject> list = new ArrayList<>();
        // 客服电话
        JSONObject customerPhone = new JSONObject();
        customerPhone.put("label", "客服热线");
        customerPhone.put("value", "400-818-7999");
        list.add(customerPhone);
        // 应急人电话
        JSONObject emergencyPhone = new JSONObject();
        emergencyPhone.put("label", "交车争议处理联系人 万超 ");
        emergencyPhone.put("value", "182-7913-3598");
        list.add(emergencyPhone);
        // 协调人电话
        JSONObject concertPhone = new JSONObject();
        concertPhone.put("label", "协调 吴刘婷 ");
        concertPhone.put("value", "152-5566-8801");
        list.add(concertPhone);
        // 安全部电话
        JSONObject securityPhone = new JSONObject();
        securityPhone.put("label", "安全部 王杰 ");
        securityPhone.put("value", "152-7082-0691");
        list.add(securityPhone);
        // 油卡管理员电话
        JSONObject oilCardPhone = new JSONObject();
        oilCardPhone.put("label", "油卡管理员 陈荣华 ");
        oilCardPhone.put("value", "133-9700-2812");
        list.add(oilCardPhone);
        r.setData(list);
        return r;
    }

    /**
     * 获取客服中心数据
     *
     * @return 结果集
     */
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<CrmCustomerServiceContact>> customerCenter(String clientType, String appType) {
        Result<List<CrmCustomerServiceContact>> result = new Result<>(true, null, "查询数据成功");
        try {
            result.setData(crmCustomerServiceContactService.listCrmCustomerServiceContact(clientType, appType));
        } catch (Exception e) {
            LOGGER.error("TmsAboutController.customerCenter error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
