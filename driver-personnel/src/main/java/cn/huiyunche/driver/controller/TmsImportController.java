package cn.huiyunche.driver.controller;

import cn.huiyunche.base.service.enums.UserTypeEnum;
import cn.huiyunche.base.service.framework.utils.JdbcTemplateUtils;
import cn.huiyunche.base.service.framework.utils.TmsQueueApi;
import cn.huiyunche.base.service.interfaces.DWaybillService;
import cn.huiyunche.base.service.interfaces.TmsQueueService;
import cn.huiyunche.base.service.interfaces.UserService;
import cn.huiyunche.base.service.model.SUser;
import cn.huiyunche.base.service.vo.Result;
import cn.huiyunche.base.service.vo.TmsQueueOrderVo;
import cn.huiyunche.driver.service.TmsImportService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 导入tms司机数据到平台 控制器
 *
 * @author hdy [Tuffy]
 */
@Controller
@RequestMapping("/tmsimport")
public class TmsImportController {

    @Autowired
    private TmsImportService tmsImportService = null;

    @Autowired
    private TmsQueueService tmsQueueService = null;

    @Autowired
    private UserService userService = null;

    @Autowired
    private DWaybillService dWaybillService = null;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Result<String> importExcel() {
        return this.tmsImportService.importUserExcel();
    }

    @RequestMapping(value = "/stop", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> stop() {
        Result<String> r = new Result<>(true);
        TmsQueueApi.setRootStartQueue(false);
        r.setMessage("派单终止");
        return r;
    }

    @RequestMapping(value = "/start", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> start() {
        Result<String> r = new Result<>(true);
        TmsQueueApi.setRootStartQueue(true);
        r.setMessage("派单开始");
        return r;
    }

    @RequestMapping(value = "/dispatch", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> dispatch() {
        Result<String> r = new Result<>(true);
        // 清除缓存
        TmsQueueApi.clearOrderQueue();
        TmsQueueApi.clearDriverQueue();
        TmsQueueApi.clearDriverIndexQueue();
        return r;
    }

    /**
     * 分供方下单接口
     *
     * @param license    数据口令
     * @param phone      手机号
     * @param orderCodes 订单数据
     * @return
     */
    @RequestMapping(value = "/consumer", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> consumerDispatch(String license, String phone, String orderCodes) throws Exception {
        Result<String> r = new Result<>(false);
        if (StringUtils.isBlank(license) || !license.equals("fgf_ZC")) {
            r.setMessage("参数不合法");
            return r;
        }
        // 用户信息
        SUser user = this.userService.getByPhone(phone, UserTypeEnum.CONSUMER_DRIVER.getValue());
        if (null == user) {
            r.setMessage("用户信息不存在");
            return r;
        }
        return this.dWaybillService.dispatchConsumerDriver(orderCodes, user, false);
    }

    @RequestMapping(value = "/order2redis", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> importOrderInRedis() throws Exception {
        Result<String> r = new Result<>(false);
        List<Map<String, Object>> list = JdbcTemplateUtils.getJdbcTemplate().queryForList("SELECT * from d_waybill where user_id = 0 and waybill_status = 0");
        if (null != list) {

            for (Map<String, Object> m : list) {
                //加入队列
                TmsQueueOrderVo vo = new TmsQueueOrderVo();
                vo.setWaybillId(m.get("id").toString());
                vo.setTmsSerialNo(m.get("order_line_id").toString());
                vo.setFrom(m.get("departure_city").toString());
                vo.setTo(m.get("dest_city").toString());
                vo.setUrgent(Boolean.valueOf(m.get("is_urgent").toString()));
                this.tmsQueueService.appendOrder(vo);
            }
            r.setSuccess(true);
            return r;
        }
        r.setMessage("数据为空");
        return r;
    }
}
