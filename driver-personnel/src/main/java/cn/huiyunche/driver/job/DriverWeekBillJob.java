package cn.huiyunche.driver.job;

import cn.huiyunche.base.service.interfaces.CAccntBillService;
import cn.huiyunche.base.service.utils.DateUtils;
import cn.huiyunche.tools.basic.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 人送司机周账单 更新可提现金额
 *
 * @author Administrator
 */
@Component
public class DriverWeekBillJob {

    private static final Logger log = LoggerFactory.getLogger(DriverWeekBillJob.class);

    @Autowired
    private CAccntBillService cAccntBillService = null;

    public CAccntBillService getAccntBillService() {
        return this.cAccntBillService;
    }

    @Scheduled(cron = "0 0 4 ? * MON") // 每周一4点执行 人送司机
    // @Scheduled(cron = "* * * * * *") 每秒钟执行一次－测试
    public void doJob() {
        log.info("gen driver week bill Job start.");

        Date currentDate = new Date();
        try {
            // Date beginDate = DateUtils.firstDayOfLastMonth(currentDate);
            // Date endDate = DateUtils.firstDayOfCurrMonth(currentDate);
            Date beginDate = DateUtils.lastMonday(currentDate);
            Date endDate = DateUtils.addDays(beginDate, 7);
            boolean updateBalance = false; // 是否更新 可提现金额
            boolean redo = false; // 是否重新结算
            Long userId = null; // 指定某个用户ID
            int userType = 50; // userType = 10人送司机
            // int userType = 0;
            this.getAccntBillService().genMonthBill(beginDate, endDate, updateBalance, redo, userId, userType);
        } catch (Exception e) {
            log.error("gen driver week bill Job start error : {}.", e);
            throw new BusinessException("生成司机周账单异常");
        }
    }

}
