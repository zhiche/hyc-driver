//package cn.huiyunche.driver.schedule;
//
//import cn.huiyunche.driver.service.TmsQueueDimensionService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
///**
// * 派单定时任务
// * @author hdy
// *
// */
//@Component
//public class DispatchOrdersSchedule {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(TMSOrderSchedule.class);
//
//    @Autowired
//    private TmsQueueDimensionService tmsQueueDimensionService ;
//
//    @Scheduled(cron="0 */1 * * * *")
//	public void execute() {
//        tmsQueueDimensionService.startDispatch();
//	}
//
//}
