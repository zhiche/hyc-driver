package cn.huiyunche.driver.schedule;

import cn.huiyunche.base.service.utils.DateUtils;
import org.joda.time.DateTime;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * 通过查询TMS关联人送订单定时任务
 *
 * @author ligl
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class TmsCancelAssignQuartzCluster implements Job, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(TmsCancelAssignQuartzCluster.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        logger.info("job start: update orders job time: {}", new DateTime().toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));
        logger.info("catalina.home: {}", System.getProperty("catalina.home"));
        logger.info("Thread.currentThread: {}", Thread.currentThread().getContextClassLoader().getResource("").getPath());
        // this.tmsQueueService.start();
        QuartzClusterFactory.getTmsUpdateOrdersService().start();
        logger.info("job end: update orders job time: {}", new DateTime().toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));
    }

}
