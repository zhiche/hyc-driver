package cn.huiyunche.driver.schedule;

import cn.huiyunche.base.service.utils.DateUtils;
import cn.huiyunche.driver.service.TmsDDeliveryAuditService;
import org.joda.time.DateTime;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * 交车审核后运单完成定时任务
 *
 * @author hjh
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class WaybillDeliveryQuartzCluster implements Job, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(WaybillDeliveryQuartzCluster.class);


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("jbo start: delivery update waybill status job time: {}", new DateTime().toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));
        TmsDDeliveryAuditService deliveryAuditService = QuartzClusterFactory.getTmsDDeliveryAuditService();
        try {
            deliveryAuditService.updateWaybillStatus();
        } catch (Exception e) {
            LOGGER.error("delivery update waybill status job error: {}", e);
            e.printStackTrace();
        }
        LOGGER.info("jbo end: delivery update waybill status job time: {}", new DateTime().toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));

    }

}
