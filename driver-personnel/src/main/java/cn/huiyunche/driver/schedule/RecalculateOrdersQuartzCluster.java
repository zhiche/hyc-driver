//package cn.huiyunche.driver.schedule;
//
//import cn.huiyunche.base.service.constant.TmsUrlConstant;
//import cn.huiyunche.base.service.enums.TmsUrlTypeEnum;
//import cn.huiyunche.base.service.interfaces.TmsOrderCallHistoryService;
//import cn.huiyunche.base.service.model.TmsOrder;
//import cn.huiyunche.driver.service.OrderService;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import org.apache.commons.collections4.map.HashedMap;
//import org.apache.commons.lang3.StringUtils;
//import org.joda.time.DateTime;
//import org.quartz.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.Serializable;
//import java.util.List;
//import java.util.Map;
//
///**
// * 更新价格定时任务
// * The type Recalculate orders quartz cluster.
// */
//@PersistJobDataAfterExecution
//@DisallowConcurrentExecution
//public class RecalculateOrdersQuartzCluster implements Job, Serializable{
//
//    private static final long serialVersionUID = 7089854684831425637L;
//
//	private static final Logger logger = LoggerFactory.getLogger(RecalculateOrdersQuartzCluster.class);
//
//    @Override
//	public void execute(JobExecutionContext context) throws JobExecutionException {
//		logger.info("job start: recalculate orders job time: {}", new DateTime().toString("yyyy-MM-dd HH:mm:ss:SSS"));
//
//        final String url = TmsUrlConstant.PULL_ORDER_OFFLINE;
//
//        TmsOrderCallHistoryService tmsOrderCallHistoryService = QuartzClusterFactory.getTmsOrderCallHistoryService();
//
////        //查询redis中所有未派的订单
////        OrderService orderService = QuartzClusterFactory.getOrderService();
////        String lineIds = orderService.getIdsFromRedis();
//
//        //查询所有未派和待发车的运单
//        OrderService orderService = QuartzClusterFactory.getOrderService();
//
//        String lineIds = null;
//        try {
//            lineIds = orderService.selectRecalculateOrderIds();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if(StringUtils.isNotBlank(lineIds)){
//
//            Map<String, Object> paramsMap = new HashedMap<>();
//            paramsMap.put("orderlineid", lineIds);
//
//            String result = tmsOrderCallHistoryService.urlCall(url, TmsUrlTypeEnum.PULL_ORDER_OFFLINE.getValue(), paramsMap, 60000);
//            logger.info("RecalculateOrdersQuartzCluster.execute url call return : {}.", result);
//            if(StringUtils.isNotBlank(result)){
//                JSONObject jsonObject = JSONObject.parseObject(result);
//                String data = jsonObject.getString("data");
//                if(StringUtils.isNotBlank(data)){
//                    List<TmsOrder> tmsOrders = JSONArray.parseArray(data, TmsOrder.class);
//                    logger.info("RecalculateOrdersQuartzCluster.execute tmsOrders : {}", tmsOrders);
////                    orderService.handleOrderCost(tmsOrders);
//                }
//            }
//        }
//
//
//        /*List<TmsOrder> tmsOrders = this.tmsImportService.importOrderExcel();
//		LOGGER.info("saveTmsOrders tmsOrders : {}", tmsOrders);
//		if (CollectionUtils.isNotEmpty(tmsOrders)) {
//			tmsOrderService.saveTmsOrders(tmsOrders);
//		}*/
//        logger.info("jbo end: import orders job time: {}", new DateTime().toString("yyyy-MM-dd HH:mm:ss:SSS"));
//	}
//
//}
