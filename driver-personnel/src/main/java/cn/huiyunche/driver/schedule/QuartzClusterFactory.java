package cn.huiyunche.driver.schedule;

import cn.huiyunche.base.service.interfaces.TmsOrderCallHistoryService;
import cn.huiyunche.base.service.interfaces.impl.TmsOrderCallHistoryServiceImpl;
import cn.huiyunche.base.service.utils.SpringApplication;
import cn.huiyunche.driver.service.OrderService;
import cn.huiyunche.driver.service.TmsDDeliveryAuditService;
import cn.huiyunche.driver.service.TmsQueueDimensionService;
import cn.huiyunche.driver.service.TmsUpdateOrdersService;
import cn.huiyunche.driver.service.impl.OrderServiceImpl;
import cn.huiyunche.driver.service.impl.TmsDDeliveryAuditServiceImpl;
import cn.huiyunche.driver.service.impl.TmsQueueDimensionServiceImpl;
import cn.huiyunche.driver.service.impl.TmsUpdateOrdersServiceImpl;

/**
 * 定时任务工厂类
 *
 * @author hdy
 */
public class QuartzClusterFactory {

    /**
     * 定时任务 订单服务接口
     *
     * @return TmsOrderService
     */
    public static OrderService getOrderService() {
        return SpringApplication.getBean(OrderServiceImpl.class);
    }

    /**
     * 定时任务 订单派单接口
     *
     * @return TmsQueueDimensionService
     */
    public static TmsQueueDimensionService getTmsQueueDimensionService() {
        return SpringApplication.getBean(TmsQueueDimensionServiceImpl.class);
    }

    /**
     * 定时任务 查询TMS关联人送订单接口
     *
     * @return TmsQueueDimensionService
     */
    public static TmsUpdateOrdersService getTmsUpdateOrdersService() {
        return SpringApplication.getBean(TmsUpdateOrdersServiceImpl.class);
    }

    /**
     * 定时任务 订单流程历史接口
     *
     * @return TmsOrderCallHistoryService
     */
    public static TmsOrderCallHistoryService getTmsOrderCallHistoryService() {
        return SpringApplication.getBean(TmsOrderCallHistoryServiceImpl.class);
    }

    /**
     * 定时任务 订单交车接口
     *
     * @return TmsDDeliveryAuditService
     */
    public static TmsDDeliveryAuditService getTmsDDeliveryAuditService() {
        return SpringApplication.getBean(TmsDDeliveryAuditServiceImpl.class);
    }
}
