package cn.huiyunche.driver.schedule;

import cn.huiyunche.base.service.utils.DateUtils;
import org.joda.time.DateTime;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * 派单定时任务
 *
 * @author hdy
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class DispatchOrdersQuartzCluster implements Job, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(DispatchOrdersQuartzCluster.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        logger.info("jbo start: dispatch orders job time: {}", new DateTime().toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));
        logger.info("catalina.home: {}", System.getProperty("catalina.home"));
        logger.info("Thread.currentThread: {}", Thread.currentThread().getContextClassLoader().getResource("").getPath());
        // this.tmsQueueService.start();
        QuartzClusterFactory.getTmsQueueDimensionService().startDispatch();
        logger.info("jbo end: dispatch orders job time: {}", new DateTime().toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));
    }

}
