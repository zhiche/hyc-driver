package cn.huiyunche.driver.schedule;

/**
 * 获取订单定时任务
 *
 * @author hdy
 */
//@Component
//public class ImportOrdersSchedule {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(ImportOrdersSchedule.class);
//
//    @Autowired
//    private TmsQueueDimensionService tmsQueueDimensionService ;
//
//    @Scheduled(cron="0 */1 * * * *")
//	public void execute() {
//        final String url = TmsUrlConstant.PULL_IMPORT;
//
//        TmsOrderCallHistoryService tmsOrderCallHistoryService = QuartzClusterFactory.getTmsOrderCallHistoryService();
//
//        //查询上一次成功url调用成功的记录
//        Date lastTime = tmsOrderCallHistoryService.getLastTimeSuccess(TmsUrlTypeEnum.PULL_IMPORT.getValue(), true);
//
//        Map<String, Object> paramsMap = new HashedMap<>();
//        paramsMap.put("lastTime", new DateTime(lastTime).toString(DateUtils.YYYY_MM_DD_HH_MM_SS_SSS));
//        LOGGER.info("ImportOrdersSchedule.execute interface query time : {}", lastTime);
//
//        String result = tmsOrderCallHistoryService.urlCall(url, TmsUrlTypeEnum.PULL_IMPORT.getValue(), paramsMap, 30000);
//        if(StringUtils.isNotBlank(result)){
//            JSONObject jsonObject = JSONObject.parseObject(result);
//            String data = jsonObject.getString("data");
//            if(StringUtils.isNotBlank(data)){
//                List<TmsOrder> tmsOrders = JSONArray.parseArray(data, TmsOrder.class);
//
//                OrderService orderService = QuartzClusterFactory.getOrderService();
//                orderService.handleTmsOrders(tmsOrders);
//            }
//        }
//	}
//
//}
